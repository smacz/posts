---
layout: post
title: "Vivaldi Setup"
date: 2020-04-27
category: Battlestation
program: Vivaldi
process: Configure
repo:
image: dead_bird.png
description: "The Power User's setup of the Power User's browser."
---
A while ago, I wrote up my Firefox customization post. However, since then, there have been a lot of changes, including the entire plugin architecture, and a ton of marketing and political shenanigans in the browser space. The most interesting development has been the creation of the [Vivaldi Browser](https://vivaldi.com/). This is a browser with a ton of cool stuff, but most importantly, the stuff I need.

# Workflow

As some of you may (or may not) know, my favorite way to work is to have one browser window open for every task that I am currently working on. This allows me to focus on one thing at a time, and to have all of my tabs easily split up between functional workstreams.

This is actually born out of the workflow that `tabgroups` allowed for back in the pre-web extensions days in Firefox. I could have a group of tabs that I could background and pull a different group up to the front, effectively letting me context switch between jobs dynamically. When the API that allowed that went away, I went searching for another workflow that complimented that style, and found Vivaldi.

# Quick Commands

For those of you that still have F-keys, `F2` has become a beloved shortcut of mine in the browser, with which I can access things like this by fuzzy searching rather than having to click through menus. A lot of Vivaldi's functionality would otherwise remain hidden or inaccessible, but this has made all the difference in its functionality.

[Vivaldi Browser Help | Quick Commands]

#### This is also very amenable to being mapped additionally to `CTRL+Space` if that shortcut isn't already mapped for you (like in the Cinnamon DE)

This window is doubly helpful since it shows you the keyboard shortcuts that could alternatively be used, if there are any.

# Theme

Vivaldi has a Dark theme that is not bad by default. Selecting it will get you a pretty comprehensive all-around dark browser.

# Closing Windows

There are a couple settings to touch on about closing windows. The first is "Show Close Window Confirmation Dialog". This gets rid of the confirmation pop-up to close a window. This works perfectly in conjunction with the "Don't Close Window with Last Tab" option unchecked. The combination of these allows a seamless windows management that compliments the idea of "Sessions", described below. However, I _do_ keep "Show Exit Confirmation Dialog", because there are some times where a close button gets accidentally pressed on my main window when it shouldn't, and I want to make sure I don't lose any of my progress. Not like it's that hard to re-open Vivaldi though...

Additionally, for Manjaro anyways, I always check the "Use Native Window" option, as it looks much more uniform with the rest of my OS, as well as enables the Vivaldi Button repositioning as demonstrated below.

#### The "Use Native Window" setting requires a restart to take effect. Make sure you know how to close your windows without using the 'X' on the window because that _will_ be going away once you select this option.

# URL Bar

Whenever I open a new tab, it's better than even money that I'm going to search something. In Manjaro, it's `CTRL+L` to highlight the URL bar. From there, it's as simple as typing in my query and searching what I'm looking for.

To get there I have to enable a couple of things. First, under Address Bar, I disable "Include Typed History", and enable "Include Frequently Visited Pages". This allows for the _very first_ suggestion in my drop-down to be my bookmarks, followed by any history that is still in my browser. Under Quick Commands, I also exclude "Notes", to keep the functionality uniform.

Then, under "Search", I delete all but my favorite search engine. For me, that's [DuckDuckGo](duckduckgo.com), as it provides me [bang commands](https://duckduckgo.com/bang). So I set that as my default and delete all of the others; in that order. It won't let you delete your default search engine, so change that first before you start deleting all of the other ones.

Also under the "Search" section, I disable the "Search Field Display". This gives me almost all of the URL bar to display my URL, which comes in handy when I'm tiling vertically and only have half the screen to display the URL, as well as all of the navigation buttons next to all of my addons. In the same vein, I also get uncheck "Show Profile Button" so that I use all of my URL bar space judiciously.

Under "Privacy", I also get rid of "Show Typed History" in the address field. No need for that. I only want to see where I actually ended up, not what I searched for.

# Blocking Level

New in Vivaldi 3.0+ is the tracker and ad-blocking capabilities of Vivaldi. As you will see below, I already have addons that take care of most of this for me, but it doesn't hurt to have some default protection in place. So I set my Blocking Level to "Block Trackers and Ads", and let Vivaldi do its thing.

# Passwords

As you'll see below, I already have my own password manager, so I don't have to rely on having my own password manager within my browser. First of all, it's not portable, and second of all, it's not very cool either. So I always uncheck "Save Webpage Passwords" in the "Passwords" section of my settings.

# Image Capture

Vivaldi has the _best image capture hands-down_. The only issue I have is that by default it puts the name of the webpage in there, so I end up getting a filename like `...andrewcz.com.png`. There is a very small subset of programs that don't like that multiple extension-like suffix _coughbookstackcough_, so I change the default "Capture File Name Template" to `$year-$month-$day-$hour-$minute-$second-$title`. This ensures that the `$host` variable -- which contains the domain -- does not show up in the filename.

# Fonts

Setting the fonts is easy for me since I have a favorite font family, and it's primarily due to its name. The `Liberation` family of fonts has `-mono`, `-sans`, and `-serif` variations, which takes care of 99% of my use-cases for fonts. Similarly, Vivaldi is able to set these for every option of font that it has settings for.

# Tab Management

Nothing, but _nothing_ is as ugly as a top menu that is more full of tabs than can fit across someone's screen. And especially with the recent WFH pandemic, I am seeing more of that than ever. Vivaldi has quite a few tricks up its sleeve to combat that and make it easy to manage tabs to keep you sane.

## Session Management

Vivaldi has a concept of sessions, which are simply groups of tabs. Typically if I get neck-deep in a task, but run out of time and cannot continue down the rabbit hole, what I can do is save my progress with all of my open tabs in their order for another time.

[Vivaldi Browser Help | Session Management](https://help.vivaldi.com/article/session-management/)

The Session Management Quick Command for this that I'm used to would be "Save Selected Tabs as Session", but usually "save" is close enough to bring it up. However, this is after I select all of the tabs that I want to keep in that session, which is typically all of the tabs in the window. This gives me a leg up since I am able to name this session whatever I want, which would typically refer back to the task that I was working on.

Then, when I'm ready to bring them back, I'll do the same, with the "Open Saved Session" Quick Command, and jump right back into where I started off. And I could clean them as I go if I wanted to, but as long as I keep on top of them and do a purge every now and then, it seems to keep itself manageable.

## Hiberation

Another alternative would be to hibernate tabs.

[Vivaldi Browser Help | Tab Features | Hibernating Tabs](https://help.vivaldi.com/article/tab-features/#hibernating)

I don't prefer this as much, since RAM is cheap, and it doesn't get rid of the window, but it's better than nothing if you're trying to save resources.

## Tab Placement

This is by far the coolest thing about Vivaldi. My tabs are lined up along the right side of my browser window, vertically. This provides me with a couple of benefits:

1. I get a preview of the tab's contents as there is a little preview window under the tab's title
2. That preview can scale appropriately for the amount of tabs I have open
3. When there are a lot of tabs, the title of the tab is not shrunk away to nothing, in fact it is perfectly preserved, since it always takes up the width of the tab space on the side of the browser
4. The most precious real estate of a browser is the vertical space. This increases the available vertical space of the window.

I'll resize the horizontal real estate that I give the tabs so that at their default height (when only 2-3 are open), they are longer vertically than horizontally. I just find that a more judicious use of the screen space than the default **fat** size that comes OOTB.

## Close button location

This is probably the most diddling of tweaks, but I like to have the close buttons for the tabs on the left hand side of the tabs. Confusingly, this is entitled "Window Controls Position" in the settings.

## Disable Tab Stacking

One of the other tab management techniques that Vivaldi has is the ability to "stack" tabs. This essentially allows you to mouse over a tab, and see three or more tabs pop out to select.

There is more working against this than working for this. Just on the face of it, I can't see the tabs that are open this way, I can only see the main one of the tab stack. Additionally, when I'm re-ordering tabs, if I drag one and hover over another for a split second too long, it thinks I'm trying to create a tab stack. So instead of moving the first tab, it merges those two into a new tab stack.

Luckily there's a way to disable this. And I guess this is the first time I'm mentioning this, but the way to get to the settings (the hacker way, anyways), is to go to [vivaldi://settings](vivaldi://settings). (Or, `CTRL+Space`, and typing in `settings`).

That gets me to the General settings of Vivaldi. From there I can either Display All or filter the options by category. The one I'm looking for now is under "TAB FEATURES". I always disable "Allow Stacking by Drag and Drop", and most of the features around that too, since I don't use tab stacking, but there are plenty of options to tweak there if you want to play around with that functionality. For me, using "Sessions" is more than adaquate.

## Close window on tabs close

Since I use windows mainly as a way of organizing tasks, when I'm done with a task, I usually either close the entire window, or close all of the tabs in that window that are remaining. However, the default action is to pop up your start page in a new tab in that window when you've closed the last one. This isn't what I want it to do, so I always make sure to uncheck "Don't Close Window with Last Tab" in the settings, in order that it does - in fact - close the window when the last tab closes.

## Tile Tabs

OK, now _here_ is the one reason why Vivaldi shines above all of the rest of the browsers. Tab Tiling!

> One of the unique features of Vivaldi is the ability to create split screen views with the click of a button. We call it Tab Tiling.
>
> Browsing several sites at once can come in handy. For example, if you’re working on a blog post, it might be useful to have the preview version open in a separate tab to check out the layout as you go. Or, perhaps you need to keep your reference material for a research article close by while you write.
> [Vivaldi Browser Help | Tab Tiling](https://help.vivaldi.com/guide/key-features/split-screen/)

The easiest way to do this (IMO) is to `CTRL+Click` 2+ tabs and right click and select "Tile X Tabs". This will bring up a vertical split by default, which you can change on the bottom in the little square. You can choose horizontal or vertical or grid (for 3 or more).

However, I would go one step further and assign some keyboard shortcuts to help me split:

- Tile Vertically: `CTRL+SHIRT+'`
- Tile Horizontally: `CTRL+SHFT+5`
- Tile to Grid: `CTRL+SHFT+=`
- Untile Tabs: `CTRL+SHFT+[`

There are other defaults that come set up on the system, but these more closely imitate `tmux`, of which I am intimately familiar. It is a lot easier to remember.

## New Tab Defaults

New tabs can do a couple of things. Personally though, I prefer to have it open to a custom location. At the moment, this is my personal blog right here, [`andrewcz.com/jekyll`](https://andrewcz.com/jekyll). However, this can be anywhere that you would like to begin on every new tab. Of course, there is a customizable new tab page, but I didn't find that to be as compelling as my own webpage was. Go on, call me a narcissist, I've been called worse.

## Settings

As a stupid aside to wrap up this talk about tabs, I have to mention that I also check the option to "Open Settings in a Tab", as it's just more fluent with the aesthetic of the browser to use that instead of a stand-alone window.

# Main Menu Location

Before I dive into the addons (which aren't any different than Firefox or Chrome), I wanted to highlight one more cool trick with Vivaldi. This is a bit more in-depth, as it isn't instantly obvious how to go about doing it.

When Vivaldi's tabs are on the right hand side, there is an entire horizontal bar at the top of the browser which takes up the entire width of the screen, and about just as might height as the URL bar. This is obviously a misuse of space, especially considering that the toolbar is conveniently located on the left side of the screen. Wouldn't it be much better if it were over there?

So since I'm on Manjaro, and I compile Vivaldi from the AUR, I'm able to edit the source in `/opt/vivaldi`. So there are a couple of things to do in there. First, add the CSS to remove the header bar, since it's only used to house the Vivaldi button now:

```
# cat /opt/vivaldi/resources/vivaldi/style/remove-header.css
#header {
    display: none !important;
}
```

Make sure that is called by Vivaldi's CSS:

```
# head -n 1 /opt/vivaldi/resources/vivaldi/style/common.css
@import "remove-header.css";
```

That will get rid of the button on the top, and now, to put it on the side, do the following:

```
# cat /opt/vivaldi/resources/vivaldi/move-icon-to-panel.js
function vivaldiIcon() {
    const panel = document.getElementById('switch');
    const button = document.getElementsByClassName('vivaldi')[0]
    const app_icon =  document.getElementsByClassName('application-icon')[0]

    app_icon.innerHTML = '<svg viewBox="0 0 13 12" xmlns="http://www.w3.org/2000/svg"><path d="M9.96 2.446c-.498-1.02.032-2.164 1.115-2.41.881-.2 1.793.464 1.909 1.38.051.402-.027.771-.222 1.118-1.606 2.855-3.213 5.709-4.816 8.566-.298.531-.731.852-1.325.896-.665.049-1.188-.237-1.524-.828-1.016-1.789-2.021-3.586-3.03-5.379-.614-1.092-1.229-2.182-1.84-3.275-.616-1.102.079-2.442 1.315-2.507.653-.034 1.157.274 1.488.854.453.794.897 1.593 1.346 2.389.323.574.64 1.152.972 1.72.481.824 1.189 1.289 2.13 1.347 1.333.081 2.571-.907 2.73-2.358l.025-.272c-.007-.47-.093-.865-.275-1.239z"></path></svg>';
    app_icon.firstChild.style.width = '18px';
    button.style.height = 'calc(var(--toolbarHeight) + var(--padding) - 6px)';

    panel.insertBefore(button, panel.firstChild);
};

let adr = {};
setTimeout(function wait() {
    adr = document.querySelector('.toolbar-addressbar.toolbar');
    if (adr) {
        vivaldiIcon();
    }
    else {
        setTimeout(wait, 300);
    }
}, 300);
```

And this one needs to be called in the `browser.html` file:

```
# grep 'div id="app"' -A 1 /opt/vivaldi/resources/vivaldi/browser.html
    <div id="app" />
    <script src="move-icon-to-panel.js"></script>
```

And Voila! you should have an appreciable amount more vertical space for your browsing.

#### Tapping the `Alt` button will still bring up the menu at its new location.

# Addons

Addons are the lifeblood of a browser. Since Firefox adopted WebExtensions, they are fairly similar across all browsers on desktop. Any one of these can be installed from the Chrome Web Store directly to Vivaldi.

Also, since I tend to use a lot of these when I'm in private browsing, it helps to - after I install them - select the option in their details to "Allow in incognito", which makes sure they are present in my private browsing mode windows.

Here is a quick rundown of the ones I use:

## Vimium

This is one of my favorites! I used to use vimperator before, but this is a cleaner interface, and although it is simpler, there isn't a lot that I have to configure. There is one thing that I do make sure to do though, since my tabs are vertical, it makes sense for me to cycle through them using `j` and `k`, like in vim would take my cursor up and down. However, by default this functionality ends up being inverted. That can be fixed in Vimium's settings:

```
map J nextTab
map K previousTab
```

## Dark Reader

This is another one of my favorites. It is the best global dark mode that I've found. It supports per-domain exceptions, and works pretty much anywhere. And it turns out that almost every single website looks cooler in dark mode.

## uBlock Origin

This is almost an instinct at this point to have an adblocker. I don't go anywhere without this.

## uMatrix

This addon is similar to the old "NoScript" extension in Firefox. It does the same thing, with a fairly intuitive interface, once you get used to it. It's even more granular to an extent. This is definitely only for people who _like_ messing with computers.

However, just as a fair warning, it does break a lot of sites to use it. I would only recommend this if you're someone who is concerned about trackers or other fringe threats.

The benefits though are pretty cool. Extra transparency on what is actually running in your browser, an uncluttered browser pane, and a little speed boost for sites that you visit frequently. Any changes that are made are on a temporary basis until you click on the little lock icon in the top to make them permanent. It will also give you a number of scripts that it blocked as a badge on the addon logo.

## Https Everywhere

This is a no-brainer that costs nothing. We want to make sure we connect via https as often as possible, which we are able to do with this.

## Bitwarden

I may not have said so before, but I use a self-hosted instance of bitwarden for my password manager. This plugin is great for ease-of-access to passwords when logging into sites, and when creating new logins.

## Cookie Autodelete

This is right up there with uBlock origin with how long I've used this. It's inefficient to block cookies on sites, but it's useful to autodelete them. I have it set to two minutes after leaving a site to have its cookies deleted if they are not being used elsewhere. This seems to work out for me just fine.

## JSON Viewer

There are any number of JSON viewers out there, and really, any of them will work. Only a couple of months ago, a colleague of mine at work turned me onto this type of plugin, which is now indispensable when I'm working with APIs that I'm trying to debug or GET requests that make no sense.

## Quick QR Code Generator

This will make more sense when I discuss the mobile Opera browser that has a built-in QR code reading in the browser's URL bar, but this plugin allows a QR code to be generated for any address on the web. It defaults to the page that you are currently on, but is also in the right-click context menu for links, and is editable when it is generated, in order to remove custom tokens and the like.

## Floccus

This is something I'll be talking about, but this addon allows me to sync my Nextcloud Bookmarks into my browser. Once this is enabled and I setup my account, I am able to access them from Vivaldi's native bookmarks tab.

# Bookmarks

Since this required the above Floccus plugin, I wanted to save this until after to write about. Once that is installed and setup (almost everything is default), we can get going on some of the cool things.

## Settings

In Vivaldi's settings, I have the bookmarks set up to "Show Bookmark Bar", but have it positioned on the "Bottom". Having Floccus already installed, I can use "Nextcloud" as my "Select Bookmark Bar Folder".

## Bookmark Settings

Alternatively, if you set this up after installing your bookmark bar, it's probably easier to open up the bookmark button on your left-hand panel in order to set a couple of things. Right click on the 'Nextcloud' folder and:

1. Use as Speed Dial
2. Set as Bookmark Bar Folder

You should see the check boxes down at the bottom select as you do. I guess you could also use those as a substitute for the above, but that's not where my mind went initially. If I wanted to, I could also select a sub-folder to be my main bookmark squeeze, depending on the type of machine (work/personal/other) that I was using.

Also, in order for this to be the only thing on Speed Dial, go ahead and open a new tab. On the top where it shows "Speed Dial", right click and select "Delete". Then it will default to your bookmarks for your New Tab page.

Lastly, in Speed Dial, I remove the options to "Show Delete Button" and "Show Add Button" so there isn't any cruft on that screen.

# Conclusion

Well, I think that's enough for now. There's no other secret sauce; or if there is, I've forgotten it already. Feel free to let me know if this page needs updating as time goes on, but this has been fairly good to me for a while now. I hope you can get something out of it too!
