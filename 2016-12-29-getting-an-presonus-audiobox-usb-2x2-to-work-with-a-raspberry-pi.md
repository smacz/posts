---
layout: post
title: "Getting an PreSonus AudioBox USB 2x2 to work with a Raspberry Pi"
date: 2016-12-29
category: HomeLab
program: ALSA
process: Config
repo: 
image: presonus.jpg
description: "I got new studio monitors for Christmas, and picked up a used PreSonus AudioBox USB 2x2 to drive them. It worked fine with PulseAudio on my Lenovo, but it took a bit more finagling to get it to work on the Pi."
references:
  - title: Power Consumption of a USB Device
    link: https://unix.stackexchange.com/questions/81508/get-power-consumption-of-a-usb-device
  - title: Enable 1.2A Per USB Port on a Raspberry Pi
    link: https://www.raspberrypi.org/forums/viewtopic.php?p=594183#p594183
  - title: Set USB Card as Default Audio Device
    link: https://raspberrypi.stackexchange.com/questions/39187/alsa-base-conf-file-missing/39230#39230
  - title: JBL 3 Series Studio Monitors
    link: http://www.jblpro.com/www/products/recording-broadcast/3-series
---

# The Current

The first time I plugged the AudioBox into the Pi I heard a popping noise - a rhythmic one, and the lights on the PreSonus were flashing in time to the popping. Looking at `/var/log/messages` I found:

```
[  398.499442] usb 1-1-port2: over-current change
[  398.755655] usb 1-1-port2: over-current change
```

Looking around a bit, this meant that the usb port was trying to draw more power than the OS would allow it. Luckily, since I had a 2A charger, I could afford for it to supply a bit more power to the PreSonus - hopefully enough

## What was enough

As a sidenote, I wondered just how much "enough" was going to be. Like I said, it worked when it was plugged into my laptop, so I figured I'd see how much it pulled when it was running. This turned out to be pretty easy.

```
$ lsusb -v | grep presonus -A 24
```

This gave me the range that included the setting for "MaxPower" and I was able to see exactly how much I needed. It was reported that 500mA were being drawn from the usb port, but that's _right_ on the edge, so I'm not sure if I would trust it. Also, the Pi (Model B+) can supposedly put out 600mA per port without any additional configuration. Either way, I _did_ end up needing to append a line to the `/boot/config.txt` file. It was quite simple:

```
max_usb_current=1
```

That did the trick, and now the device was recognized, but had yet to work correctly.

# ALSA

Luckily for me, the driver for the PreSonus was already in the kernel, and it recognized everything right off the bat. (`aplay -l` showed that `card 1` was the AudioBox) However, `alsamixer`, when I pressed `F6` and chose the USB sound card, it displayed the notification:

> This sound device does not have any controls.

Which is disappointing. On my laptop, bringing up `pavucontrol` and setting that works, but that's because I have PulseAudio on my laptop. No so with my Pi, and I want to keep it that way. I want my options open when I start configuring a `mpd` server.

Also, running `speaker-test` does not produce any output either. However, that's because it's using the default device. Running it like the following _does_ produce output (white noise in fact):

```
$ speaker-test -D default:USB
```

This was determined by looking at `aplay -L`'s output, especially the line:

```
default:CARD=USB
    AudioBox USB, USB Audio
    Default Audio Device
```

And to force `mpv` to use that audio device, I invoked it straight from the command line:

```
$ mpv Acid_Wolfpack.mp3 --ao=alsa:device=[default:USB]
```

It sounds awesome BTW.

## The Conf File

In many older tutorials, there is a `/etc/modprobe.d/alsa-base.conf` file that's mentioned a lot. Only, in the Model B+, that got changed to `/usr/share/alsa/alsa.conf`. The relevant lines that need to be changed in this updated file are:

```
defaults.ctl.card <card #>
defaults.pcm.card <card #>
```

It doesn't seem that it is able to be hot-swapped by any ALSA program, or at least, I solved it before I tried the one that worked.

#### The `card#` is the one that is showing when pressing `F6` in `alsamixer`

And reboot. After that, I got sound coming through loud and clear. I tell you, these things really sound amazing.
