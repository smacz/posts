---
layout: post
title: "Conky Config 1"
date: 2015-01-21
category: Battlestation
program: Conky
process: Script
description: "Conky becomes transparent without any compositing!"
---

# I didn't believe that it was possible

So I, all these years, never knew that conky could be transparent without a
composite windows manager. Now thanks to the #! forums, I know better.

## draw-bg.lua

I copypasta'd the code that *easysid* has posted in May of 2013 on "The New
Monster Conky Thread" and made it just a plain old hidden file in my home dir.
Next I edited my conkyrc to the specs that the intro to the script said that it
required. I also added

	own_window_Class Conky
	own_window_transparent yes
	background no

Just because the conkyrc scripts had it on there as well. I'm sure I'll be
fiddling around with that soon enough.

BTW, the second line if you're doing if without a compositing WM needs to
reference `draw_bg`, not whatever you saved the file as (like in the top line)
and without the extension.

I put the color to 0x002b36 per Ethan Schoonover and the transparency to .75,
like my terminals and active workspace. It seems to work just fine. Now, I want
to make it the entirety of my screen, and not cut off at the bottom.

## Resizing

I removed my shortcut reminders from conky because if I haven't internalized
them by now...let's just say I have. Also, I resized it so that it would fit
flush with tint2, and upped the font to 11pt. 

## Multiple Conkys

Since I know that I want multiple conkys (and have known for awhile) I will just
follow the [crunchbang wiki's
page](crunchbanglinux.org/wiki/howto/howto_setup_multiple_conky_sessions) on how to do this and get it going so that I
can start making my desktop MY desktop.
	
I plan on creating a conky for each of these:

* Logs
* Networking
* Disk Usage
* Processes
* Feeds
* Music Player
* Environment
* Entropy/Sys Info
* TODO
* Workout

For now I'm just going to log out and back in to see if the autostart works.

The autostart is fine. I'm going to have some problems with the size of these
windows, I guarantee it. For the time being, I am going to just try to get the
all approximately where I want them, and then try out different scripts on them.
Let's see what happens.

## Size Allocation

So basically, this conky window won't shrink to less than it was originally. I
had taken out several components so that it only reflected things pertaining to
disk usage, but it refuses to shrink. I also adjusted `minimum_size` to all 
sorts of possible numbers, small, zero, and negative. Even trying to take it out
entirely. No luck.

Let me logout and log back in. I have it set to zero currently.

HUH! That worked. Cool. 

In order to accomodate tint2, I have set the `gap_y = 32` which I'm assuming is
in pixels, or something...either way, it works, so anything that is anchored to
the top (except sys as it is between the right-most end of tint2 and the edge of
my screen) will be down 32 in the `gap_y`.

This one gave me a little bit of trouble, I had to find out that conky defaults
to only 256 characters in the text buffer, so setting:

	text_buffer_size 512

worked like a charm. There's no practical restriction on this number, '512' was
M
just the example given at stackexchange, and it works for me for the time being.

#### Lessons

* To align something to the middle is to align it to the 'center'.

* Entropy is in 'bits'.

* Crunchbang's Logo is in FreeSans font.

* To vary the font size:

	${font size=XX}foo bar${font}

 Make sure the ending `${font}` is there, otherwise the rest of the text is
affected. Also this works for italics/bold and typeface.


