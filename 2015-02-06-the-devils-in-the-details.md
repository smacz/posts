---
layout: post
title: "The Devil's in the Details"
date: 2015-02-06
category: SysAdmin
program: FOSS
process: Metacognition
description: "Put yourself into perspective."
---

## I wouldn't have been able to exploit Shellshock

There is no way that I would've been able to see that vulnerability. That is
just way too advanced for me. Even when I got the dumbed-down version reported
to me, I didn't understand exactly how it was executed, or the genius that
really went into exploiting it. As paranoid as I am, I can't audit everything,
and that frustrates me.

## What to do about it?

Cope.

Literally, that's all you can do.

There will be more Shellshocks. There will be more Heartbleeds. And I realize
that I will neither be the person reporting it, nor the one patching it. 

AND THAT'S OK.

That's really the beauty of open-source. I can rest easy that there are people
who dedicate themselves to this stuff, either because they rely on it for
living, dedicating themselves to it is their living, or simply for the love of
the game. I know I can rattle off why email is insecure and how to fortify it. I
know that I can tell you what algorithms are insecure to use in cryptographic
protocols. I know that I can secure Firefox using extensions and addons. And I
will play to my strengths.

## Have the community cover your weakn(ass)es

There will always be exploits and hacks that will be discovered. That's the
nature of the beast. Being part of a community though, you can rest easier
knowing that more finely-tuned minds are working on the problem for your
benefit. 

>With many eyes, are bugs are shallow.

Linus's Law. Chances are, any problems will be readily apparent to any number of
people. And further chances are, those problems will have a readily apparent
solution -- but maybe not to the ones who found the problem in the first place.

## I don't work on runtime-critical software

I just don't. I'm not even NEAR the expertise level that is necessary to fool 
around with the kernel or systemd. I leave that to better minds than mine...but
I will gladly make sure that they are able to sort their coding music
appropriately, or secure their email addresses exherting no more effort than
reading the documentation.

## The imposter complex

Many great minds suffer from the imposter complex. They may feel that they don't
know as much as the average penguin, but I'll tell you this. If you're active in
the community, you maintain an open mind to listen with, and contribute when and
where and what you can, then you are no imposter, you are a valuable asset. 

I know that I get frustrated some times when I notice a problem and can't do
anything other than report it. I feel bad not being able to even participate in
the diagnosement process. And I feel worse when I arrive late to the game and
the fix is already in the process of being distributed. But that's part of being
a community. No one can be on top of everything all of the time. 

## Play to your strengths

All I can do is what I know how to do. All I can contribute is my time, my
donations, and my voice. I am in this for the long haul; I rejoice at every
achievement, and lament at every setback. I know I am part of the community
because I feel with them.

Where do I fit in? Everywhere...that I'm willing to put in the time and effort.

What can I do? Anything...that I'm willing to put in the time and effort to do.

When should I do it? (Really? please...)

Volunteering is more intrinsic than anything. You know your strengths and your
ideas and knowledge, use them!

> Enrich yourself while simultaneously enriching others.

I would add one last part to that line:

> ...and they will enrich you.

## It doesn't matter

It really doesn't matter that you can't save everyone from the next disaster
(unless you're the one who can, in which case...SAVE US!!!). It really doesn't
matter that what you're doing may seem trivial. Evolution is made up of
trivialities. Do what you can with what you have. There's a chance to be a part
of something much much bigger than any one person, and I for one am glad that I
have others watching my back, so that I can watch theirs.
