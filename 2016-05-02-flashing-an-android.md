---
layout: post
title: "Flashing an Android"
date: 2016-05-02
category: Battlestation
program: Android
process: Install
description: "Messing around with my phone, VirtualBox, and Micro$oft Windoze. Because I broke it doing it the wrong way. Now I gotta fix it."
---

# Get Windoze image for Virtualbox

So, I have to use windoze in order to use odin. There's an open-source alternative called [Heimdall](http://glassechidna.com.au/heimdall/). Unfortunately, it only officially supports a few samsung phones and mine, the sm-g530t1, does not function properly with heimdall, as reported by others, and experienced by myself. Oh well.

Unfortunately, by using it I soft-bricked my phone (after backing up everything/anything important) so now I'm forced to experiment and learn something new. It's the _only_ way to learn.

Now, I don't want to pay for windoze, and luckily, there's a free demo that is available for developers that runs in virtualbox for testing against different IE versions. It only is valid for 90 days, however at any time, it can be rolled back to a snapshot to reset the 90 days. I mean, I don't plan on having it on my computer for long, but in case I need to do this again, or need windoze for another reason (unlikely) then I can just rollback to the snapshot. That being said, I'm going to take a snapshot before I start anything in order to have that ability.

There's a [ProTip post on Reddit](https://www.reddit.com/r/apple/comments/1iwhyt/protip_microsoft_provides_free_windows_vm_images/) that explains more, and gives the [link to the downloads](https://developer.microsoft.com/en-us/microsoft-edge/tools/vms/linux/). It takes a while to download the file, as it's ~3.5GB so it's prudent to download this before starting on the rest of the project. I downloaded the latest Win7 image.

# Install Virtualbox

I'm not going to rehash another's work, so [here's the instructions that I followed](http://www.if-not-true-then-false.com/2010/install-virtualbox-with-yum-on-fedora-centos-red-hat-rhel/). It was originally published in 2015, but has since been updated for Fedora 23 - which I'm running at the moment.

I followed the instructions verbatim, and didn't run into any problems. I guess there's a first time for everything. Just remember that most if not all of the commands need to be run under root or `sudo`.

# Install Virtualbox Extension Pack

Find out which version your virtualbox is (mine was 5.0), and visit the [Virtualbox Old Builds](https://www.virtualbox.org/wiki/Download_Old_Builds) site to find the extension pack. I had to spin up Virtualbox to find out (via the Help > About screen) which specific version I had, and then download the extension pack for that version. Gnome gave me the option to open it directly with virtualbox, and I was able to install it directly.

If you're running the most recent version (5.0.20 at writing time), the extension pack is located at the [main page](http://www.oracle.com/technetwork/server-storage/virtualbox/downloads/index.html#extpack)

# Running Windoze in Virtualbox

By the time virtualbox was setup and ready to be run, windoze had finished downloading. Opening virtualbox, I went to [File] > [Import Appliance] and selected my virtual disk image to import.

That took about an hour - keep in mind that was on a 1st Gen i3. So any computer newer than 7 years old should be quicker. But probably not much quicker.

4%

36%

78%

91%

And now I get to start.

## Adding Extension Pack

Making sure that the extension pack installed correctly is as simple as going to `[File] > [Preferences] > [Extensions]` and making sure that it is loaded. Otherwise, you can download the package, save it somewhere, and manually install it.

## Adding USB Device

Make sure that the windoze VM is powered off but selected, and go to `[Settings] > [USB]` and select '[X] Enable USB 2.0 (EHCI) Controller' to add the USB ports so that the VM can recognize your phone when it's plugged in.

Also, make sure that the Linux user's groups include `vboxusers` in order to give the correct permissions to access the phone.

## Adding Shared Folder

Under `[Devices] > [Shared Folders] > [Shared Folders Settings]` use the icon that looks like a folder with a plus sign to select the folder that contains the files that you want to use with ODIN. For my purposes, I used [kalbit87's 0F8_full](https://www.androidfilehost.com/?fid=24351979186683928) and [ShinySide's TrapKernel_G530T_AOF8_Root](https://www.androidfilehost.com/?fid=24052804347817375) tar archives.

Keep in mind that I had to unzip kalbit87's archive to get the PIT file and everything.

## Downloading and Installing Kies

Kies is required for Samsung devices (apparently) for the drivers for windoze. Yet another reason why I use linux. Anyways, it's available from [Samsung's Download page](http://www.samsung.com/us/support/owners/app/kies). I downloaded Kies 3 since this will be running Android 5.1.1 (IIRC) and I put that in that shared folder set up earlier.

That shared folder is located in windoze files, under `[Network] > [VBOXSVR]`. Next just run the `.exe` installer, and let it do it's thing. The windoze version that I downloaded only came with 512MB of RAM, so all my projects are going to be slow on this. I also chose to not run the app _just_ yet.

## Downloading and Installing ODIN

Per [Reddit](https://www.reddit.com/r/GalaxyNote3/comments/32x8pf/avoid_any_links_from_xda_to_samsungs_odin/), I was advised to not download Samsung's Odin installer from XDA. [/u/raceme](reddit.com/u/raceme) provided [the Android Central clean links page](http://androidcentral.us/2013/11/download-odin/) which directs to mega.nz files which I downloaded and put into the windoze shared folder.

It downloads in a `.rar` file. To unpack this to get the `.exe` file, use `unrar`. Downloading unrar requires the [rpm-fusion-nonfree repo to be set up](http://www.howopensource.com/2011/08/how-to-extract-rar-files-in-fedora-15/):

```bash
# dnf install http://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm

# dnf install unrar

$ unrar x Odin\ v3.10\ by\ Ohguideme.rar
```

## Adding Phone via USB

At the bottom of the VM, there is a staus bar with various icons. The USB icon is for...USB devices.

I'm not sure if this was strictly necessary, but my phone wasn't being recognized, so I blacklisted `cdc_acm` driver per previous instructions:

```bash
# modprobe -r cdc_acm
```

And edited `/etc/modprobe.d/blacklist.conf`

```
blacklist cdc_acm
```

And then reboot. Then it was able to find my phone.

# Flashing PIT, Firmware, Kernel, and Custom Recovery via ODIN

## PIT and Firmware

Opening ODIN, I put my phone into download mode by holding volume down and the home button along with the power key for about 10 seconds. Then I unplugged it.

Now, this isn't the right way to do this, but it's what I did. After installing the device driver software failed, I opened Kies. Going to `[Tools] > [Reinstall Device Driver]` I waited for windoze to do whatever windoze does, and then exited kies. Then I plugged the usb cable back in, and the auto driver software installation initiated again, and succeeded.

Then I unplugged it again.

Skipping a long DDGo search, I found out two things:

1. Take out SIM/SD cards
2. Connect phone _after_ starting Odin

From there, I put the `.pit` file into to `[PIT]` slot, and the `OF8.tar.md5` into the `[AP]` slot. I waited for it to be checked as OK, and then pressed `[Start]`

I'm going to wait to write that "everything went smoothly" until everything goes smoothly.

So everything went smoothly, and the phone rebooted twice - once for an "update" and a second for the actual reboot. I was greeted by a T-Mobile splash screen and went through the initial setup process.

## Kernel

After the setup was done, I closed Odin, powered off the phone, and unplugged the USB cable. Then I opened Odin back up, powered up the phone in download mode again and plugged it back in. Virtualbox made me re-add the usb drive responsible for the phone, and it appeared in Odin. This time I put the `TrapKernel*.tar.md5` file into the AP slot and clicked `[Start]` again.

This time it wasn't even five seconds until the phone rebooted, everything indicates that it went well. Yay.

#### NOTE:

I made sure to turn off `[Wifi Calling]` in the pull-down menu because there has been a major bug reported in that functionality. And I don't need it.

## Custom Recovery

This custom recovery does not allow full system backups or flashing large ROMs, but I only need the ability to flash zips for sideloading applications.

[There have been many requests for custom recoveries for this phone](http://forum.xda-developers.com/grand-prime/general/sm-g530t-search-to-custom-recovery-g530t-t3339027), but there has not been one yet that does these two things. Which is holding back development of custom ROMs, which is disappointing. But [the linked zip file](http://forum.xda-developers.com/grand-prime/development/custom-recovery-twrp-2-8-7-0for-lollipop-t3190351) _can_ be flashed to the phone in the following way.

Once again:

1. Close Odin
2. Power off phone
3. Unplug Cable
4. Power up Odin
5. Boot phone into download mode
6. Plug into computer
7. Register with virtualbox

Now we're back to basics. Now we can put the `.tar` file into the `[AP]` slot and press `[Start]` again. It takes like half a second and then successfully reboots. I guess flashing a phone is like painting - it's the setup that's the hardest part.

Now the phone can be booted into recovery mode by holding down volume _up_ along with the home button and the power button.

# Sideload SuperSU

So using TWRP I can sideload supersu. There's a whole long [thread](http://forum.xda-developers.com/showthread.php?t=1538053) on supersu with links to stable and beta to the `.zip` file.

Following the [XDA Sideloading Guide](http://forum.xda-developers.com/showthread.php?t=2559200) I used TWRP to install supersu. I don't know if this is strictly necessary, but when installing it, I initially copied it from the SD card that I put it on to the internal memory while in TWRP.

Then after rebooting the boot got stuck on the boot screen. I pulled the battery and rebooted into recovery, checked the files and saw that the supersu files were there, and booted into the phone. The program was there and working just fine. Everything's set. Now to install f-droid and go ham.


#### NOTE:

This is copied right from the terminal output:

```
******************************************************************
                       IMPORTANT NOTICES
******************************************************************
If TWRP offers to install SuperSU, do *NOT* let it!
******************************************************************
First reboot may take a few minutes. It can also loop a few times.
Do not interrupt the process!
******************************************************************
```

You've been warned.

# Conclusion

Well, this is _certainly_ not the end of my Android Adventures (hah!) and I am waiting with baited breath for the custom recovery that can make a full system backup and flash ROMs. But mainly make a full system backup to help the devs with testing their various ROMs. Man, I can't wait for CM and/or AOSP!

## TL;DR

[Reddit Summary of flashing process.](https://www.reddit.com/r/linux/comments/19kaic/using_odin_in_virtualbox_windows_guest_in_linux/)
