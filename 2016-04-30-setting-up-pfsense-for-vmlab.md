---
layout: post
title: "Setting up pfSense for VMLab"
date: 2016-04-30
category: HomeLab
program: pfSense
process: Install
description: "Setting up pfSense in a VM as a router for an internal VM-LAN network - named VMLab. My plan for my VM server is to create an internal network in which I can experiment and test applications and configurations."
---

## NICs and IP Addresses

For my interfaces when spinning up this VM, I did not use the NAT default, but instead set up two interfaces.

The first interface was a hostdevice macvtap. This is a bridge that binds the IP address of the VM to the ethernet port on my host machine. Now through that one ethernet connection, my router recognizes both the host as well as the VM. Since this is functioning as my VMLAB router, that's the only one that I will need to do this with during my VMLAB setup.

The second interface was a Virtual Network 'VM-LAN'. An isolated network, internal and host routing only. This was my LAN for all intents and purposes. The rest of my VMs would _only_ be connecting to this network, and the router would do the routing as needed.

To configure the IP addresses, the WAN (first) interface was set to the subnet that my TrappedInTheCloset router was using (192.168.2.0/24) and the second LAN interface I set up to use 192.168.3.0/24.

During the webconfig setup wizard (described below) I made sure to uncheck the "Block private networks" box so that it would not block my LAN IP addresses from connecting. That would be dumb if I did, seeing as this is all internal anyways.

## Webconfigurator

This is to access the webconfig on the WAN port.

First of all, you have to access the webconfig to allow access to the webconfig on WAN. Which is difficult if you are running it in a VM with no other hosts on that local network.

Luckily, as long as you correctly set your macvtap intercface up, and you can ping your box, you should be able to point your browser there after logging in to the remote console and disabling the pf firewall.

```
pfctl -d
```

Once that's done, type in the IP address of your pfSense VM and login. The default user is 'admin' and the password is 'pfsense'. I changed these at the end of the setup wizard that automatically runs using keepassx to generate a complex passphrase.

If possible, I would recommend setting up a static IP so that you don't have to worry about that changing. Although as long as you set up the DHCP hostname field, you should be able to find the box with that.

Keep in mind that once the setup wizard is complete, that pfsense turns the firewall back on. We have not set up the ability to allow WAN access to the webconfig yet, so we have to turn this off again and log back in.

>Access to the GUI is disabled by default for security reasons, and should be left that way or restricted to specific authorized remote management IP addresses.
>If HTTPS (tcp/443) is already being forwarded on the WAN IP address to an internal system, it may be necessary to change the port for the GUI in order to reach it from the WAN using HTTPS.
>To allow access from the WAN, navigate to Firewall > Rules on the WAN tab add a firewall rule to pass the appropriate source(s) to a destination of the WAN IP and port the webGUI is using. Passing from a source of "any" is highly discouraged for security reasons.
>*--[PFSenseDocs - How can I access the webGUI from the WAN](https://doc.pfsense.org/index.php/How_can_I_access_the_webGUI_from_the_WAN)*

So change:

* Interface -> WAN
* Source
    * Type -> WAN net
* Destination
    * WAN address
* Destination port range
    * from -> HTTPS (443)
    * to -> HTTPS (443)
* Description -> WAN webconfig access

Now Save and apply the changes. The firewall will now reload, but you _should_ be able to access the VM from your browser on the same LAN that the WAN interface is on now.

#### NOTE:

If you can't access the WebGUI, try putting `https://` before the IP address. Otherwise, if you fell back onto `http` for your webconfig protocol, swap `443` for `80` in the firewall rule. You can also change this in the `System -> Advanced` back to https. I also changed my port to `8888` in both that page as well as the firewall rule.


## Other setup options

Options worth taking a look at are:

* DNS
* DHCP (should be initially set up by the webconfig setup wizard)
* sshd
* IPv6
* Theme (for webconfig - I personally use "metallic")
* [And this](https://xkcd.com/350/)

# Static routing for downstream gateway

If you have a downstream gateway that isn't set up as a static route, and that isn't reached via the default gateway, any pings to or from that gateway won't be returned. For instance:

```
WAN Interface IP: 192.168.2.50
LAN Interface IP: 192.168.3.1
Default Gateway: 192.168.2.1
Downstream Gateway: 192.168.3.50
Downstream Gateway LAN IP: 192.168.4.1
```

A client in `192.168.4.0/24` won't be able to ping a client in `192.168.3.0/24`, because the LAN Interface IP (of the router) won't have a route _back_ to `192.168.4.0/24`. Therefore, a gateway needs to be established that specifies the Downstream Gateway _as_ a...gateway. Then a static route can be set up for it as well. At this point, routing can take place between the two subnets.
