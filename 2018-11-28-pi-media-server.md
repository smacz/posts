---
layout: post
title: "A pi-powered speaker setup"
date: 2018-11-28
category: SysAdmin
program: Mopidy
process: Install
description: "Hooking up my work Macbook air to my USB-powered speaker system _just_ wasn't cutting it..."
references: ""
---

# Service Summary


## Service Type: Hardware Server

# Servers

- pumpkin
    - Services:
        - `tornado`
    - Files:
        - Conf files in `/boot/config/settings.ini`
    - Subnet: `10.0.64.0/24`

# Firewall Rules

- firewall: pfsadmnw01bcto
  - rule: DNS name resolution
    interface: WAN
    port: 53
    src: 10.0.0.0/16
    dst: 10.0.32.0/24
    protocol: IPv4 TCP

# Accounts

## Admin groups

- `bind`

# Workflow

- Add entries in master DNS server
- Have entries propagate to slave DNS servers
- Load Balance DNS requests between slave DNS servers

# App Setup

## dnsmastin01fato.adm.opensource.osu.edu

```
$ sudo dnf install -y bind bind-utils policycoreutils-python-utils
$ sudo mkdir /srv/named
$ sudo cp -ar /var/named/* /srv/named/
```

Edit `/etc/named.conf`

```
options {
  listen-on port 53 { 127.0.0.1; 10.0.3.33; }
  listen-on-v6 port 53 { none; }
  directory '/srv/...'
  dump-file '/srv/...'
  statistics-file '/srv/...'
  memstatistics-file '/srv/...'
  allow-query { localhost; 10.0.3.33; };
  forwarders { 10.0.3.13; };
  recursion yes

logging {
    channel default_debug {
        file "data/named.run";
        severity dynamic;
    };
};

zone "." IN {
        type hint;
        file "named.ca";
};
zone "adm.opensource.osu.edu" IN {
        type master;
        file "/srv/named/adm.opensource.osu.edu";
};
zone "3.0.10.in-addr.arpa" IN {
        type master;
        file "/srv/named/3.0.10.db";
};
zone "dmz.opensource.osu.edu" IN {
        type master;
        file "/srv/named/dmz.opensource.osu.edu";
        allow-transfer { 10.0.1.33; };
        notify yes;
};
zone "1.0.10.in-addr.arpa" IN {
        type master;
        file "/srv/named/1.0.10.db";
        allow-transfer { 10.0.1.33; };
        notify yes;
};
zone "bld.opensource.osu.edu" IN {
        type master;
        file "/srv/named/bld.opensource.osu.edu";
        allow-transfer { 10.0.2.33; };
        notify yes;
};
zone "2.0.10.in-addr.arpa" IN {
        type master;
        file "/srv/named/2.0.10.db";
        allow-transfer { 10.0.2.33; };
        notify yes;
};

#include "/etc/named.rfc1912.zones";
#include "/etc/named.root.key";
```

Edit `/srv/named/3.0.10.db`

```
$ORIGIN 3.0.10.in-addr.arpa.
$TTL 86400
@       IN      SOA     dnsmastin01fato.adm.opensource.osu.edu. root.adm.opensource.osu.edu. (
        2018010301      ;Serial YYYYMMDDXX increment XX each time changed per day
        3600            ;Refresh
        1800            ;Retry
        604800          ;Expire
        86400           ;Minimum TTL
)

@       IN      NS      dnsmastin01fato.adm.opensource.osu.edu.

; Network
13      IN      PTR     pfsadmnw01bato.adm.opensource.osu.edu.

; Infrastructure
33      IN      PTR     dnsmastin01fato.adm.opensource.osu.edu.

; Applications

; Special
192     IN      PTR     jmphostsp01fato.adm.opensource.osu.edu.
193     IN      PTR     jmphostsp02fato.adm.opensource.osu.edu.
```

Edit `/srv/named/adm.opensource.osu.edu`

```
$ORIGIN adm.opensource.osu.edu.
$TTL 86400
@       IN      SOA     dnsmastin01fsdh.adm.opensource.osu.edu. root.adm.opensource.osu.edu. (
        2018010301      ;Serial
        3600            ;Refresh
        1800            ;Retry
        604800          ;Expire
        86400           ;Minimum TTL
)

                NS      dnsmastin01fato.adm.opensource.osu.edu.
; Network
pfsadmnw01bato  A       10.0.3.13

; Infrastructure
dnsmastin01fato A       10.0.3.33

; Special
jmphostsp01fato A       10.0.3.192
jmphostsp02fato A       10.0.3.193
```

All zone files have to be labeled with the `named_zone_t` SELinux context.

```
$ sudo semanage fcontext -a -t named_zone_t /srv/named/3.0.10.db; restorecon -v /srv/named/3.0.10.db
$ sudo semanage fcontext -a -t named_zone_t /srv/named/adm.opensource.osu.edu; restorecon -v /srv/named/adm.opensource.osu.edu
```

Firewall has to be open for `53/UDP` to the outside world to resolve outside addresses, and `53/TCP` for zone transfers.

```
$ sudo firewall-cmd --permanent --add-service=dns
$ sudo firewall-cmd --permanent --zone=public --add-port=53/tcp
$ firewall-cmd --permanent --remove-service=dhcpv6-client
$ sudo firewall-cmd --reload
$ sudo chown -R root:named /srv/named/
$ sudo chown -R named:named /srv/named/{data,dynamic,slaves}
$ sudo bash -c 'for i in data dynamic slaves; do semanage fcontext -a -t named_cache_t /srv/named/${i}; restorecon -v /srv/named/${i}; done'
$ sudo systemctl start named
$ sudo systemctl enable named
```

## dnsslavin01fdto.dmz.opensource.osu.edu and dnsslavin01fbto.bld.opensource.osu.edu

```
$ sudo dnf install -y bind bind-utils
$ sudo mkdir /srv/named
$ sudo cp -ar /var/named/* /srv/named/
$ sudo chown -R root:named /srv/named/
$ sudo chown -R named:named /srv/named/{data,dynamic,slaves}
```

#### NOTE: SELinux does not run inside of containers.

Edit `/etc/named.conf`

```
options {
    listen-on port 53 { 127.0.0.1; 10.0.3.33; }
    listen-on-v6 port 53 { none; }
    directory '/srv/...'
    dump-file '/srv/...'
    statistics-file '/srv/...'
    memstatistics-file '/srv/...'
    allow-query { localhost; 10.0.3.33; };
    recursion yes

logging {
    channel default_debug {
        file "data/named.run";
        severity dynamic;
    };
};

zone "." IN {
        type hint;
        file "named.ca";
};
zone "dmz.opensource.osu.edu" IN {
        type slave;
        masters { 10.0.3.33; };
        file "/srv/named/slaves/dmz.opensource.osu.edu";
        notify no;
};
zone "1.0.10.in-addr.arpa" IN {
        type slave;
        masters { 10.0.3.33; };
        file "/srv/named/slaves/1.0.10.db";
        notify no;
};

#include "/etc/named.rfc1912.zones";
#include "/etc/named.root.key";
```

Firewall has to be open for `53/UDP` to the outside world to resolve outside addresses, and `53/TCP` for zone transfers.

```
$ sudo firewall-cmd --permanent --add-service=dns
$ sudo firewall-cmd --zone=public --add-port=53/tcp --permanent
$ sudo firewall-cmd --reload
$ sudo systemctl start named
$ sudo systemctl enable named
```

When the slave(s) and the master is set up, then reload the slave and the server, and watch the logs for a successful zone transfer:

```
# systemctl reload named
# journalctl -xefu named
```

# Post-mortem notes

Make sure to change the `DNS1` entries in the `/etc/sysconfig/network-scripts/ifcfg-eth0` to themselves on the bind servers once they are set up, so that they can use ldap and all other infrastructure services.

