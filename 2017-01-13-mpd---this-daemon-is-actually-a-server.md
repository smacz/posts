---
layout: post
title: "MPD - this daemon is actually a server"
date: 2017-01-13
category: HomeLab
program: MPD
process: Install
repo: 
image: jbl-3-series.jpg
description: "Repurposing my old mail server in conjunction with my old backup server gives me two servers for two sets of badass speakers. Now my whole house is blasting Pretty Lights...jealous?"
references:
  - title: Enable RPM Fusion on your system
    link: https://rpmfusion.org/Configuration
  - title: Command line mp3 player
    link: https://lists.centos.org/pipermail/centos/2015-May/152409.html
  - title: How to change date, time timezone on CentOS 6
    link: https://www.namhuy.net/2435/how-to-change-date-time-timezone-on-centos-6.html
  - title: Linux Questions Wiki - Cronttab
    link: http://wiki.linuxquestions.org/wiki/Crontab
  - title: How to configure and install << OPUS >> on CentOS
    link: http://thelinuxfaq.com/382-how-to-configure-and-install-ffmpeg-ffmpeg-php-on-redhat-centos
  - title: Playing OGG files with mplayer
    link: http://www.patrickmin.com/linux/tip.php?name=mplayer_ogg
  - title: Install ffmpeg v1.0+ on CentOS
    link: https://www.lowendtalk.com/discussion/64946/ffmpeg-centos-install-v1-0
  - title: MPV.io
    link: https://mpv.io/installation/
  - title: WAF.io
    link: https://waf.io/
  - title: Raspberry Pi Documentation - SSH
    link: https://www.raspberrypi.org/documentation/remote-access/ssh/
  - title: mpd init script
    link: http://forum.slitaz.org/topic/mpd-init-script
  - title: Build-in HTTP streaming
    link: http://mpd.wikia.com/wiki/Built-in_HTTP_streaming_part_2
  - title: Cannot connect to remote mpd
    link: https://www.linuxquestions.org/questions/linux-software-2/cannot-connect-to-remote-mpd-935651/
  - title: MPD over RTP over PulseAudio
    link: https://www.raspberrypi.org/forums/viewtopic.php?f=38&t=25684
  - title: Synchronizing mp3 playback
    link: http://snarfed.org/synchronizing_mp3_playback
  - title: Raspberry Pi and MPD Sound System
    link: https://swordfischer.me/2013/11/09/Raspberry-Pi-and-MPD-Sound-System/
  - title: Install Newsbeuter on Linux
    link: http://ask.xmodulo.com/install-newsbeuter-linux.html
  - title: How to install gcc-4.7+ on CentOS6
    link: https://superuser.com/questions/381160/how-to-install-gcc-4-7-x-4-8-x-on-centos/542091#542091
---

#### The second part of this is for CentOS 6, which I don't recommend that anyone install this on going forward.

1. Install a whole bunch of progs
2. Install nfs rpm for opensuse
3. `./configure` and check all intended options are enabled
4. Add firewalld commands

# "The Server Previously Known as Mail"

So I had to remove all of the NFS and postfix/dovecot stuff that I had set up on the old system; namely:

```
service rpcbind stop
service squid stop
service dovecot stop
service postfix stop
chkconfig rpcbind off
chkconfig squid off
chkconfig dovecot off
chkconfig postfix off
```

I honestly have no freakin' clue why `squid` was running. I took a look at the conf file, and it was the default conf file. But anyways, I turned off everything else. `rpc.statd` was still lingering on an external port, but after a reboot, `netstat -plant` showed that only `cupsd` and `sshd` were listening. `sshd` I need, and CUPS is only listening to `localhost`, so that doesn't concern me any. Reboot, and onto the next step.

# Alsa

After I plugged in the AudioBox (mixer), I found that alsa recognized it, I just had to set it as default. Again, as in the previous post, this was simply:

```
/usr/share/alsa/alsa.conf
---
defaults.ctl.card <card #>
defaults.pcm.card <card #>
```

## Test

There had to be a test after the reboot, and my player of choice - `mpv` - was nowhere to be found on centos. Literally, I am at their github right now trying to figure out how to compile it. That'll come later though. For the time being, installing `epel-release` and enabling RPM Fusion (below) and then installing `mplayer` gives me a usable demo. That is:

```
# mplayer -vc null -vo xv music_file.mp3
```

At that point the video is disabled and I can verify that the audio-out works. I'm sure that `speaker-test` would have done fine also, but I was getting tired of _not_ listening to music.

# Alarm Clock

Let's set a crontab alarm clock for myself using this.

```
# cronttab -e
---
30 09 * * * mplayer -vc null -vo xv /srv/mpd/SundayAfternoon/*/*/* -shuffle
```

This plays the shuffled version of my mornings.

```
# ~/.bashrc
---
alias snooze="pkill mplayer && sleep 5 && mplayer -vc null -vo xv /srv/mpd/SundayAfternoon/Slightly_Stoopid/Slightly\ Stoopid\ Meanwhile...Back\ At\ The\ Lab\ 2015/*"
```

And this lets me put the water on for coffee before I start getting to know Slightly Stoopid's latest album.

#### You never want to play the same song for your alarm clock. No matter how much you "love" it, you will forever associate it with the pain of waking up and therefore have a visceral negative reaction whenever that song (the one you used to "love") plays.


# FFmpeg

My favorite program - `youtube-dl` - kept complaining that my version of `ffmpeg` was less than 1.0. And it was. So I decided to compile it from source.

First, I had to uninstall `ffmpeg`, as I didn't want any conflicting libraries. Building it after that was pretty simple. Following [this guide on lowendbox]({{ references[7].link }}) I only ran into a couple problems.

## ffmpeg_build folder

Not necessarily a problem, but the directions state that most of the prefixes should be `/usr/local/ffmpeg_build`. This is all fine and good for building ffmpeg, but when I need those same libraries to build other programs (see `mpv` below) the aren't recognized there. So if I had to do this again, I might just leave the prefix as only `/usr/local`, that way the files get deposited in their system default places.

## libvpx

When I got to the stage at which I was to compile libvpx, it wouldn't build. It took _forever_ and then would error out regarding some bit of the code. I decided to leave it.

## ffmpeg

Apart from those two annoyances, the process was fairly smooth. When it was finally time to compile ffmpeg, I had to exclude the `--include-libvpx` statement, as per above, it wouldn't build. After starting the make, I hopped on IRC and watched an episode of [The Librarians](https://en.wikipedia.org/wiki/The_Librarians_%282014_TV_series%29) because it took even longer than libvpx to compile. Fortunately, it worked correctly the first time, and I then had a workable version of ffmpeg after `make install`.

# MPV

MPV was a pain in the ass for me trying to get it to work. First of all, the version of `git` that was on this machine didn't recognize the `--detatch` option to clone. So `mpv`'s [build-scripts](https://github.com/mpv-player/mpv-build) failed out on me.

## Python Portability

So I cloned `mpv` from github and launched `./bootstrap.py` - but that failed too! That command is meant to download `waf` - which seems to be just another way to compile code. It complained of a `.format()` call to a print statement. Of all the...

Anyways, I changed the format of the print statement. After this, it worked just fine:

```
print("String to {}...".format(WAFSTRING))

---

print("String to %s..." % (WAFSTRING))
```

## Compilation

Once `waf` was downloaded, the instructions just stopped. I did some research on the build process, and it was pretty straightforward. While in the base of the project after installing `waf`:

```
# ./waf --disable-libass configure && ./waf build && ./waf install
```

### libass

That `--disable-libass` part was because I hadn't build libass, and it was only for subtitles and captions anyways. I have no intention of hooking this up to a screen (ever) so I'm not concerned with that.

### ffmpeg headers

As I said earlier about choosing the prefix for `ffmpeg` and it's files, I had to move the files in `/usr/local/ffmpeg` to `/usr/local`. In that directory is `lib` and `includes`. After merging the programs contained in those two folders together under `/usr/local`, I no longer got any complaint about not being able to find the development headers.

## Invocation

Unlike `mpv` on my arch install, if the video out fails and it wasn't run with `--no-video`, `mpv` fails. Therefore, I've aliased it to itself in my `~/.bashrc`.

```
alias mpv="mpv --no-video"
```

Also, if I invoke it with a bad path or another obvious failure, the shell that I'm in refuses to print any more characters to the screen on the command line. It will show output, and reply to `Enter`, but it will not show what command I'm typing. Thank god for autocomplete I guess.

# MPD

https://www.cyberciti.biz/faq/howto-install-c-cpp-compiler-on-rhel/

https://github.com/loli10K/scripts/blob/master/centos-mpd-install.sh

There are two port specifications in the conf file. One is at the root level, and the other is inside of an `audio_output` list. The port inside the list (whose type would most likely be `"http"`) is for the stream, but not the control. The control - which ncmpcpp and MPDroid can connect to in order to change the tunes, etc, is on the first specified port. The example `mpd.conf` has it towards the top. Make sure to set the IP address to localhost (or the IP address if you're going to connect to it remotely) and the port to 6600 per convention. The stream has been displayed on the internet almost exclusively as on port 8000.

# Raspberry Pi

## SSH

Since Nov. 2016, SSH is disabled on the stock Raspbian distro. I needed to do two things to make it work on boot, since I run this headless. First was to `touch ssh` in the `/boot` directory of the filestructure. This turns SSH on. Then I needed to generate keys, as they are not generated initially.

```
$ ssh-keygen -b 4096 -t rsa -f <Raspberry Pi FS>/etc/ssh/ssh_host_rsa_key
```

And then a `ssh-copy-id` to set up SSH keys.

## Wireless

The trick was to change the password so that it didn't have quotes in there and put that in the `/etc/wpa_supplicant/wpa_supplicant.conf` file. In the `/etc/network/interfaces` file I had to use use `dhcp` instead of `manual` and `wpa_conf` instead of `wpa_roam`.

# Newsbeuter

## make

```
vim config.mk

- LDFLAGS+=-lncursesw
+ LDFLAGS+=-lncursesw -ltinfo
```
