---
layout: post
title: Usability Configs for Iceweasel
date: 2015-02-22 
category: Battlestation
program: Firefox
process: Config
description: Iceweasel gets a makeover due to some strategic configuration settings.
---


## What is a browser, *really*?

So browsers are INCREDIBLY insecure. I like to view them as a portal to the internet, but not as, say, ssh or wget is. No, this is a portal that has to anticipate all use-cases, and basically handle anything that any web-dev has to throw at it. And trust me, they have a shit ton of weapons in their arsenal. Not only does it need to render everything flawlessly, it has to do it securely and quickly. Now, in all practicality, no tool exists that is perfectly flexible, secure, and fast; it's always a compromise between those three factors. You, however, can help Iceweasel by making a few small sacrifices, something a mature person should have no problem doing. And in doing so, you'll secure yourself, you'll secure the web, and you'll promote responsibility world-wide. Think I'm overexaggerating? Check out how much revenue comes out of web advertising and scamming every MONTH. Even the playing field with your knowledge.


## Peace-of-Mind vs Ease-of-Use

These two may seem to always be at odds with each other, and to a point it's reminiscent of the ongoing struggle of cryptographers vs cryptoanalysts, never seeming to end in this cat and mouse game, but they can occasionally play nice with each other. It's like I said, it taks a little sacrifice. But there are ways to work around it, and the time invested upfront will lead to a mutually beneficial compromise between security and user friendliness. Keep that in mind while I outline these following ideas.

Keep in mind, before (but mainly after) installing/settings these addons/configs/whatever-this-morphs-into, *Read The Furnished Material*. There may be functionality out the wazoo that you're not aware about because you were too excited to get started searching for the latest celeb gossip. Don't be that person. RTFM.


## Extra Credit

Read through the 13 pages on [TweakGuides.com](http://tweakguides.com/Firefox_1.html)

(It is meant for Windoze users, but is applicable in most respects to Linux users also. Well, page 4 and beyond anyways.)


# Settings

I *love* Jack Wallen's writings, and he did a piece for techrepublic awhile ago that is still mostly applicable today. This entry is mostly based off of that article and the maketecheasier one as well.

* [Jack Wallen](http://techrepublic.com/blog/10-things/10-handy-firefox-aboutconfig-hacks/)

* [Make Tech Easier](http://maketecheasier.com/28-coolest-firefox-aboutconfig-tricks/)

## Speed up firefox

Millenials like speed, but we feel that most of "Speeding up X" is a gimmick.  Well this one's not, so let's implement this. I'm starting with the hardest first, so just bear with me, we'll get this started.

1. network.http.pipelining "True"
2. network.http.proxy.pipelining "True"
3. network.http.pipelining.maxrequests "32"
4. network.http.max-connections "256"
5. network.http.max-persistant-connections-per-server "32"

See? not so hard. Some of those will be preset. They've made the default values higher over the years due to advancing technology so yours may already be fast.  But this is just a small tweak to ensure that it's the fastest it can be.

## Default Font/Color

In the "Preferences" dialog is a "Content" tab that lists the Fonts & Colors.  The font is pretty straight-forward, listing all of the fonts installed on your system and left up to your choosing. Also set the size at this point. I'd suggest having (a couple of) your favorite site(s) up in the background for a live demo. Mess around with the advanced options if you have any preference. For instance, I always prefer to read monospaced fonts, so I default to a monospaced font (Liberation Mono for the time being as it is quite ubiquitous). 

For the colors, you do have to be somewhat careful. This is where you can impersonate a "Iceweasel Dark Mode", of which Iceweasel does not actually have. 

If you have a good GTK theme, I strongly suggest that you select "Use system colors" as it makes Iceweasel much more coherant, and in tune with the rest of the defaults in it. Also, if you uncheck the ability for the pages to choose their own colors, it will force any page you view to be compliant. However, this may come at a cost of obfuscating some text based on the setup of any given websites. One again, test out your favorites to see if that will work for you.  Also, the "Link" and "Viewed Link" options will have to be set by you, so choose something that goes well with your colorscheme. I chose a matte green and orange that goes well with my #! grey. Like anything else here...experiment!

## Multiple home pages on startup

If you are like me and live in a couple different communities online, you can have your must-see sites popup every time you open Iceweasel.

Under "Menu" --> "Preferences" --> "General" (tab) It should say:

> Startup
>   When Iceweasel starts:

Choose "Show my home page". That will obviously show whatever page that you put in the input below that. But how to put in multiple addresses? Easy, separate them with a pipe, aka the `|` button above the "Enter" key (at least in the US). One example would be:

```
crunchbang.org/forums/search.php?action=show_recent|reddit.com/r/linuxadmin
```

That pulls up the recent threads on the #! forums, and the linuxadmin subreddit in separate tabs for each new browsing instance.

## Always ask to save files

While we're in that same menu, under the same tab is the "Downloads" option. I find it incredibly aggrivating if, when I download a file or what have you, I have to move it from `~/Downloads` to wherever it needs to be. I prefer to set this option to "Always ask me where to save files" so that I can specify where I need that file to go and save myself a step that I don't have to waste.

If you're one of those people who *like* having all of their content that was gleaned from the interwebz to be all in one folder, keep in mind that you can at least change the folder from `~/Downloads` if there's a better place for your default to be. 

## Spellcheck

In order to have spellchecking in input fields, find `layout.spellcheckDefault,` and set the value to `2`. Now you are less likely to be flamed for your spelling. 

Iceweasel takes no responsibility for flamewars based on your wrong opinions though.

## Open search bar results in new tab

Instead of having the results from the search bar overwriting whatever you were looking at by showing results in the current tab, change `browser.search.openintab` to `True`.

## Browser cache

In `about:config` there are a couple entries starting with `browser.cache`.  Experiment here. Some functionality on the web breaks if you set the cache to zero, but I'm sure it doesn't need over 100MB to just store running instances.  It should be wiped with Private Browsing after every session anyways. 

Also, google how to delete it and search for the most recent article. It seems like it changes frequently. Read up [here](https://support.mozilla.org/en-US/kb/how-clear-firefox-cache) about that.

## Disable download delay

Whenever a download of a PDF or Addon is started, a popup box, well, pops up asking you if you're sure you want to install/download this. Except that the button to accept is greyed out for the first five seconds after the box is popped up. That is way to long for a millennial like me to wait. So I changed `security.dialog_enable_delay` to `0` and bypassed the delay. 

## [Saving your SSD from extinction](https://www.servethehome.com/firefox-is-eating-your-ssd-here-is-how-to-fix-it/)

By default, Firefox can write (on average) `10G`/24hrs to your SSD! This will severely degrade the lifetime of your SSD if you allow it to keep doing what it's doing, as excessive writes will wear out the flash storage faster on the device.

This is caused by the ability to save tabs every interval of time. The fix is to simply include the interval of time that it saves the tabs to memory. Of course, if you don't care about saving any open tabs in the event of a crash, then you can disable this, however, for everyone else who just wants to increase the time of the save, a safe value to the `browser.session.interval` parameter would be something like 30 minutes instead of the default 15 seconds.

## That about wraps it up

Don't forget there are four parts to an iceweasel setup, and this is only one of them. The other three are crucial as well in order to get a reliable, well-functioning web browser up and running. 
