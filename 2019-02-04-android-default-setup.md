---
layout: post
title: "Android Default Setup"
date: 2019-02-04
category: SysAdmin
program: Android
process: Configure
image: phone.png
description: "My several apps have different ways to make sure they're portable across devices, and this is how"
references: []
---

# F-Droid

I use F-Droid as package management for the system that keeps me up to date with the upgrades for the applications that I use.

This is installed as an `apk` from their [official site](https://f-droid.org/). Then, all of the other apps listed below can be installed from it. It also keeps itself up to date (not unlike `pip`).

#### NOTE: To allow `apk` installs, you'll have to enable in the settings: **[Settings]** -> **[Device administration]** -> **[Unknown Sources]** -> **[X]**

## Configure

- Dark Mode:
    - **[Settings]** -> **[Theme]** -> **[Dark]**
- Include anti-feature apps (apps that have features that may not be fully respecting of users' freedoms)
    - **[Settings]** -> **[Include anti-feature apps]** -> **[X]**
- Set updates only over WiFi:
    - **[Settings]** -> **[Updates]** -> **[Over data]** -> **[X - -]**
- Automatically Fetch Updates:
    - **[Settings]** -> **[Updates]** -> **[Automatically Fetch Updates]** -> **[X]**
- Check for updates every 4 hours:
    - **[Settings]** -> **[Updates]** -> **[Automatic update interval]** -> **[- - - - - X -]**

### List of apps

Just in order to keep track of them here, this is the list of apps that I have set up on my homescreen in order top to bottom, left to right:

```yaml
TopRow:
    - TimeKillers:
        - AndBible
        - ChessWalk
        - RedReader
        - Minesweeper
    - Media:
        - Music
        - NewPipe
        - Voice
        - PodListen
        - Recorder
        - AudioFX
    - Credentials:
        - SQRLLogin
        - Bitwarden
        - FreeOTP+
    - Admin:
        - AFWall+
        - SuperSU
        - F-Droid
        - JAWS
    - DocumentReaders:
        - MuPDFViewer
        - Book Reader
HomeRow:
    - IRL:
        - Nextcloud
        - Kandroid
        - Bitcoin.com
        - OsmAnd
    - Utilities:
        - Clock
        - Calculator
    - Web:
        - Firefox Klar
        - NextcloudBookmarks
        - QRScanner
    - Communication:
        - Messaging
        - Riot.im
        - Phone
        - DAVx5
        - FairEmail
```

## Backup

So this is kind of meta - in order to list your apps, you need to install an app. F-Droid has a Share feature, but it shares the raw CSV data of the list of the packages installed, which is not good to upload to Nextcloud. The [List My Apps](https://f-droid.org/packages/de.onyxbits.listmyapps/) app spits out a file that you can upload to Nextcloud.

## Restore

It's probably best to install the Nextcloud app right now and login to be able to access all of the files that you've previously backed up to your instance. F-Droid does not have an import feature yet. If you're adventurous, you can probably use [fdroidcl](https://github.com/mvdan/fdroidcl/), but as long as there are not too many apps that you use installed, you'll probably want to do it manually.

# Podcasts

I use [podlisten](https://github.com/einmalfel/PodListen) for a podcast app. It is able to have its URLs backed up onto a file using the OPML format, which most podcast apps will take. All I do is periodically throw this up in nextcloud if I make any changes. Otherwise, it's a fairly static configuration.

## Configure

This will take up a lot of storage if you let it. If you don't have an external SD card, you'll want to go easy on these settings:

- Speed up downloads:
    - **[Preferences]** -> **[Max Parallel Downloads]** -> **[5]**
- Auto Download Episodes:
    - SD-Card: **[Preferences]** -> **[Episode Auto-Download]** -> **[All new episodes]**
    - No SD-Card: **[Preferences]** -> **[Max Parallel Downloads]** -> **[Episodes added to playlist]**
- Refresh Subscriptions fairly often:
    - **[Auto-refresh subscriptions]** -> **[Every 3 Hours]**

## Backup

Backing up is exporting the list of subscriptions. This is done by **[Preferences]** -> **[Export to OPML]**. That exports it to a pre-determined local file, and that can be synced up to Nextcloud.

## Restore

The Nextcloud app _syncs_ files, and stores them under `/storage/emulated/0/Android/Media/com.nextcloud.client`, and then select your account and walk down the file structure from there. It's actually quite handy. Either way, it's easy to work with finding the OPML file that you backed up to.

# DAVx5

Contacts use _CardDAV_ to sync with your Android phone. This requires an app, as the CardDAV functionality is not built in. The app that I use is [DAVx5](https://gitlab.com/bitfireAT/davx5-ose) (formerly DAVdroid), which Nextcloud works excellently with.

Setting it up is easy using Nextcloud. In the Nextcloud app's **Settings**, you'll see "Sync calendar & contacts". As long as your Nextcloud instance has both of those apps set up, it's as easy as hitting that button and putting in your password to your Nextcloud instance.

If you don't want the automatic setup (because you're learning about this stuff, or just too good for automation), then it's handy to note that the path after the domain name is `/remote.php/dav` when prompted. This can additionally be after any subdomain or subdirectory that is needed.

## Importing Local Contacts

If you've already got contacts, no worries! Once your Nextcloud account is set up, you have to go a little round-about, but you can certainly import them.

First _export_ your existing local contacts to a file. Remember where that file is.

Next, import them, and select your Nextcloud account to import them to. Then, assuming you left the defaults alone, Nextcloud will sync them up and you can now access them from anywhere!

## Save to Nextcloud as default

It is recommended to save your contacts by default to your Nextcloud account if you make new ones. This can be done by going into your **Contacts** app, and its **Settings** menu, and selecting your Nextcloud account for the "Default Account for New Contacts" setting.

There is an [official KB article](https://www.davx5.com/faq/existing-contacts-are-not-synced) if you need screenshots.

# Maps

## Configure

After installing the `osmand` maps app, you will have to download the global map, and then your local map that you want to actually use. This should be prompted for at the beginning of the install. For me, it takes just under 350MB for both the World overview map, and my local state map. YMMV.

- Dark Mode:
    - **[Menu]** -> **[Map rendering]** -> **[Map mode]** -> **[Night]**
    - **[Menu]** -> **[Settings]** -> **[General Settings]** -> **[App Theme]** -> **[Dark]**
- Always keep map in portrait mode:
    - **[Menu]** -> **[Settings]** -> **[General Settings]** -> **[Screen Orientation]** -> **[Portrait]**
- GPX Files:
    - **[Menu]** -> **[Configure Map]** -> **[GPX Files]** **[X]**
- Activate the screen when driving and 20 seconds out from a turn:
    - **[Menu]** -> **[Navigation]** -> **[Car]** -> **[Turn Screen On]** -> **[20 seconds]**
- Turn off announcements of mundane things:
    - **[Menu]** -> **[Navigation]** -> **[Car]** -> **[Announce]** -> **[[X] Street names]**

## Backup

Share the Favorite Locations file to Nextcloud like you would share it anywhere else.

**[Menu]** -> **[My Places]** -> _Share Button_

#### NOTE: You'll probably want to overwrite the latest backup when saving as you'll probably just be _adding_ entries, and otherwise it'll save as `favorites (2).gpx`, `favorites (3).gpx`, etc...

## Restore

**[Menu]** -> **[My Places]** -> **[Import]**

This will allow you to select your Nextcloud account in Android's native file browser to make it easier to find that file.

# Bitcoin Wallet

## Configure

The Bitcoin.com app isn't on F-Droid yet, but can be installed from any of its [Releases](https://github.com/Bitcoin-com/Wallet/releases).

- Have cheap transactions:
    - **[Settings]** -> **[Bitcoin Network Fee Policy]** -> **[Super Economy]**
- Delete BTC Wallet:
    - **[Bitcoin Core Wallet]** -> **[Settings]** -> **[More Options]** -> **[Delete Wallet]**

## Backup

The backup, since we're using a service, is simply to [copy your 12-word backup phrase and secure it](https://blog.bitpay.com/private-key-security/).

I suggest putting it in your password manager, as that's...what it's there for.

Additionally, you have the option of backing up all of your wallet files, which would be great in the event that the service I'm using shut down tomorrow, and I happened to delete all of my installs right at that time, or all of my installs got wiped at that time. However, even I'm not that paranoid...yet.

## Restore

Here it's a simple restore:

**[Bitcoin Cash(BCH) +]** -> **[Import Wallet]** -> _<Enter Recovery Phrase>_

# 2FA

## Configure

## Backup

FreeOTP+'s backup is as simple as selecting **[Export]** in the main (only) menu of the app, and exporting it to a local file. That file can then be shared to Nextcloud. Done and done.

## Restore

The import is the same as OSMAND's restore, from the main menu, select the backup JSON file.

# Firewall

## Configure

We're only going to be dealing with rules, since the preferences backup is behind a paywall...

## Backup

AFWall+ exports a timestamped backup file to `/storage/emulated/0/afwall/` after hitting:

**[Menu]** -> **[Export]** -> **[Export rules]**

## Restore

Sync that file down and import it:

**[Menu]** -> **[Import]** -> **[Import rules]**

It's probably best to restore this after all of the apps are installed, but it works even if you only have a subset of the apps installed that it contained in the export.

# AndBible

## Configure

It'll first off prompt you to download a Bible from the internet. I chose ESV and KJV. Neither NIV nor NASB are available from the prompt.

## Backup

This is the most straightforward backup I've seen so far:

**[Menu]** -> **[Administration]** -> **[Backup]** -> **[Backup]**

## Restore

The submenu being **[Backup]** makes it somewhat odd for restorations, but you've already seen it if you took a backup.

The only kicker is that the new file has to overwrite the old one. So copy the file that was synced from Nextcloud on top of your old backup file. Then run the following:

**[Menu]** -> **[Administration]** -> **[Backup]** -> **[Restore]**

And answer 'Yes' when it asks if you need to overwrite it.

# Bitwarden

## Configure

For my self-hosted server, all I had to put is the URI that my bitwarden address is located on. For me that is `https://andrewcz.com/bitwarden`.

# Voice

## Configure

I had to set up the folder where my audiobooks were. `Voice` actually prompted me to do this. Since all of my audiobooks were on my SD card, it's easy to do this in between flashes.

# Login Apps

Lastly, there are a couple apps from my personal list that require me to log in to various services:

- K-9 Mail
- Nextcloud
- Nextcloud Bookmarks
- Riot.im
- Kandroid

The rest are good to go as-is!
