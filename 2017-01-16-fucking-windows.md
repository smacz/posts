---
layout: post
title: "Fucking Windows"
date: 2017-01-16
category: Battlestation
program: Windows
process: Install
repo: 
description: "Helping a poor soul install Linux"
image: xploded.jpg
references:
  - title: How to run CHKDSK in Windows 10
    link: https://windowsinstructed.com/run-chkdsk-windows/
  - title: Unable to mount windows NTFS filesystem due to hibernation
    link: https://askubuntu.com/questions/145902/unable-to-mount-windows-ntfs-filesystem-due-to-hibernation/145904#145904
  - title: Debian Wiki - NTFS
    link: https://wiki.debian.org/NTFS
  - title: How to disable and re-enable hibernation on a computer that is running Windows
    link: https://support.microsoft.com/en-us/help/920730/how-to-disable-and-re-enable-hibernation-on-a-computer-that-is-running-windows
  - title: Unable to mount Windows (NTFS) filesystem due to hibernation
    link: https://askubuntu.com/questions/145902/unable-to-mount-windows-ntfs-filesystem-due-to-hibernation
  - title: Unable to mount Windows (NTFS) filesystem due to hibernation
    link: https://support.microsoft.com/en-us/help/920730/how-to-disable-and-re-enable-hibernation-on-a-computer-that-is-running-windows
  - title: How To Resize Windows Partitions
    link: https://help.ubuntu.com/community/HowtoResizeWindowsPartitions
  - title: Ubuntu - Install Grub2
    link: https://help.ubuntu.com/community/Grub2/Installing
  - title: Fedora - Install Grub2
    link: https://fedoraproject.org/wiki/GRUB_2#Updating_GRUB_2_configuration_on_UEFI_systems
  - title: How to reinstall Grub2 with efi
    link: https://superuser.com/questions/376470/how-to-reinstall-grub2-efi
  - title: Grub2 'deadora' theme
    link: http://glacto.deviantart.com/art/Deadora-fedora-600786359
  - title: Trick to get Grub2 theme working in EFI
    link: http://forums.fedoraforum.org/showpost.php?s=50155614a0462c08a829222601336ce2&p=1657786&postcount=6
  - title: Procedure to change GDM login background image
    link: https://wiki.archlinux.org/index.php/GDM#Log-in_screen_background_image
  - title: "It seems that no one on the entire Internet knew how to change the font size!"
    link: http://myunster.com/blog/server-administration/18.html
---

# Getting windows ready

The first step to any dual boot is to shrink Windows as necessary. For us that meant deleting all the data that was extraneous, as well as executing Window's own shrink function.

## Windows hibernate

Windows 10 (it appears) by default will hibernate the system instead of turning it off entirely when it is shut down. That is to say that it stores a file name `hiberfile.sys` that is basically a snapshot of the state of the machine. There was an error displayed in the terminal at boot saying as much, and recommending that we take care of it. So we did.

To make hibernation unavailable, follow these steps:

```
Click Start, and then type cmd in the Start Search box.
In the search results list, right-click Command Prompt, and then click Run as Administrator.
When you are prompted by User Account Control, click Continue.
At the command prompt, type powercfg.exe /hibernate off, and then press Enter.
Type exit, and then press Enter to close the Command Prompt window.
```

After rebooting, we determined that the file was still there, and hadn't been deleted by Windows since the last boot. This you can check with dmesg after booting up a live DVD. IIRC there was a message spit out by NTFS during the latter half of the boot that scrolled by that specifies that the partition is RO as it is just hibernated.

## [BunsenLabs to the rescue](#bunsenlabs-to-the-rescue)

I was to repartition the disk in BunsenLabs as it's a familiar distro for me. After `gparted` couldn't determine the filesystem's metadata in order to shrink the Windows partition, we decided to remove the hiberfile in a more round-about way than just deleting it. After all, with that file there, `ntfs-3g` would only allow us to mount the partition read-only. Luckily it has an option to remove that file and make it readable-writable again.


```
# mount -t ntfs-3g -o remove_hiberfile /dev/sda5 /media
```

At that point, the "hiberfile" was removed, and `gparted` was up next.

## Windows cleans itself, again

Well, not really. There was a `ntfsresize` error that occurred when we tried to resize the partition. This time, it complained of bad filesystem metadata, disallowing us from resizing the partition. Fixing the partition had to be done in Windows, so we booted back into Windows and fixed the filesystem.

```
chkdsk /f
```

After issuing that command, we were greeted with a prompt stating that the operation could only be done once the filesystem was unmounted - aka after a reboot. In fact, it took two reboots (one forced by Windows itself) to cycle through the filesystem check. However, after that check, there was less used space on the drive, and the filesystem was able to be resized by `gparted`. The only thing left to do was install Fedora itself.

# The Fedora Install

For some reason, the Fedora ISO would only boot in Legacy mode. Unfortunately, this meant that when it was installed, GRUB was installed to the (pseudo?) MBR, and not the EFI partition. At that point, booting from EFI didn't let us boot either Fedora _or_ Windows. Oops.

## Chroot

I then chrooted into the Fedora system from the Fedora live USB with the following:

```
# mount /dev/{root} /mnt
# mount /dev/{home} /mnt/home
# mount /dev/{boot} /mnt/boot
# mount /dev/{efi} /mnt/boot/efi
# mount --bind /proc /mnt/proc
# mount --bind /dev /mnt/dev
# mount --bind /sys /mnt/sys
# chroot /mnt
```

## GRUB

My thought was that GRUB needed to be put into the EFI partition, so I did what I thought I needed to:

```
# grub-mkconfig -o /boot/efi/EFI/fedora/grub.cfg
```

Rebooting, there was still no way to boot into either OS, so I thought that next it was time to try to reinstall grub, I invoke `efimanager` after creating the chroot again, but still that didn't work.

## The Final Solution

There wasn't much to it, but I reinstalled Fedora. Turns out that troubleshooting the problem wasn't really worth it. More to come on that in my next post.

The way I came about it was this. How does grub get installed on the EFI partition? I have no clue. But I do know that before you installed Ubuntu, you had the Windows bootloader. Then you had GRUB. It must've gotten installed somehow.

Also, the first time that we got the LiveUSB to work was when we booted in in Legacy mode. Follow me here - if it was booted in Legacy mode, what impetus did it have to install itself in EFI mode? Absolutely none. That's why we saw that grub error in the first damn place! Also, probably relevant is that I had neglected to mount the EFI partition when I was installing Fedora the first time. I had only mounted `/` and `/home` in the GUI. So...double trouble. And that one's on me. We could've been done like 3 hours earlier.

And yes, *sigh* windows works just fine too.

# GDM Login Background

The GDM image is controlled by a generated binary. The instructions are found below. However, Fedora doesn't include `gresource` by default. It has to be installed with the package `glib2-devel`. That is all.

# Errata - Fucking Ubuntu

## Making changes for EFI

So, it turns out that `update-grub` in Debian distributions is only "a stub for grub-mkconfig -o /boot/grub/grub.cfg". However, EFI doesn't read from that location. The actual installation in an UEFI system is:

```
# grub-mkdconfig -o /boot/efi/EFI/ubuntu/grub.cfg
```

## Font Size

GRUB, on high resolution screens, has an imperceptibly small relative font size, relative to just about anything else. The only thing that increased the size of the font size was to make a brand new font (`.pf2`) and apply that to GRUB's config. Then `dpkg-reconfigure console-setup` can change the size of the TTY text size, but in my experience that's all it changed.
