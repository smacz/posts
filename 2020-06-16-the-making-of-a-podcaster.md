---
layout: post
title: "The Making of a Podcaster"
date: 2020-06-16
category: Battlestation
program: OBS
process: Configure
repo:
image: mxl.jpg
description: "Recording a Podcast with OBS -- Notable mentions to Jitsi, PulseAudio/Pulseeffects, Audacity, and OurCompose"
---

# Grand Plan

My co-host and I use Jitsi to do VoIP conferences, video included. It's a pretty stable software stack, and rarely fails us catastrophically. What I wanted to do was to take some of what we went over and to make it into a podcast. Not our literal babbling, but a representative sample of our finer moments.

# Tech Breakdown

Unfortunately, doing that is a bit harder than it would appear at first blush. There are a couple of components that are required in order to do that that we were able to identify:

- Remote communication medium
- Centralized Show Notes
- A/V recording software
- A/V _editing_ software

## Remote Communication Medium

In these _unprecedented times_, we all had to choose a video chat service that worked for us. Luckily, my co-host and I had already been using Jitsi for a while. It definitely helped that it was integrated into Matrix/Riot's integrations. We just kind of went for it, and kept on using it.

Importantly, it gives a decent grid view, where attendees can view all of the other attendees in a grid. For my co-host and I, this was easy, since it was only the two of us. However, for those who have larger parties, it may prove to be more unwieldy. For us, it worked fine. We would be able to capture the window of the two of us and use that for our video stream. Or, as it turns out, our per-episode image.

## Centralized Show Notes

So there was a bit of concern over show notes. These notes should obviously be made available as a useful reference material to the public, but should be used by us during the show itself. We wanted to eat our own dogfood with this, so we determined to create the show notes on the very page that we deployed the podcast episodes onto. This was made possible by utilizing OurCompose, and deploying a Jekyll instance of our show notes, that deployed a branch that was several commits ahead of master. This allowed us to view our show notes during our recording, and to backport those notes _along with any follow-up notes or links_ to our master branch that was being deployed as our production podcast page. Of course we have git version-controlling all of the things.

## A/V Recording Software

This is where things started getting real. [Open Broadcasting Studio](https://obsproject.com) (OBS) was our first go-to when it came to podcasting on open-source software. There was no hesitation. However, as you'll see down below, there were a couple of roadbumps during the process of getting it working.

OBS ended up having the following devices:

1. Jitsi Audio Out - remote audio
2. Mic In - local audio
3. Jitsi Screen Capture

With the following caveats:

### Mic Ambient Audio

Since I'm recording without headphones, and with my partner's audio coming out of my studio monitors, I have a fair bit of audio that needs to be suppressed. This can be done in post.

Later, I transitioned over to using headphones (Aftershokz Airs), but that came with its own hardships as you'll see below.

### Video stream vs window capture

For some reason, OBS likes to capture only the video stream of my partner without capturing the entire window which shows the two of us. For some reason simply exiting out of the connection (not necessarily restarting the program), and re-joining the meeting results in a window capture of the entire stream rather than just his stream. There seems to be no rhyme or reason for this, but it's an easy work-around, so I have no incentive to track this down quite yet.

### PulseEffects

There is a PulseEffects preset that is "Perfect EQ and Bass Boost", which really makes my JBL's shine. Unfortunately, having that running in-line when trying to capture various streams ends up being the biggest headache I've had to deal with in a long time.

Also, since PulseEffects is set to start whenever my computer does, and simply exiting the program does not revert everything that it set up, what I end up having to do is to turn off the audio start and reboot my computer to make sure there are no traces of it left. Fortunately, that seems to be a fairly robust solution to getting this working, so I'll leave it be for the time being.

## A/V Editing Software

[Audacity](https://www.audacityteam.org/) is an audio editing software that we're using right now. It works just fine, since all we're putting out right now is audio. It also has a dark mode.

### Keybindings

Some neat tricks to know when using Audacity to edit audio are:

1. "`CTRL` + Scroll" - Zoom in and out
2. "`SHIFT` + Scroll" - Scroll left and right on the timeline
3. "`CTRL` + `x`" - cut a section of audio that's highlighted
4. "`CTRL` + `z`" - undo the last edit

And since all we're doing usually is trimming up snippets that we end up rambling in and cutting out all of the "um"s and "ah"s, that's the bread and butter of editing.

### Key Functionality

There are a couple of things that come in super handy when creating this podcast:

1. *[Effect]* -> *[Compressor]*

This comes in super handy since we have to lower the sound of our mics in order to avoid clipping. Without this the audio is literally not able to be listened to. Also it does a pretty good job. I haven't seen too many parts that were super quiet or super loud.

If I wanted to take the sound quality from "good" to "great", then this is probably what I'd be tinkering with. However, for the time being, using close to the presets is fine. For instance, all I have to do is set the following:

- Threshold: -12db
- Noise Floor: -35db
- Ratio: 3:1
- Attack Time: 0.20 secs
- Release Time: 0.10 secs
- Make-up gain for 0db after compressing: *Checked*
- Compress based on Peaks: *Checked*

2. Highlighting and playing

One of the 'gotchas' that I first ran into was listening to the audio as I was editing it. It often skipped back to the beginning of the show when I went to resume playing after pausing it to cut out a section of audio! I found out that where the selector was, the playback would start from as well. This got pretty annoying, at least annoying enough to develop muscle memory around it.

To break it down, the spacebar once will pause the playback. The spacebar a second time will restart the playback from the marker in the audio itself (like where you clicked on it). The `x` button will _also_ pause the playback, but it will restart it from where it was paused when pressing it a second time.

3. *[Edit]* -> *[Preferences]* -> *[Interface]*

This is ultimately important for late-night editing. It turns on dark mode, and also sets up whatever other types of tweaks you want to implement.

4. Importing multiple audio tracks

During our recording, we tried to set the mix to have one of us panned to either side. This didn't work since my input was mono and strictly refused to pan when upmixed to stereo. So what we did was set OBS to record two different tracks, and then imported them into Audacity. There's actually a prompt right off the rip to do this. It makes it super simple to do... just make sure that you remember which are the ones that you needed to import!

One of the other 'gotchas' that I ran into was that the selection trimmed only one track if the second wasn't selected. Working with this is as easy as making sure to always drag the selector across both tracks when trimming. If something sounds off when playing it, you'll probably have to undo all the way until you forgot to trim both tracks.

5. *[Effect]* -> *[Plug-in 1 to 15]* -> *[Noise Gate]*

Since I run fans when recording, and get other quiet noises, I also run the Noise Gate plugin over the audio. This filters out all of that. I have it set at:

- Level reduction: -100db
- Gate threshold: -43db
- Attack/Decay: -250ms

6. EQ

This is something that I could work on in the future as well. Having both of us using professional mics helped, and we ended up sounding just fine. Also, since I'm not an audio engineer, it would probably end up being more fiddling than it was worth. Of course, if I do get a workup done here, I'll make sure to post it. If you look around, there are several good posts online about setting up an EQ for spoken voice.

7. Panning

After going through several write-ups, I found that there was no real reason to pan left-right. There are only two of us, and we don't make a habit of talking over each other. Especially considering the fact that we monologue more often than not, it would get fatiguing to have audio coming from one side for a prolonged period of time. I decided to leave both of our audio centered and just go from there.

8. *[Generate]* -> *[Silence]*

By far the most intriguingly-named option here is the one that came in the most handy. For a quick-fix, since my partner's audio was in the background of my audio stream, when he went on his soliloquies, there was a disturbing echo from my stream. In order to combat that, I couldn't just "cut" the audio, as that would lead to a discrepancy of the sync, and we would start talking all out of whack. No, what I chose to use instead was the generation of silence, which worked the same as cutting the audio, but replaced it with silence. It worked well enough for the time being.

This will probably be replaced by Ambient Noise Suppression, but for the first few episodes, this has been good enough.

9. *[File]* -> *[Export As]* -> *[MP3]*

In case you didn't know, doing this is the same as exporting a ODF format into anything usable. However, when going through this process, it asks for the metadata for the audio file. This is important, as it is what shows up when the media file is played in any intelligent player. Most of it is straightforward, but it is geared towards albums, rather than podcasts. So instead of the "Album Name", I put the name of the show, and instead of the track number, I put the episode number. This seems to work out well enough, and even leaving them blank doesn't do any harm.

10. Project Retention

Audacity saves it's projects in folders named according to the project. These can get pretty large, depending on how you're editing, and what you're editing. However, they're great for quitting and resuming editing the project. Audacity will generate an `.aup` file that points to that directory, and that file can just be opened by audacity to resume editing.

Audacity doesn't have an auto-save functionality necessarily, in the sense that if you choose to close the project and choose "Do Not Save" on the popup asking you if you want to save the project, then you will lose all of your edits. However, it does have an automatic crash recovery in the event that it quits unexpectedly. I wouldn't rely on that, and I would `CTRL + s` regularly.

As far as exporting it though, I want to save a couple things besides the final project. I want to save the raw tracks, and I want to save the cut tracks. I went searching for a file format that would allow me to save two tracks in one file. This doesn't really seem to be a _thing_, so instead of trying to force it into an `.m4a`, or a `.mkv`, I just exported two FLAC files; one for each of the tracks. The first two that I exported were raw, and the second two were the cut versions.

This is done with Audacity's "Export Multiple" feature. What I did was export based on track, and then create a prefix that it would append the number of the track onto. I did this firstly because I knew it would be trivial to rename it to my name or my partner's name to identify the track, instead of using 01 or 02 to identify them. Secondly I did this because based on what I was exporting I could name them with the type of recording, to differentiate the cut versus the raw audio. For instance, I would have the prefix of the raw audio be "episode-1-episode-name-raw-", which would then append "01" and "02" to it based on the track it was exporting.

I haven't archived these up since the space saving would most likely be minimal, but there is the possibility to do this too once those four (2x tracks raw and cut) files are generated per episode. That means that all I need to retain are those four files and I can instantly start re-cutting or adding additional content into any given episode. We'll see how well this works, but it seems to be fine for the time being.

# First Show Recording

Just for historicity's sake, I'm going to document the myriad of issues that occurred when attempting to record this podcast on the initial recording date.

- Vivaldi Web Browser refused to load any pages from the internet
- Audio began to echo in the recording
- Headphone jack was 1/4", and headphones only had 1/8" male end
- PulseAudio duplicated inputs and outputs
- OBS did not record full Jitsi window
- Bluetooth mouse failed to connect
- Show notes were not able to be deployed in their branch
- Audio was unable to be recorded by OBS
- Audio was unable to be transmitted over Jitsi
- Email client stopped syncing its provider's Inbox

## Lesson Learned

> Don't do it live.

In all reality, ...actually yes. Don't do it live. And rehearse it. And test your software stack. There is a whole bunch of things that go into making a performance, that aren't necessarily things that have to be considered when you're writing or deploying software. It's a different skill set. It will take time to become accustomed to it.

For us, the issues listed above overwhelmed us and we were not able to record a passable episode for the podcast. And especially debilitating was that this was supposed to be the first episode. However, we were able to touch base later with more than our fair share of humility. Right now we're focusing on viewing our yaks, and figuring out the best way to shave them down to the stubble. Turns out, there are some frustrations that will follow you all the way from software development to podcasting and beyond.

# First Show Recording; Second Attempt

This one was pure beginner's luck. After futzing around a bit and double-checking to make sure that everything was set up so that nothing was broken, we started to record. There were plenty of times where we had to do several takes, or retrace our steps, however the sheer level of professionalism that we suddenly snapped into surprised both of us.

We ended up recording almost an hour and fifteen minutes of audio, after promising a 20-45 minute show. Since I'm too lazy to re-record my estimate, I ended up trimming it down to just under 45 minutes. There were a couple of hilarious outtakes that I don't want to lose, but that were NSFW, so I am not sure how I want to handle those. I might just end up saving the raw audio, and maybe another with outtake snippets. No reason to not start early, right?

## Lesson Learned

> We're awesome at this stuff.

Well, it came out awesome anyways. For some reason, the editing brought it all together, and made it pretty cohesive. But the content really shone.

Another thing is the temptation to talk over one another is one to be avoided. There were several false-starts that had to get cut because it was both my partner and I trying to start up a comment and talk over each other at the same time. I think these instances resolved themselves for the most part, however. Still, there were also times where I or my partner were getting ready to segue into the next section and hand off the monologue, where the other jumped in early. There is no need to rush it in this setting. It is a lot easier to cut out dead air than to cut out overlapping chatter. Of course it's possible when recording multiple channels (we didn't find that out until the next recording), but it's still not fun or easy.

Lastly, it takes several listen-throughs to figure out which parts should be kept, and which should be cut. Especially the first when there was a _lot_ of good material, there was some that I was very proud of that just didn't make it into the final cut. I wouldn't have been able to make those decisions had I not given myself several times through to listen to it before making those hard decisions. Taking notes while listening to the run-through for the 2nd or 3rd time doesn't hurt either.

# Second Show Recording

This one started out great! Right off the rip we had some great banter before diving into our main points. We were starting to get a little chatty and make excited, but not necessarily salient points, but there was still some solid content there.

However, the further we went on, the more we realized that we were running out of steam quickly. We went back and forth, the two of us not necessarily being on the same page as one another; at one time I actually asked my partner to start over again. That was a dick move, and I didn't do that again (nor hopefully will ever again, it just didn't feel respectful to do so). At the end of it, the conversation started to meander hardcore and lost almost all of its focus. Having looked over at the recording time, I resumed a sense of urgency to finish out the remaining points and record the outro. By the time I had done so, we were less than 5 minutes shy of the two-hour mark. Considering that we promised a 20-45 minute show, this felt crazy.

After the recording stopped, there was no shortage of the possibility of a re-take thrown around. We both left that evening feeling pretty defeated, and got ready to face up to the music. However, on a whim, I gave the episode a listen-to at 2.3x speed (as I am wont to listen to podcasts), and lo and behold, I thought that there might be a workable episode somewhere inside of those ramblings. I started noting down things that I heard, and things to cut, and listened to it several more times here and there.

I took four and a half hours out of one of my evenings to cut it together into something workable. At the end of that, I wasn't sure where up was, and I stumbled off to bed. The next day I listened to it, and by some miracle, it actually made sense! When I saw my partner that weekend, the two of us listened to it in absolute astonishment as we went back and forth about what had been cut, and about how well it had come together. Inexplicably, we pulled it off!

## Lesson Learned

> I will never let that happen again.

It takes anywhere from 2-2.5x the length of the recording to take the raw recording, put it through post processing, and get it uploaded and published with all of the surrounding setup. I never want to spend that much time listening to my own voice again. My roommate came in during that evening, and instead of wishing me good luck, told me not to get too caught up in listening to my own voice. Such a supportive motherfucker right there.

This also gave me the courage to cement the idea that I can listen to this sped-up without any terrible ramifications while reviewing it before post. In fact, that's almost necessary, given the length of a decent episode, not to mention this overbearing episode.

This is the first one where we did two audio tracks, and I learned:

1. Save the panning until post
2. You can cut out one of the other's interruptions or coughs or whatever without having to cut out the whole section
3. Once you mix it down to a single track, you can't unmix it again, so save the project!!!

Lastly, even though the episode is pretty solid despite what it had to go through to get to that place, the cuts are a lot more jarring this time around than before. That is to say they are still infinitesimally subtle, but they are still there on the second or third listens, that is to say every time I listen back to it now. It would be worthwhile to figure out if there is some method that I'm missing to make them smoother. For the time being, I'm simply cutting the audio at the nodes of the waveforms of the audio, hoping I'm not cutting in the middle of a word. And it just seems to be working.

# Third Show Recording

I was really excited for our third show, as we got the show notes in way ahead of the day of recording, and I was able to edit them and review them before sitting down to record. My partner and I also came into the studio with some great energy and recorded a really solid podcast. Well, I recorded my side of the really solid podcast anyways...

Unfortunately, due to some unfortunate experimenting, the second audio track wasn't recorded, so what was produced at the end of the day was a recording of me talking to myself! Obviously this wouldn't work, and we had to schedule a re-recording.

## Lesson Learned

> Don't change anything after the final test.

In this case, I was changing the audio output to my headphones in order to alleviate the echo from the studio monitors. Of course, this threw OBS all outta whack, and it didn't pick up the second stream. I'm kicking myself now that I wasn't able to notice that the levels weren't registering for my partner, but that's just how this goes I guess.

# Third Show Recording; Take Two

Luckily my partner was gracious enough to make time the next day to re-record the episode. I had still taken time to listen to the first recording of myself, to see if there was anything worth noting down; things to make sure to say, things to make sure _not_ to say, or anything that was left unsaid. However, as soon as we started to record, I promptly forgot all of the mental notes I had made.

The most frustrating part of the second recording was that there was a lack of energy to it that was noticeable from the start. We weren't riffing off each other's quips like normal, and the witty comments that we had made the day before lost their luster the second time around. It was a disappointing experience. However, I am proud that we were able to persevere and crank out a passable show. Even though we had to put real effort into re-hashing and re-creating some of the energy and spontaneity that was there before, I'm glad that we did, because it led to an all-around solid episode.

## Lesson Learned

> Fake it 'till you make it.

While relying on the natural energy of a sparkling conversation is a decent enough strategy for a casual recording, sitting down to do it a second time is actual work. It takes a lot more effort to be just as excited to rehash the same points a second time, and I could imaging a third time as well. Here's hoping that I will never have to find out if that's true first-hand though.

There were also a couple of instances in this recording specifically where the audio glitched out a bit for my partner, as Jitsi was buffering. This comes through in the show notes, as it was minute enough to be difficult to edit down, but prominent enough to be noticeable. A different setup would have him record his audio locally, and then send it over for me to edit, however, for the time being, we are not phased by dealing with these infrequent hiccups.

# Take-aways and going forward

While this was more setup and actual work than I thought it was going to be at the beginning, I have thoroughly enjoyed all of the time that I have dedicated to going through putting together a functional podcast. I've learned some really good baseline skills to dealing with audio editing and mastering, and have opened up a path forward doing that. I've had some really good conversations, and can't wait to have even more.

## Final Lessons Learned

> Podcast with someone you love working with, and you will never podcast a day in your life.
