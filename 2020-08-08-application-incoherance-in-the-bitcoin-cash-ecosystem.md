---
layout: post
title: "Application Incoherance in the Bitcoin Cash Ecosystem"
date: 2020-08-08
category: Battlestation
program: Bitcoin
process: Architect
repo:
image: bitcoincash.png
description: "Trying to adopt an ecosystem to operate in and evangelize is harder than it should be."
---
This all started off when I stumbled across the fact that the Opera browser has a built-in crypto wallet. It was about time for me to step back and ask myself, "Why am not using bitcoin more?" Especially in these _unprecedented times_, why wasn't I more active with crypto?

I stepped back and looked at all of the other ecosystems that I had adopted into my day-to-day life to figure out what was missing from my BCH experience to keep it from being a staple of my online life. What I found was a messy ecosystem full of either major implementation shortcomings, abandonware, or just general fuckery.

# Requirements

To start off, I wanted to list the requirements that I was looking for. This is in-line for what I ask for from the rest of the ecosystems that I interact with on a day-to-day. The ones apart from what I need for my day job, anyways. For example, Bitwarden, Nextcloud, Matrix, Kanboard, and Email are all systems that I use on a regular, ongoing basis. They all have this functionality. And those that don't have a desktop client are just one Electron application away from having one, but most do. Besides, if I'm not coding or playing Civilization, 99% of my time is spent in a browser. I thought it was fairly basic, but that doesn't seem to be the case:

- Android App
- WebUI / Webextension Plugin
- Open API

Some other things that would be nice to have would be some sort of self-hosted node/config server software, the ability to buy/sell/trade crypto, have it support SLP tokens, and that the entire stack be open-source. But being practical, I was ready to forgo these requirements for the time being just in order to value single.

# Clients vs Servers

Pausing to highlight the first of many incredulous moments, let's consider that the open-source full-node implementations ([Bitcoin Unlimited](https://www.bitcoinunlimited.info), [Bitcoin ABC](https://www.bitcoinabc.org), and [BCHD](https://bchd.cash)) do not offer client software. Being well-versed in the way any clients need not know implementation details about any of these implementations, still I would not similarly expect to see a phone company offering a service plan without a phone to go with it! Which is not to say that you can't BYOD, but it defeats the entire purpose of joining a network if you can't participate on it.

Besides the necessity to connect to full nodes, I also realize that most of the apps are backed up by configuration services that provide backup storage, synchronization functionality, the ability to buy/sell/trade crypto, and more. This is obviously the realm of the ecosystem surrounding the network itself. I'm here talking about everything surrounding the client-node and node-node communication. Again, like above, I would love to see a way to run that in a self-hosted manner, but I can accept that it's proprietary on the backend as long as the wallet is non-custodial. This is the place where a cohesive brand is absolutely necessary, and a big legitimacy booster in terms of experience for the end-user.

These applications actually have a simple enough job, which would be to provide a cohesive experience between platforms, the most common nowadays being the big three:

1. Web/Browser Extensions
2. Mobile
3. Desktop

Or, if you were expecting me to divide them up by OSes:

1. MacOS
2. Linux
3. Winbloze

If you're thinking to yourself, "That's pretty comprehensive at a high-level, but not impossible to maintain for one service;" you'd be in the same boat as I was when I began this journey.

# Ecosystems

Here we'll go through all of the available ecosystems for transacting with BCH, and what they do and do not bring to the table.

## Electron Cash

https://electroncash.org/index.html

Electron Cash is the wallet that I started out with. It's simple since it has a desktop and mobile install. These can obviously be sync'd up in the usual sort of manners. And hey, it's open source, so that's a plus!

The immediately obvious drawback here is that there was never a web wallet that was written. And I understand that; but not only that, there is no plugin for browsers either. This is an immediate fail, without going into depth on the confusion that comes with figuring out that their SLP functionality is housed at an entirely separate project. Or their UI which is clunky and outdated. This is obviously par for the course in FOSS, but there doesn't seem to have been any loved paid to the UI in at least a decade. Add onto this the confusion that Electron Cash is a fork of the legacy Electrum client, and you've got a recipe for adding even more confusion onto the sentence, "Well, what you're referring to is _actually_ known as Bitcoin Cash, which is the real continuation of the original vision for Bitcoin..."

## Dead Wallets

There are some wallets that are just gone. But some of them are still on https://www.bitcoincash.org/wallets.html, because, why not! Here is the short list that I found to be dead, Jim!

## Mobile-Only

Just to get these out of the way, these are the ones that didn't have a browser _or_ a desktop client:

- [BRD](https://brd.com)
- [Edge](https://edge.app)
- [IFWallet](https://www.ifwallet.com)
- [mobi](https://www.mobi.me/)
- [Neutrino](https://neutrino.cash)

## Browserless

- [Exodus](https://www.exodus.io/download)

## Casa

Now here is a setup that is pretty neat. It has all of the requisite pieces, plus what looks like be a sustainable and scalable business model. The only problems:

1. It doesn't support BCH
2. It's based on the lightning network

A+ for branding, F for common sense.

## Badger/Bitcoin.com

Now this one is interesting, [Bitcoin.com](https://badger.bitcoin.com) put out Badger Wallet, which has a mobile app only. However, Bitcoin.com also has their rebranded CoPay wallet, but do not seem to be developing the desktop application anymore. The application appears to be mobile-only in their github repo, and there has been no work done on the old desktop app version, which currently is a full major version behind the mobile app.

Setting that aside, Bitcoin.com also have a web wallet in [Mint](https://mint.bitcoin.com/#/portfolio). This is yet another divergence in their cohesive branding scheme. At this point, I would hope, but would not be reassured that all of them will continue to live in harmony.

## Bitpay

Bitpay have their [CoPay](https://bitpay.com/wallet/) wallet that is branded for Bitpay. While they also have desktop wallets, they don't offer a WebUI. However, the _do_ offer a WebExtension. However, this appears to be a Bitpay-specific integration, in that it will allow the purchase of store credit through BTC, but not transact using typical addresses. I'll try it out and see what the support there is like.

However, true to form, there is a very shaky branding that doesn't seem to concern itself with consistently. While I may not care about it on a technical level, it is definitely off-putting to customer in me.

# Conclusion

Most of my frustration here is to realize that there are no web-frontends to Bitcoin, either as an extension or website. For almost every other upright, legitimate service, this is not the case. The tech community has drilled it into people's heads to "just get started at the website". And granted, mobile is a majority of the usage and still growing. However, that is no excuse for focusing on mobile apps while ignoring the fact that the majority of our own time is spent in front of a keyboard, as techies, and yet we can't put something together that can follow us around the internet. That's kinda sad.

I don't really have a recommendation for a wallet after this. I'll be putting Badger Wallet through its paces as long as Roger Ver is CEO of Bitcoin.com. And for convenience's sake, I'm interested in how the Bitpay wallets stand up to my day-to-day transactions.

For something that was supposed to be the "Internet of Money", we sure have forgotten about the Web, haven't we?

# Sidenotes

## A note on the branding

Oh yeah, and anyone who doesn't know bitcoin, knows that Bitcoin's logo is gold. That's the cold hard truth. Despite the BCH community's nearly universal brand transition to green, it's going to be a long time before that gets changed, if ever. That's probably the biggest uphill battle that has to be faced. Which is why I understand why there was a long drawn-out battle for the logo color. It's just not a great situation any way you slice it.

Also, as much as I harp on branding on general, as long as you don't mind believing that it changes everything, branding will never (technically) matter.

## A note on community turbulence

The BCH community is an incendiary one at that, and there is bound to be one company or another causing drama. Most recently has been Bitcoin ABC's march forward on the controversial IFP proposal. Make sure to check in periodically to know what's going on. Of course, any _real_ bitcoiner wouldn't have to be told that.
