---
layout: post
title: "Unsolveable Bluetooth Problems"
date: 2020-10-03
category: Battlestation
program: Bluetooth
process: Troubleshoot
repo:
image: bluetooth.jpg
description: "I've been facing finicky bluetooth issues, and wanted to note them here."
references:
  - title: ''
    link: ''
---

# Symptoms
Randomly after reboots or suspends, my bluetooth just stops working.
This is especially problematic as my mouse is bluetooth.
This leaves my computer in a near-unusable state.
The most frustrating part of this is that there doesn't seem to be any straightforward guidance on how to fix this.

The symptoms are as follows:
- No bluetooth device available
- Bluetooth daemon refuses to start up
- Bluetooth desktop applet is missing

So there's not a lot to search for.
There was this single error message when running `systemctl status bluetoothd`:
```
Condition check resulted in Bluetooth service being skipped
```

# Background
Since I have a combo bluetooth/WiFi chip, I can tell if it's a hardware issue by simply testing if the WiFi on the computer is working.
When it is, but the bluetooth controller isn't seen by the computer, I have to assume that it's a software issue.
So what's going on?

# Solution
Given that googling around didn't pull up a single answer, I had to ask myself what I tried?
I tried a simple `shutdown -r now` reboot, which didn't work.
I tried modprobe-ing btusb, which didn't work.
Basically nothing was working.

I came across a post saying that the bluetooth drivers in the kernel might be putting the bluetooth hardware into a bad state, and maintaining that through everything except a cold boot.
So I tried shutting down, and rebooting, but that didn't help.
Further down the post, they clarified what they meant by a "cold boot".
Their definition was a complete shutdown followed by being disconnected from the power.
I figured I tried everything else, so why not this?

After unplugging it, I left the computer in that state overnight.
After plugging it back in, voila!
The bluetooth was back.
This is ridiculously frustrating.

It's sad that in this advanced day and age, where we are contemplating boot-less kernel upgrades, that a simple "cold boot" is the only thing that can solve a bluetooth issue?
As long as it continues to work, I'm not too concerned, just disappointed.
