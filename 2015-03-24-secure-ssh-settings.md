---
layout: post
title: "Secure SSH Settings"
date: 2015-03-24
category: SysAdmin
program: OpenSSH
process: Config
description: Setting up the SSH defaults so that they can be secure from your siblings as well as government agencies.
---


So sshd wasn't set up on my new Jessie netinstall, so I decided to start it up:

~~~~ shell
$> sudo service ssh start 
~~~~

I also learned that if openssh isn't installed on the system, that won't do
anything. So I installed the Debian metapackage `openssh`, and knew that it
would start now. But then again, let's take this opportunity to learn systemd.
There's a great [cheatsheet](https://fedoraproject.org/wiki/SysVinit_to_Systemd_Cheatsheet) put out by the Fedora project that compares
sysvinit commands and transposes them to systemd commands. So, to get `sshd`
started I executed:

~~~~ shell
$> sudo systemctl start sshd
~~~~

Cool. But now, we have to [secure our ssh](https://stribika.github.io/2015/01/04/secure-secure-shell.html) implementation and augment the paltry
defaults that it ships with.

## The crypto

### Key exchange

Read the link above if you're interested in the reasoning behind what comes
next. Otherwise, implement the next few things to follow.

In `/etc/ssh/sshd_config`:

	KexAlgorithms curve25519-sha256@libssh.org,diffie-hellman-group-exchange-sha256

In `/etc/ssh/ssh_config`:

	# Github needs diffie-hellman-group-exchange-sha1 some of the time but not
	# always. 
	#Host github.com
	#	KexAlgorithms curve25519-sha256@libssh.org,diffie-hellman-group-exchange-sha256,diffie-hellman-group-exchange-sha1,diffie-hellman-group14-sha1
	 
	Host *
		KexAlgorithms curve25519-sha256@libssh.org,diffie-hellman-group-exchange-sha256

For `/etc/ssh/moduli` if it exists:

	$> awk '$5 > 2000' /etc/ssh/moduli > "${HOME}"/moduli"
	$> wc -l "${HOME}/moduli"
	$> mv "${HOME}/moduli" /etc/ssh/moduli

If it doesn't:

	$> ssh-keygen -G "${HOME}/moduli" -b 4096
	$> ssh-keygen -T /etc/ssh/moduli -f "${HOME}/moduli"
	$> rm "${HOME}/moduli"

### Authentication

In `/etc/ssh/sshd_config`:

	Protocol 2

	HostKey /etc/ssh/ssh_host_ed25519_key
	HostKey /etc/ssh/ssh_host_rsa_key

	PasswordAuthentication no
	ChallengeResponseAuthentication no

	PubkeyAuthentication yes

	AllowGroups ssh-user


In `/etc/ssh/ssh_config`:

	Host *
		PasswordAuthentication no
		ChallengeResponseAuthentication no

		PubkeyAuthentication yes
		HostKeyAlgorithms ssh-ed25519-cert-v01@openssh.com,ssh-rsa-cert-v1@openssh.com,ssh-rsa-cert-v00@openssh.com,ssh-ed25519,ssh-rsa

To clean out any old short insecure keys:

	$> cd /etc/ssh
	$> rm ssh_host_*key*
	$> ssh-keygen -t ed25519 -f ssh_host_ed25519_key < /dev/null
	$> ssh-keygen -t rsa -b 4096 -f ssh_host_rsa_key < /dev/null

To generate new keys:

	$> ssh-keygen -t ed25519 -o -a 100
	$> ssh-keygen -t rsa -b 4096 -o -a 100

By the way, the `-o` switch:	

> Causes `ssh-keygen` to save SSH 'Protocol 2' private keys using the new
> OpenSSH format rather than the more compatible PEM format. The new format has
> incereased resistance to brute-force password cracking but is not supported by
> versions of OpenSSH prior to 6.5. Ed25519 keys always use the new private key
> format.

To add group `ssh-user`:

	$> sudo groupadd ssh-user
	$> usermod -a -G ssh-user *username*


## Symmetric ciphers for data transmission encryption

In `/etc/ssh/sshd_config`:

	Ciphers chacha20-poly1305@openssh.com,aes256-gcm@openssh.com,aes128-gcm@openssh.com,aes256-ctr,aes192-ctr,aes128-ctr
	MACs hmac-sha2-512-etm@openssh.com,hmac-sha2-256-etm@openssh.com,hmac-ripemd160-etm@openssh.com,umac-128-etm@openssh.com,hmac-sha2-512,hmac-sha2-256,hmac-ripemd160,umac-128@openssh.com

In `/etc/ssh/ssh_config`:

	Host *
		Ciphers chacha20-poly1305@openssh.com,aes256-gcm@openssh.com,aes128-gcm@openssh.com,aes256-ctr,aes192-ctr,aes128-ctr
		MACs hmac-sha2-512-etm@openssh.com,hmac-sha2-256-etm@openssh.com,hmac-ripemd160-etm@openssh.com,umac-128-etm@openssh.com,hmac-sha2-512,hmac-sha2-256,hmac-ripemd160,umac-128@openssh.com


## Conclusion

There are many [unorthodox](https://schneier.com/blog/archives/2009/02/xkcd_on_cryptan.html) ways and many [sidechannels](http://arstechnica.com/security/2013/12new-attack-steals-e-mail-decryption-keys-by-capturing-computer-sounds) by which someone can
compromise your data transmisson security/integrity. These defaults however
provide a solid foundation of security when it comes to `ssh`. There is much
*much* more to come however.
