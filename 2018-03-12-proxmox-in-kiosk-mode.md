---
layout: post
title: "Proxmox in Kiosk Mode"
date: 2018-03-12
category: SysAdmin
program: Proxmox
process: Install
description: "Proxmox is great and all, but being able to monitor it with a WebGUI doesn't leave much room for old-fashioned KVM access. Especially when the server is in the office, and it's easier for others to associate the monitor on top of the computer with what is running on the computer."
references:
  - title: Open vSwitch on Libvirt/KVM/Qemu
    link: http://docs.openvswitch.org/en/latest/howto/kvm/
---

# Installation

To be fair, I'm going to copy off of [@Peter Quiring's answer on Stack Overflow](https://unix.stackexchange.com/questions/30207/debian-based-system-only-one-gui-program-nothing-else#30209) for the most part. Luckily, it uses all the components that I am familiar with, coming from a `#!` background.

```
# apt install --no-install-recommends xorg openbox lightdm chromium
# adduser kiosk
# cat << EOF >> /etc/lightdm/lightdm.conf
[SeatDefaults]
autologin-user = kiosk
EOF
# cat << EOF >> /etc/xdg/openbox/autostart
xset -dpms
xset s off
chromium --kiosk https://127.0.0.1:8006
EOF
```

Hot damn! That's cool. I mean, I won't be able to log in unless I have my keepassx file around here somewhere, but there's a reaction of pure coolness that it gives me to be able to have this up and running. Simple and elegant!

## Escape

By the way, in Openbox, `Alt` + `Space` will give you a window menu in which the browser can be closed. Then from there a right click anywhere on the screen will give you a menu that has a regular browser launcher and a terminal launcher. So don't consider this as a "secure" desktop or anything.
