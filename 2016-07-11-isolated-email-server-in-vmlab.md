---
layout: post
title: "Isolated Email server in VMLab"
date: 2016-07-11
category: HomeLab
program: Postfix
process: Config
description: "I want to simulate an email server for my VMLab, which isn't connected to the internet. This is about getting email set up on a single LAN in a sane manner...hopefully."
---

### So both postfix and dovecot will be of interest here. But also the MX records of the servers that are handled by the pfSense router I have routing between my LAN and VMLab.

The architecture is a little complex, and I'll do my best to outline it before I dive in.

There will be one "central" email server. That server will handle the actual sending of the emails as well as the recieving and storage of inbound emails for that domain.

I'll have several applications running that can send email. It's not necessary for them to recieve email though, so I won't have to worry about replies. But I'll still need a "From:" address.

That being said, there may be some applications (gitlab, discourse, kanboard) that have the capacity to accept and take action on incoming emails. This will have to be handled.

The names of the users at the VMLab domain email should be appropriate to the application. There should also be an `admin@` user for me. That should be different than `client@` though.

#### NOTE: [Sending from a "no-reply" is no good.](https://postmarkapp.com/blog/no-no-reply) Applications that do not accept resoponses should have a signature stating so, yet still retain the emails. Perhaps they should also batch notify the admin if this happens every week or so.

I have one client (currently) that is in the VMLab. That will have to both send as well as recieve emails.

The central email server will contain the MDA (Dovecot) for all recieved emails and it must handle any unwanted emails incoming for the domain and/or the applications themselves.

## MX Records - pfSense

First let's look at my DNS records. `dig` will be useful here. With `dig`, I can specify the rootserver to use to avoid any mistakes, and I'm trying to make more use of it, so I might as well use it for checking.

```
# dig @pfsense.vmlab email.vmlab

    ...
    ;;ANSWER SECTION:
    email.vmlab     3600    IN  A   192.168.5.15
    ...

# dig @pfsense.vmlab gitserver.vmlab

    ...
    ;;ANSWER SECTION:
    gitserver.vmlab 3600    IN  A   192.168.5.17
```

We don't have any MX records for either of these. This is going to be handled by pfSense.

PfSense now uses [unbound](https://doc.pfsense.org/index.php/Unbound_DNS_Resolver) as it's default DNS resolver. To add MX records, it looks like I would have to [specify a "local-data:" entry](https://forum.teksyndicate.com/t/need-some-help-with-unbound-dns/68740) that will go in the "Advanced" options.

```
local-data: "gitserver.vmlab. IN MX 10 email.vmlab."
```

And now to test it:

```
# dig @pfsense.vmlab gitserver.vmlab MX

    ...
    ;;ANSWER SECTION:
    gitserver.vmlab 3600    IN  MX  10  email.vmlab
    ...
```

Perfect! Although I'll have to automate this later to reflect the fact that all of the IPs that have a DHCP through pfSense will have a corresponding MX record similar to this one. Either that, or I can have the servers themselves set this up during configuration (I _really_ can't wait to try out ansible...). But for now I at least know that this works and we can do it manually if necessary.

## Local Users - Postfix

All we need to do here is to get the mail to get directed to the correct users. I realized that I had never read the [postfix documentation](http://www.postfix.org/postfix-manuals.html), so I decided to take the time to do that.

I figured I'd just get everything set up for at least local users. So far my typical setup works, however, `$myorigin` has to be set to `$myhostname` in order for the delivery receipt messages to be sent to the correct address. Otherwise, the receipt messages get sent to `vmlab.vmlab` instead of `email.vmlab`. I have a feeling that this is due to my not having a TLD in `$myorigin`. Meaning that I set `$myorigin = vmlab`. If this was changed, maybe the workaround wouldn't be necessary. But for now it is. We'll see how this affects remote accounts shortly.

## Remote Users SMTP - Postfix

[How To - Postfix and Dovecot SASL](http://wiki.dovecot.org/HowTo/PostfixAndDovecotSASL)

http://www.postfix.org/TLS_README.html

http://www.krizna.com/centos/setup-mail-server-centos-7/


Firewall:

```
# firewall-cmd --permanent --zone=public --add-service=imap
# firewall-cmd --permanent --zone=public --add-port=587/tcp
```

## Remote Users IMAP - Dovecot

## Mutt MUA

https://dev.mutt.org/trac/wiki/MuttFaq
