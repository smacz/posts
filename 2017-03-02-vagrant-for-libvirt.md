---
layout: post
title: "Vagrant for Libvirt"
date: 2017-03-02
category: SysAdmin
program: Vagrant
process: Install
repo: 
description: "I finally decided to get vagrant working because I didn't want to go through another install process of a CentOS box. That graphical installer is fine once in a while, but having to spin up two in a row tonight was just not something that I wanted to do."
references:
  - title:
    link:
---

# Install Vagrant

https://www.vagrantup.com/downloads.html

```
# rpm -Uvh vagrant_1.8.7_x86_64.rpm
```

# Install `vagrant-libvirt` plugin

```
# yum install -y \
    libxslt-devel \
    libxml2-devel \
    ruby-devel \
    libguestfs-tools-c \
    gcc \
    qemu\
    qemu-kvm
    libvirt \
    libvirt-devel
```

https://github.com/vagrant-libvirt/vagrant-libvirt/releases

```
# usermod -aG libvirt root
```

logout and login

```
# vagrant plugin install vagrant-libvirt
```

# Spin up VM

```
# vagrant init centos/7
# vagrant up --provider libvirt
```

# Sign into VM

## Change root password

```
# vagrant ssh
(localhost) $ sudo su -
(localhost) # passwd
```

# Vagrantfile

## Adding ssh keys

https://stackoverflow.com/questions/30075461/how-do-i-add-my-own-public-key-to-vagrant-vm

## Join virtual network

```
config.vm.network :private_network,
        :libvirt__network_name => "isolab"
```

Change interface in the management subnet (192.168.122.0/24) at `/etc/sysconfig/network-scripts/ifcfg-eth<X>` from `BOOTPROTO="dhcp"` to `BOOTPROTO="none"`. And restart networking with `systemctl restart network`.

# define another box by name

```
config.vm.define "foobar" do |foobar|
end
```
