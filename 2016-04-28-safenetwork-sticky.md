---
layout: post
title: "Safenetwork Sticky"
date: 2016-04-28
category: HomeLab
program: MaidSafe
process: Metacognition
repo: https://github.com/maidsafe
description: "An explanation of the safenetwork in five minutes or less - with links"
references:
  - title: Introductory Video
    link: https://www.youtube.com/watch?v=Wtb6L7Bg3zY
  - title: maidsafe.org
    link: http://maidsafe.net
  - title: safenetforum.org
    link: https://safenetforum.org
  - title: What are the founding principles of Maidsafe - Youtube Playlist
    link: https://www.youtube.com/watch?v=rWOB2WajfPY&list=PLgFMaA9HeffQwjWMEFk7iCz2SUK9w5XLG
  - title: Sovryn Tech interview
    link: https://soundcloud.com/sovryntech/sovryn-tech-special-0012
---
# Overview

[{{ page.references[1].title }}]({{ page.references[1].link }})

### MAIDSAFE (the company)

Massive Array of Internet Disks - Secure Access For Everyone

Started in 2006 by Scottish engineer David Irvine, MaidSafe is a small team with nicklambert as COO and @Ross as community outreach, a community that is mainly concentrated at [forum.safenetwork.io](http://forum.safenetwork.io). [The rest of the team](https://cdn-business.discourse.org/uploads/safenetwork/original/2X/6/62a81985e5386f0d668b336ea3a17dfb32b35b59.png), based in Troon, Scotland, consists of talent drawn from many different cultures and countries. Despite this variety, this company shares a mission with its community: a desire to provide security and privacy for everyone.

## What

This project aims to be a connected network that is [build on top of the IP networking layer](http://systemdocs.maidsafe.net/content/en/detail/img/stack.png). It allows for both public and private paid remote data storage, with the possibility of networking socially with built-in currency and naming systems.

This possibility is explored by the usage of apps, and developers can have their app interact with a local Launcher running on your own machine to interact with the data that you put onto the safenetwork. This Launcher can be the stock one that is being built by MaidSafe or a third-party Launcher that anyone can develop, as this entire stack is open source.

MaidSafe (the company) is building their own apps to release onto the safenetwork as well, starting with LiteStuff - their own dropbox-like app - taking advantage of the most basic ability of the safenetwork.

## Where

Maidsafe is a similar technology to [Storj](https://www.reddit.com/r/storj) and [IPFS](https://www.reddit.com/r/ipfs) in that it is a decentralized and distributed file storage. It is also similar to [Bitcoin](https://www.reddit.com/r/btc) however it uses no type of blockchain technology whatsoever. Like stated above, this was started back in 2006. Also, this is similar to [Tor](https://www.reddit.com/r/tor) in that it aims to capitalize on the social aspect - namely anonymous blogging, social media, and marketplace aspects.

[This Network has no servers](http://techcrunch.com/2014/07/23/maidsafe/). Like any decentralized and distributed technology, the community is what makes it function. Any user of the safenetwork can participate as:

* Client - Browsing the internet & Creating webpages
* Farmer - Running a Server
* App Dev - Desktop, Mobile, or Browser integration
* Core Dev - Creating and maintaining the code infrastructure

Rewards are granted by the safenetwork on a recycling basis for contributing to the safenetwork, and spent for adding data, but never for retrieving data.

## Why

Inspired by the principles of data ownership, security and integrity, the safenetwork puts control back into the hands of individuals. Uncensorable blogs, anonymous online societies and marketplaces, as well as privacy in general necessitated the creation of this safenetwork.

The ability to switch between being completely anonymous and instantly recognizable all within the same system with any reputation based on actions and ideas isn't new. In fact, this was the vision of the original internet, before it became centralized and inefficient. This safenetwork and the protocol that it introduces allows for some fundamental rethinking of how not only computers, but individuals can act and interact.

Above all, as has been said many times within this ecosphere, is that this safenetwork allows for privacy, security, anonymity, and liberty for all.

## When

The MVP (Minimal Viable Product) is _set to arrive any day now_, however, that is not a full release of the safenetwork. The MVP includes both client-side and vault (Server) applications that are able to be run behind any router in any setting.

After Safecoin (exactly what it sounds like) as well as the Messaging protocol are implemented, the safenetwork will stabilize in the midst of several Beta releases.

During this point, upgrades will wipe data from any vaults, effectively resetting the safenetwork by clearing all of the data on the safenetwork. Once the core code and API is considered stable, the upgrade to v1.0 will eliminate the possibility of an additional reset, and v1.0 will be considered to be released. Any additional updates will be done on a rolling schedule and handled by both the Companies, as well as other "pods" that are envisioned to come together throughout the world - incorporated independently or not - to provide additional developers to expand the core code of the safenetwork.

## How

This technology has the capacity to be global or local. It is possible to both segment the safenetwork independently, or even begin a brand new safenetwork from scratch by bootstrapping. However, like most decentralized technologies, the security of the safenetwork increases exponentially with a linear increase in participation. So while the impetus is to grow and keep growing, anyone can fork the safenetwork and release it howsoever they choose.

The core of the safenetwork is written in [Rust](https://www.reddit.com/r/rust), which previously had been C and C++ up till 2015, when the team decided that the savings and security inherant in the Rust language was worth a re-write of the entire codebase. In doing so, they brought close to one million lines of code down to several tens of thousands, increasing both readability and test coverage.

## Conclusion

This safenetwork has been years in the making, and is poised to be released on the world. Technically, it is mind-bending. Philosophically, it is enlightening. Potentially, well, that's up to you and what you do with it.

Discussion and announcements are updated here frequently, but our main hangout is at [forum.safenetwork.io](https://forum.safenetwork.io). Stop on by, ask some questions, and soon, you can subscribe to our weekly newsletter.

Our moderators are:

* /u/fabrunelle
* /u/SMACz24
* /u/happybeing
* /u/upstatekentucky

Feel free to drop them a PM if you have any questions about this community and welcome. Glad to have you here. Here are some links for you:

#### You're probably on a list now. Enjoy!
