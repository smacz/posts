---
layout: post
title: "Explanation of email servers"
date: 2016-09-25
category: SysAdmin
program: Postfix
process: Architect
image: email.jpg
description: "A quick overview of all of the moving pieces that make up the internal workings of email servers."
---

## What is an Email Server?

An Email Server is a place that when people send email to you, it gets stored
until you can read it. Gmail/Yahoo/MSN all keep your emails there for you to
access. This makes it so that anyone can send you a message without you being
there to recieve it. It's called asynchronous communication. But the sent
message can't be floating 'round the interwebz forever (how we have the internet
set up for the time being) so it needs to be stored somewhere. And that
somewhere is on the server.

## Why not Gmail?

Are you f\*\*king kidding me? In this situation, it's because you want to have
it stored locally. Also, I want the practice in being able to administer the
system. But how cool would it be to have your own personal cloud at home.
Regardless, for the time being it's just your own personal email server at home.

## Do I need a domain?

Yes. There are two ways to get one it seems though. With a personal internet
connection from a consumer-level ISP, the connection will typically be a dynamic
one. That means that your IP address will constantly be changing at the ISP's
whim. Either you can hop onto a DDNS server or pay up for a static IP.

### Static IP

Having a static IP would be the easiest way to go about doing things. Then
again, you'll need two of them for a functioning email server, and it'll take
some funds to get those IP addresses secured. And after they're obtained, they
would require extensive home network configuration to achieve. This'll probably
be detailed out later here, but for the time being, I will stick with my DDNS
setup.

### DDNS

Alternatively, you can register with a DDNS server. Typically you can add a
subdomain for free, like [*xxx*.no-ip.com](https://noip.com/blog/2010/10/27/how-does-dynamic-dns-work/) or [*xxx*.hopper.pw](https://hopper.pw/about). [Some Examples](lifehacker.com.au/2014/04/the-best-free-alternatives-to-dyndns/)
Replace the "*xxx*" with your subdomain name, and you can link back to your 
machine. This is fairly basic...when you're talking about http pages. It gets 
much more complicated when it comes to email though.

This will install a client in your server to keep track of which IP address it's
at so that it can forward mail to the appropriate location. Nifty, huh? 

Not so fast. As I'll cover later, it's *really really hard*.

## What software do I need?

A couple different kinds. I don't believe in web clients as it teaches the wrong
lesson, so I'll stick to just the basics. This is what I'll be using: 

* **Mutt**: to view the message and respond accordingly; the MUA
* **Postfix**: to send and receive email; the MTA 
* **Dovecot**: for IMAP/POP3; the MDA
* **SpamAssassin**: to keep spam out of your inbox; 
* **ClamAV**: to filter out viruses
* **Sieve**: to set up mail filters and rules

This *is* open source though, so there are many other programs that can be used.
These are just the ones that I will attempt to make play nice for now.

### Mail User Agent

The endpoint of the email process. This is where emails are opened and where
emails are sent. The "client". Examples:

* Mutt 
* Evolution
* Kmail
* Thunderbird
* Claws-Mail

### Mail Transfer Agent

This is the server itself. This is what accepts the email after you hit send,
and finds the other MTA that belongs to the recipient's email address
(presumably another MTA). Examples:

* Postfix
* sendmail
* Exim
* Zimbra

### Mail Delivery Agent

Sorting, scanning, filtering, and storing are all under the domain of the MDA. 
It's a go-between of sorts between the MTA and the MUA. Examples:

* Dovecot
* Cyrus
* Courier


## What are the differences between SMTP, POP3 and IMAP?

### SMTP

SMTP is the *"Simple Mail Transfer Protocol"*. In short, it *sends* the emails.

### POP3 and IMAP

The two of these are the retrieval options that an email client has when it
wants to retrieve a message. This one is pretty much black-and-white:

* POP3 downloads the messages onto the local computer
* IMAP reads the messages from the remote server

There are advantages and disadvantages to either protocol, and it depends on the
use case as to which one would be preferable. For a home mail server, it would
be easiest to leave them on the server itself, as it has plenty of disk space.

## What about spam and viruses?

* Spam blacklist server
* Attatchment scanning
* Reject unknown sender domain
* Reject suspicious header messages (cherry-pick-y)

To foreshadow a complication that will be run into down the line, a common
rejection test is to match the base domain's IP address to the IP of which the
email originated from. If those two don't match, the test is not passed, and the
mail either gets bounced or dropped.


## Going offline

So...what if AT&T on my block goes down for a line repair. Then what happens?


## What is an "MX Record"

### Do I need MX Records for my sub-domain?

### Where is it distributed to?

## How does mail get sent through STMP with a DDNS

## How does mail get recieved with a DDNS
