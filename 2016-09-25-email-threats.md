---
layout: post
title: "Email Threats"
date: 2016-09-25
category: SysAdmin
program: Postfix
process: Architect
description: "Threats to email while it is in transit."
---
## In Transit

I plan on making this into a four part memorandum on email insecurity, best
practices, and recommendations. Wish me luck.

## Email is fundamentally insecure

Read ProtonMail's [First three
paragraphs](https://blog.protonmail.ch/protonmail-threat-model/)

[Dumbed-the-fuck-down explanation](cbsnews.com/nets/email-privacy-what-you-need-to-know/)

[Wikipedia Email Privacy](en.wikipedia.org/wiki/E-mail_privacy)

[Email Law for All -- NOLO](nolo.com/legal-encyclopedia/email-privacy-29610.html)

* In transit
	* MITM/Eavesdropping
	* Server Compromise
	* Packet Sniffing
	* Relationship Graphing
	* Location Tracking
	* Content Decryption
	* Key/Certificate Forgery
	* Message Substitution


### Sender/Reciever to Transit Relationship Profile

Sending or Recieving an email involves it travelling (in most cases) across the
open web. There are many dangers out there, and exploits to be exploited. Here's
a couple of hypotheticals of what could happen after the message has left point
A, but hasn't quite reached point B yet.

## Surveillance

On Facebook, there's an app that lets you see who has been following your posts
most closely. Who has been looking the most at your activity. I can't honestly
say I've ever heard one of my friends NOT mention the word "creeper" in one of
those conversations, usually referring to a casual acquaintance showing up in
the top of the results. 

No one likes to be followed too closely by people who have no good reason to. If
you believe that advertising companies have hit it spot-on with their latest
pitch to you and you couldn't fathom not hearing about the next great penis
enlargement pill, then this post (read: site) is not for you. If not, then I
would assume that the same would go for email messages. I don't want people
logging every time I send an email and to who. If some hacker knows that my
account usually goes untouched during a certain time of day or knows that a
flood of emails are sent from what appears to be my work email to my home email
right before I start sending emails from my home email address, it wouldn't take
Einstein to guess (approximately) when I leave work, and how long it takes me to
get home and settled. Also, it will give off the physical IP address that could
correspond to the physical locations of where the emails are being recieved.

If that's not the definition of a creeper, I don't know what is. 

### VPN

### Message Encryption

### Hide client IP address


## Man-in-the-Middle (MITM)

MITM is most likely the scariest of all the threats that an email faces in
transit. This is the one where the message that reaches your recipient isn't the
one that you sent, it was the one that the Man in the Middle of your line of
communications sent. (See where the name comes from now?)

Now a MITM attack doesn't always mean that the message could have been tampered
with, but it does mean that your lines of communication aren't secured properly.
Mostly though, it does mean that the message could have been tampered with.

A successful Man in the Middle attack can lead to an adversary supplying false
information to each side while the transactions appear to be legitimate. There
is very little indication that a Man in the Middle attack is taking place and
little to do to stop it. 

### Message Authentication

### Perfect Forward Secrecy


## Content Scraping

Advertisers *love* content scraping. Any email whose message they can read (and
since email is by default sent via plaintext, that is most of them) they can
scrape keywords, data, or names from. Now if Bob's sending many emails to Alice
about some shoes that he wants to get, he may get more targeted ads about shoes.
Sure, whatever. 

But what if a confirmation email from Amazon (where he ended up getting the 
shoes) lists the shipping address for Bob? Now he's getting flyers from Designer 
Shoe Warehouse because the advertisers sold his information to them for pennies
on the dollar, even though he's already gotten shoes. Can't they see it's a
waste? Maybe they can, and have a plan.

What if they then sell the information to JCPennies, because he probably wants
some socks to go along with those shoes. After JCPennies uses his information to
target him, they sell his information to Aarons, because Bob is probably going
to need lease a washer and dryer for all of the socks that he is persuaded to
buy from the JCPennies flyers that are showing up at his house daily.

Lest we forget, the original advertisers who scraped the email content in the
first place still have a valid copy of Bob's information and will continue to
sell it to the highest bidder. The saying I belive goes something like:

> Once it's on the internet, it's out there forever.

### Message Encryption


## Centralization

Since centralization occurred once the internet stopped being generated in the
hands of the users, and started being owned by the servers that ran it, it
became orders of magnitude easier for hackers to select a targeted demographic.

For the purpose of disecting in-transit threats, server will mean a "node" that
the message passes through on the internet on its way to its destination.
Comparable to a USPS Post Office's role in making sure a letter goes where it
needs to. Now some people choose to send postcards, and some choose to seal
their tax returns. I don't think anyone has ever send their tax return on a post
card, or even an embarassing love letter, or an expense report. But each time an
email is sent in plaintext, each server can view the contents of the message.
Since their job is to scan and take action on the destination part of the
message, it's not much more to scan *the rest of the data* if it's open.

This shouldn't happen, but it does. I need to let the server know **not** what
I'm sending, but where to send it.

My question is, why haven't we used computers to be able to obfuscate the source
and destination to the outside world, visible only to the servers, while sealing
up the contents, instead of exposing the content and broadcasting the message
route. Seems like the first step to create a more trusting society to me. We
wouldn't have to worry about these types of side-channel attacks.


## Server Backdoor

To continue the USPS metaphor, have you ever dropped off a letter in a
streetcorner mailbox, only to see it get dropped into a hole underneath the
mailbox into a tube in the concrete that leads to some guy in his parent's
basement who just has the most intense urge to read your mail, and states that
by doing so he's keeping you safe from yourself? No. I didn't think so.

Nor would I want my mail to be vulnerable to a compromised server. If *insert
name of foreign power we're at war with at the moment* took over a local box,
I wouldn't want all of my mail to be forwarded to wherever to be opened and
dissected. 

I also wouldn't want my letter to be delayed or redirected. One compromised
server can be incredibly useful to an adversary who wants to at very least
frustrate me, and at most decimate me. 


## Encrypted Messages

[Enigmail with GnuPG in Thunderbird](securityinabox.org/thunderbird_encryption)


## Targeted vs Dragnet Weakness


## FAQ

[Ask Leo!](ask-leo.com/email_privacy.html)

[Lifehacker ch. 1](lifehackerbook.com/ch1/)


# Email Providers

I plan on making this into a four part memorandum on email insecurity, best
practices, and recommendations. Wish me luck.

## Email is fundamentally insecure

Read ProtonMail's [First three
paragraphs](https://blog.protonmail.ch/protonmail-threat-model/)

[Dumbed-the-fuck-down explanation](cbsnews.com/nets/email-privacy-what-you-need-to-know/)

[Wikipedia Email Privacy](en.wikipedia.org/wiki/E-mail_privacy)

[Email Law for All -- NOLO](nolo.com/legal-encyclopedia/email-privacy-29610.html)

### Sender/Reciever to Email Provider Relationship Profile

Most people will choose to have an email provider handle the heavy lifting for
them. They'd rather have Gmail store their mail than keep it on a server at
home. They'd rather have Lavabit be in charge of handling the specifics of where
their gateway is and have a third party supply the domain where they can access
their email if they're away from home. In essence, they'd rather not hold the
keys to their own gate. It's easier that way, but there's much more that needs
to be considered if that is the case.

## Realistic Expectation of Privacy

How much privacy do you actually expect of someone who recieves your mail. If
you've ever gone out of town and asked someone to gather your mail for you, the
answer is easy, maximum privacy. You wouldn't expect the neighbors boil a pot of
water to secretly steam open your mail to read your bank statements and credit
card offers, would you?

Of course not. But despite Citizens United, corporations are not people. They
don't believe that you actually think they won't access your emails. They think 
that if the mail is stored for long enough, they have a right to look at it no
matter what. They think that all that data iz bel0ng 2 dem. No. That's not how
it's done. 

But if you're storing *your* data on *their* servers and you don't think you're
paying for that product, you are more than likely paying with the most finite
currency on the planet -- your own personal privacy.

[Gmail 1](icontact.com/blog/email-news-google-responds-to-gmail-privacy-concerns/)


## Business-Level Security

[net-security.org](net-security.org/article.php?id=816)

### Employees Privacy 

[Email Privacy Concerns](consumer.findlaw.com/online-scams/email-privacy-concerns.html)


## Webmail Clients

Don't use them except in case of emergency. I like a allude to browsers as a
portal to the internet -- just as an email client or a torrent client is, but a
client that is made to handle many different use-cases, many of which are
insecure and vulnerable to side-channel attacks even when not active. 

I can't stress this enough. Do. Not. Use. A. Browser. For. Email.

### Tails

### Clients

## Rule of Three

[Strategies for Multiple Email Accounts](oeupdates.com/managing-multiple-email-accounts)

[Magic Number](catalogs.com/info/b2b/how-many-email-addresses.html)

[PollDaddy](polldaddy.com/poll/5025310)

[10 Reasons](performancing.com/10-reasons-to-have-more-than-one-email-address/)

[Spam and other Unwanted Mail](neomailbox.com/features/disposable-email-addresses)

[9 reasons](getapp.com/blog/multiple-email-addresses)

*download from ongage.com*

[Why you should](flyingcarstrategies.com/multiple-email-accounts)

## Sharing Email address

[Security by Obscurity](themailadmin.com/2010/03/4-ways-to-protect-email-addresses-on-websites-that-dont-really-work/)

## Targeted vs Dragnet Weakness


## Database Hacking

DNS is the Domain Name Service that controls the internet. Literally. The DNS
service tells you that "google.com" should interact with the IP address
"8.8.8.8". 

If Eve got a server under her control to have any request for "gmail.com" to be
redirected to the server she owns at "8.8.8.7", you may think you're logging
into 




## FAQ

[Ask Leo!](ask-leo.com/email_privacy.html)

[Lifehacker ch. 1](lifehackerbook.com/ch1/)


# Reciever

I plan on making this into a four part memorandum on email insecurity, best
practices, and recommendations. Wish me luck.

## Email is fundamentally insecure

Read ProtonMail's [First three paragraphs](https://blog.protonmail.ch/protonmail-threat-model/)

[Dumbed-the-fuck-down explanation](http://cbsnews.com/nets/email-privacy-what-you-need-to-know/)

[Wikipedia Email Privacy](http://en.wikipedia.org/wiki/E-mail_privacy)

[Email Law for All -- NOLO](http://nolo.com/legal-encyclopedia/email-privacy-29610.html)


* Mail Client
* Crypto Implementation
* Malicious Agents
* Message Storage/Logs
* Key Logging

* Reciever
	* Malicious Attachments
	* Malicious Links
	* Phishing Attack
	* Rich Text Code Execution
	* Receipt Request



## Email Clients

Don't use a web-based email client. Just don't. Browsers are incredibly
insecure. There's no need to access email through a browser. Use a client.

A client is to a browser what a wormhole is to a tear in the space-time
continuum. If that makes sense. Browsers are still dangerous, but the attack
vectors are much more limited with a client, and even though they are still
there, desktop email clients share a philosophy with unix's long lasting one,
namely:

> Do one thing right and do it well.

That being said, that one thing it does is pretty damn complex. There's a lot
going on with email, even without worrying about security. The bad news is that
bad guys will always find something to exploit. The good news is the exploits
are infrequent and usually quite trivial.

[Protect Against Vulnerabilities](http://spamlaws.com/email-client-vulnerability.html)

[Thunderbird and Multiple email addresses management](http://superuser.com/questions/378422/thunderbird-and-multiple-email-addresses-management)

[Set up Thunderbird for Multiple Email accounts](http://makeuseof.com/tag/how-to-set-up-mozilla-thunderbird-3-multiple-email/)

[lifehacker](http://lifehacker.com/5925096/should-i-be-using-a-desktop-email-client)

[The wrong reasons...](http://makeuseof.com/tag/stop-desktop-email-clients-stop-opinion)

### Endpoint Security

Employing an email client is certainly a step in the right direction, but just
as the greatest scripts were kept pristine in the Library at Alexandria, they
couldn't survive the burning no matter how well they were kept, your environment
must be kept secure if your email is to be secure as well. 

Like any good sysadmin would warn, make sure to keep software up-to-date and
merge the appropriate patches, but moreso make sure that you're connecting
securely. Check and re-check the protocols. Make sure that the connection
between the client and the server is not tamperable or leaking any information,
and that the connection itself is secure and/or trusted.

You may see endpoint security being toted as mainly a marketing term to push new
product, but while is perhaps a little bit too wide-ranging, it does hit on the
big picture very well when it comes to the recieving end of at least email
security. Keep your connection and your computer safe and secured. 

[Definition](http://webopedia.com/TERM/E/endpoint_security.html)
[TechRepublic Interpretation](http://techrepublic.com/blog/it-security/endpoint-security-what-makes-it-different-from-antivirus-solutions/)

### Strong Passphrase

## Sharing Email address

[Security by Obscurity](http://themailadmin.com/2010/03/4-ways-to-protect-email-addresses-on-websites-that-dont-really-work/)



## Crypto

Of course the key to a strong email system is good, strong crypto. No if, ands,
or buts about it. Crypto is what lets you establish a "secure" connection. It's
what lets information be "trusted". Its establishes what can be "verifyable".
It lets data be "unhackable".

That last one may be (quite) a stretch, but it's the strongest guard that you
have against malicious intent, whether that be corporate espionage, governement
surveillance, or your little sister reading your messages to your girlfriend.
Only ones who are granted the power can unscramble what you scrambled in the 
first place. Only they hold the keys.

### Key Management

### Public Key Infrastructure

This is HEAVY Doc...

[Wiki](http://en.wikipedia.org/wiki/Public_key-infrastructure)

[overview of the problem](http://itsecurity.com/security.htm?s=479)

[the key-distribution problem (talking points)](http://cs.wellesley.edu/~cs310/lectures/public_key_slides_handouts.pdf)

[More talking points](http://polycephaly.org/teachingslides_KMD.pdf)

[Public Key Crypto Solves Half the Problem](http://entrust.com/public-key-solves-half-key-distribution-problem/)

[LJ Distributed Hash Table](http://linuxjournal.com/article/6797)

[UseNix.org DHT](http://usenix.org/legacy/event/osdi00/full_papers/gribble/gribble_html/node4.html)

[Scholars Archive](http://scholarsarchive.byu.edu/etd/309)

### Decrypting Messages

Decryption is a very easy process. It's also full of magic. 

### Verifying Messages/Sender

Verification is done crytographically as well. Did you know that? There's a
thing called a "MAC" or a Message Authentication Code that can be tacked onto
the end of a message to verify that the message that was sent is the message
that was recieved. 


## Malicious Agents/Social Engineering

Any experienced baddie will tell you that the easiest way to crack into a system
is not by using bleeding-edge software or the most powerful computers, but
tricking someing into giving away their password. Sometime's it's as easy as
impersonating your neighborhood IT guy, or your boss, or whatever. It's as easy
as this:

> Don't give anyone your password. Ever.

There are other rules such as don't write it down, etc., but the bottom line is
that humans are not as infallible as are computers. Breaking a person is much
easier than braking a cipher [Relevant XKCD](https://xkcd.com/538/).

[Social Engineering Overview](http://business.uni.edu/buscomm/ElectronicComm/EmailSecurity.html)

[25 Common Mistakes (That can be defeated by common sense)](http://itsecurity.com/features/25-common-email-security-mistakes-022807/)

[Wikipedia](http://en.wikipedia.org/wiki/Email_hacking)

### Attachments

Don't execute attachments that you didn't send to yourself. Easy at that.

### Plain Text vs HTML/rich text

Plain text is much more secure, but you can do soooo much more with rich text.

### Return Receipts: [Web Bugs](http://email.about.com/od/staysecureandprivate/a/webbug_privacy.htm)

### Phishing 

[Phishing Email Attack -- Going for your Benjamins](http://tccsa.net/content/phishing-email-attack)

[Here's Why Companies Keep Losing the Battle Against Hackers](http://bloomberg.com/news/articles/2015-04-15-here-s-why-companies-keep-losing-the-battle-against-hackers)

### Scams

[Smarter to be lucky than lucky to be smart](http://hackyourlove.com/articles/email-password-hacking/)

### Spoofing


## Message Storage


## Key Logging

[The Easiest Way!](http://gohacking.com/hack-email-account-password)

[Birthday Special](http://hackingloops.com/2010/10/how-to-hack-email-account.html)

[Wikipedia](http://en.wikipedia.org/wiki/Keystroke_logging)

[WebOPedia](http://webopedia.com/TERM/K/keylogger.html)

[Infosec Institute](http://resources.infosecinstitute.com/keylogger/)

[Top 10](http://vagueware.com/top-10-keylogger-software)


## Spam

### Disposable email addresses

A fun way to combat Spam and identification.

[Mailinator](http://mailinator.com)



## FAQ

[Ask Leo!](http://ask-leo.com/email_privacy.html)

[Lifehacker ch. 1](http://lifehackerbook.com/ch1/)


# Sender

I plan on making this into a four part memorandum on email insecurity, best
practices, and recommendations. Wish me luck.

## Email is fundamentally insecure

Read ProtonMail's [First three
paragraphs](https://blog.protonmail.ch/protonmail-threat-model/)

[Dumbed-the-fuck-down explanation](cbsnews.com/nets/email-privacy-what-you-need-to-know/)

[Wikipedia Email Privacy](en.wikipedia.org/wiki/E-mail_privacy)

[Email Law for All -- NOLO](nolo.com/legal-encyclopedia/email-privacy-29610.html)


## Points of Failure

* Mail Client
* Message Storage
* Message Logging
* Crypto Implementation
* Key Logging

## Email Clients

[Protect Against Vulnerabilities](spamlaws.com/email-client-vulnerability.html)

[Thunderbird and Multiple email addresses management](superuser.com/questions/378422/thunderbird-and-multiple-email-addresses-management)

[Set up Thunderbird for Multiple Email accounts](makeuseof.com/tag/how-to-set-up-mozilla-thunderbird-3-multiple-email/)

[lifehacker](lifehacker.com/5925096/should-i-be-using-a-desktop-email-client)

[The wrong reasons...](makeuseof.com/tag/stop-desktop-email-clients-stop-opinion)

### Endpoint Security

[Definition](webopedia.com/TERM/E/endpoint_security.html)

### Strong Passphrase

### Sharing Email address

[Security by Obscurity](themailadmin.com/2010/03/4-ways-to-protect-email-addresses-on-websites-that-dont-really-work/)


## Crypto

### Key Management

### Public Key Infrastructure

This is HEAVY Doc...

[Wiki](en.wikipedia.org/wiki/Public_key-infrastructure)

[overview of the problem](itsecurity.com/security.htm?s=479)

[the key-distribution problem (talking points)](cs.wellesley.edu/~cs310/lectures/public_key_slides_handouts.pdf)

[More talking points](polycephaly.org/teachingslides_KMD.pdf)

[Public Key Crypto Solves Half the Problem](entrust.com/public-key-solves-half-key-distribution-problem/)

[LJ Distributed Hash Table](linuxjournal.com/article/6797)

[UseNix.org DHT](usenix.org/legacy/event/osdi00/full_papers/gribble/gribble_html/node4.html)

[Scholars Archive](scholarsarchive.byu.edu/etd/309)

### Encrypting Messages

### Signing Messages


## Message Storage

### Physical Access

## Key Logging

Basically, the one thing you can hardly do anything about.

[The Easiest Way!](gohacking.com/hack-email-account-password)

[Birthday Special](hackingloops.com/2010/10/how-to-hack-email-account.html)

[Wikipedia](en.wikipedia.org/wiki/Keystroke_logging)

[WebOPedia](webopedia.com/TERM/K/keylogger.html)

[Infosec Institute](resources.infosecinstitute.com/keylogger/)

[Top 10](vagueware.com/top-10-keylogger-software)


## Gaining Access
## FAQ

[Ask Leo!](ask-leo.com/email_privacy.html)

[Lifehacker ch. 1](lifehackerbook.com/ch1/)


