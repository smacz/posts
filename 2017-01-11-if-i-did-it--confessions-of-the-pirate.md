---
layout: post
title: "If I Did It: Confessions of the Pirate"
date: 2017-01-11
category: Battlestation
program: Calibre
process: Install
repo: 
image: thepiratebay.jpg
description: "I needed a book that was simply a collection of essays by what looks to be a quite mentally unstable professor that was assigned by an assuredly mentally unstable professor. The only reason I went through this much effort was to rip this book to shreds during our weekly write-ups on the chapters."
references:
  - title: How to Import/Export OVA Files in VirtualBox
    link: https://www.maketecheasier.com/import-export-ova-files-in-virtualbox/
  - title: "DeDRM Plugin for Calibre: The Simplest Option for Removing DRM from Most ebooks"
    link: https://apprenticealf.wordpress.com/2012/09/10/calibre-plugins-the-simplest-option-for-removing-most-ebook-drm/
  - title: /u/didjital explains how to use the DeDRM plugin in Linux using a Windows VM
    link: https://www.reddit.com/r/ebooks/comments/2muccd/remove_drm_restrictions_from_almost_any_type_of/cm8f5gt/
  - title: /u/beethoven_freak explains how to bypass the 'Cannot decode library or rented ebooks.' error
    link: https://www.reddit.com/r/Piracy/comments/1lvgjk/removing_drm_from_rented_ebook_an_azw4_file_from/
  - title: DRM Removal Tools for eBooks Overview
    link: https://apprenticealf.wordpress.com/2012/09/10/drm-removal-tools-for-ebooks/
---

# Virtual Machine

## Windows Image

First, one must [download Windoze](https://developer.microsoft.com/en-us/microsoft-edge/tools/vms/), and wait, It'll be awhile. I would suggest taking this time to workout. One might prefer a variant of the [prisoner's workout](http://www.marksdailyapple.com/prison-workout/) to pass the time.

## Virtualbox

I've used Virtualbox in the past, and it'll work just fine here. However, the Windows 10 that I downloaded came in an OVA format.

> OVF, also known as Open Virtualization Format, is one of the way to standardize virtual appliance so they can be imported and use in different virtualization software. A virtual appliance in OVF format usually consist of a series of files. The OVA format is merely a single file distribution of the OVF, stored in the TAR format. In this tutorial, we will show you how to import and export an OVA file into/from Virtualbox.

```
File -> Import Appliance -> Win10_Preview.ova -> Open appliance
CPU: 1
Memory: 4096
-> Import
```

And it takes a couple minutes.

## Shared Folder

Make sure you set up the shared folder before you boot the VM on. Also, choose automount.

#### If you happen to lock the session and are prompted for a password, [`Passw0rd!` works](https://superuser.com/questions/629632/whats-the-default-login-for-the-windows-xp-ie6-vpc-image-supplied-by-micosoft), even for the user `IEUser`.

## Kindle for PC

Inside of the VM once it's started up, open up Edge and download [Kindle for PC](https://www.amazon.com/gp/kindle/pc/download). Run the `.exe` and install it. At this point, it will ask you for an Amazon account. How you get that set up is up to you. There are ways.

Then, buy the ebook. And when you see buy, read rent. It's cheaper than purchasing it, and it'll be DRM free in no time. One might wish to pay close attention to the price margin between purchasing and renting the book to determine the value of one's time in relation to whom ever else may desire that ebook.

#### Kindle for PC 1.17 is needed for `.awz{,3} ` books, as anything newer downloads an incompatible book format that has not been cracked...yet.

# DeDRM

DeDRM allows you to remove the DRM on ebooks. There's also a plugin for Calibre. So, install Calibre from whichever repos you're using, and then download and extract the [latest version of DeDRM](https://github.com/apprenticeharper/DeDRM_tools/releases).

Installing the plugin to Calibre is fairly simple. First though, there exists a need to change the plugin to allow us to decode rented ebooks. [/u/beethoven_freak's discovery is hilariously simple](https://www.reddit.com/r/Piracy/comments/1lvgjk/removing_drm_from_rented_ebook_an_azw4_file_from/) and really shows the audacity of DRM as a whole. Anyways, once the amended plugin is installed, there should be nothing more to do.

The [plugin installation instructions](https://apprenticealf.wordpress.com/2012/09/10/calibre-plugins-the-simplest-option-for-removing-most-ebook-drm/) are fairly straightforward, and the plugin is contained inside of the latest version archive that was downloaded previously. However, there is a bit of setup to do.

Since Calibre is running in a base Linux system, the k4i keys that are used to decrypt the files in the "Kindle for PC" app on the windows VM need to be imported. [/u/didjital lays it out nicely in his post](https://www.reddit.com/r/ebooks/comments/2muccd/remove_drm_restrictions_from_almost_any_type_of/cm8f5gt/), and completing those steps results in valid decryption keys for ebooks.

At this point, simply restarting Calibre and then importing the ebook into Calibre should remove the DRM protection. If it is able to be read, either it didn't have DRM in the first place, or the DRM has successfully been removed. Otherwise, take a look at the log after removing the book, restarting Calibre in debug mode, reimporting it, and then closing Calibre. At that point DDGo is your friend.

# Stop! Philosophy time.

I hope that I don't have to convince anyone reading this that intellectual property and DRM is bad (M'kay?), but if I do, I would strongly recommend [Stephen Kinsella's free ebook - _Against Intellectual Property_](https://mises.org/library/against-intellectual-property-0) as well as [Cory Doctorow's talk "Security and feudalism: Own or be pwned](https://www.youtube.com/watch?v=duG55M8t0sc) as jumping-off points. It's a contentious issue, for sure. Hell, take Steward Brand's often misquoted aphorism:

> On the one hand information wants to be expensive, because it's so valuable. The right information in the right place just changes your life. On the other hand, information wants to be free, because the cost of getting it out is getting lower and lower all the time. So you have these two fighting against each other.

However, I can say this for a fact: Disrespecting private property is the root of all evils. The approach that is currently being taken has been nothing but trouble, and completely ineffective at the same damn time. There is nothing but more and more darkness down this same road.

In that vein, I would hope that any ebooks that may be liberated using these methods would find their way onto private or public trackers, as the torrenting community has proven to be the most resilient caretakers of knowledge and data. Wikipedia's got nothing on them. As for what comes next or how this all shakes out in the end, I can only hope that a successful sustainable model is found. Until then, hackers...are our only hope.
