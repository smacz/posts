---
layout: post
title: "Security Settings for Iceweasel"
date: 2015-02-17
category: Battlestation
program: Firefox
process: Config
description: Tweaking Iceweasel's default settings to provide more control and peace of mind when it comes to security.
---


## What is a browser, *really*?

So browsers are INCREDIBLY insecure. I like to view them as a portal to the
internet, but not as, say, ssh or wget is. No, this is a portal that has to
anticipate all use-cases, and basically handle anything that any web-dev has to
throw at it. And trust me, they have a shit ton of weapons in their arsenal. Not
only does it need to render everything flawlessly, it has to do it securely and
quickly. Now, in all practicality, no tool exists that is perfectly flexible,
secure, and fast; it's always a compromise between those three factors. You,
however, can help Iceweasel by making a few small sacrifices, something a mature
person should have no problem doing. And in doing so, you'll secure yourself,
you'll secure the web, and you'll promote responsibility world-wide. Think I'm
overexaggerating? Check out how much revenue comes out of web advertising and
scamming every MONTH. Even the playing field with your knowledge.


## Peace-of-Mind vs Ease-of-Use

These two may seem to always be at odds with each other, and to a point it's
reminiscent of the ongoing struggle of cryptographers vs cryptoanalysts, never
seeming to end in this cat and mouse game, but they can occasionally play nice
with each other. It's like I said, it taks a little sacrifice. But there are
ways to work around it, and the time invested upfront will lead to a mutually
beneficial compromise between security and user friendliness. Keep that in mind
while I outline these following ideas.

Keep in mind, before (but mainly after) installing/settings these
addons/configs/whatever-this-morphs-into, *Read The Furnished Material*. There
may be functionality out the wazoo that you're not aware about because you were
too excited to get started searching for the latest celeb gossip. Don't be that
person. RTFM.


## Extra Credit

Read through the 13 pages on [TweakGuides.com](http://tweakguides.com/Firefox_1.html)

(It is meant for Windoze users, but is applicable in most respects to Linux
users also. Well, page 4 and beyond anyways.)


## History

Just a heads up, if you're using Private Browsing, skip to the next section.
This doesn't really pertain to you.

As any internet afficianado knows, the history of one's past browsing habits may
be that he/she has a vested interest in controlling access to, or at least
the duration and type of saved information. So, let's get to it.

To access and delete the history is a trivial matter. Access it with a `Ctrl-H`
and close the window with the same. The top right menu has history as one of the
prominent choices there which will let you view/delete/restore part/all of your
browsing history. Easy enough.

Firefox v33.1 added a new ["Forget" button](http://pcworld.com/article/2845193/firefox-boosts-privacy-with-history-wiping-forget-button-duckduckgo-search-support.html) that will let you delete the last
pre-determined timeframe worth of browsing history. 

And until I can think of a good reason to expand this section, I'm going to move
on to the reason why there's not much sense in this.

### Private Browsing

Private Browsing is not private. Not by a long shot. But it does delete the
cookies, passwords, search entries, and/or temporary internet files that you
picked up in your browsing. [Times article](http://nytimes.com/2014/10/16/technology/personaltech/hiding-history-in-firefox.html)

To implement, go to the Iceweasel preferences, and on the 'Privacy' tab, switch
the 'History' value to: 'Never remember history'. Done. If you access it off of
your desktop, you may be able to right-click and select it, but I don't use any
functionality of that sort.

This will NOT protect you from being spied on by any three-word agencies or your
ISP, employer, or hackers. This is solely to prevent your history to be
accessible if anyone were to access your computer itself. So some might say that
this renders this setting useless. I disagree

This will make sure that your system stays clean of any cruft that may build up
over time. Imagine finding this ability out for the first time, accessing it,
and seeing that the number of cached items that are on your computer are nearly
double the dollars in your annual salary. Decruft.

Also (barring a password manager and cookie manager) this keeps you secure by
forgetting your sensitive information after you logout. You ever see someone's
facebook get "hacked"? Yeah, someone stole their laptop at the sleepover and
locked themselves in the bathroom. Not much of a "hack" if you ask me, but one
that certainly can be avoided by practicing good browsing hygiene.

Now if you're worried about all of the cool pages that you stumbled on getting
lost in time, I urge you to consider the benefits of a well setup bookmark
management system. Something I plan to put together soon under the "useability"
umbrella.

## Proxy (or Why Not to Use One)

Icewease has an integration feature when it comes to using a proxy while
browsing the interwebz. 

[Basically this is nothing but the appearance of security.](dailyproxies.org/why-not-use-a-proxy-to-connect-to-internet/)

They are libel to share any and all information that is passed through them, and
in fact, all it does is add another step to the route that you sensitive data
(hopefully encrypted!) passes through. 

Not to say that proxies are useless. I used a proxy daily in the computer lab
all throughout high school. But that was because the school didn't implement
their site blocker correctly, and there were times where I needed to access
legitimate, but perhaps provocative, information. And it was just easier to use
a proxy rather than get a VPN for myself at that young age. Hell, bitcoin wasn't
around then and I didn't have any credit to speak of.

But they did slow down my browsing, and there was literally no advantage to
using it besides to get around a misguided attempt at restriction. And remember,
if you don't pay for the product...you may just be the product.

## Auto Update

Iceweasel is Debian's branding of Firefox. So there is really no updating that
needs to be done other than the regular system updates that I'm SURE are
properly implemented by your own selves. But what about keeping the rest of
Iceweasel up to date?

Well, this is easy. 

Extensions: Will automatically update

Security Patches: Will automatically update

Plugins: Addons -> Plugins -> "Check to see if your plugins are up to date"

Search Engines: Preferences -> Advanced -> Check box "Automatically update
Search engines

And if I find anything else, I'll add it later.

## Do Not Track feature

Now, it may seem counter-intuitive to suggest that you NOT enable firefox's
default tracking opt-out feature, but trust me, it's doing more harm than good.
All it's doing is politely asking the webserver if it'd be so kind as to not put
any cookies in this particular browser, and not store any information if at all
possible on the local machine.

After which it promptly gets flicked the finger and logged as a suspicious IP
for wanting privacy by the webserver and whoever the webserver is communicating
with (/is infultrated by). There are better ways of defeating agressive tactics
such as these. Mainly they involve tricking the system into thinking that they
have successfully installed their software and then purging it all to /dev/null.
Muahahahahaha.

## Permissions Manager

	about:permissions

Feel free to muck about. Several settings to look closely at:

* Store Passwords
* Share Location
* Use the Camera
* Set Cookies

Just take a second to imagine if those were all set to "Always Allow". Now you
can browse with a little more piece of mind that you have control over those
settings.

As an aside, this is the easiest place to manage my cookies. Keep in mind that
cookies allow you to stay logged in on a website. Without it, every new page
that you would visit would automatically log you out again, because you have no
cookies to indicate that you already successfully verified with that site. So
cookies aren't bad, but they are a tracking tool. 


## Flash

Flash is a very highly vulnerable proprietary protocol. If that sentence doesn't
convince you to go to `about:addons` and disable it, then perhaps a quick DDG
search will. I'll wait...go ahead. Good? Good. Now just freakin disable it. We
aren't in highschool playing that 2D motorcycle game anymore, Flash is just
unnecessary and going to be replaced by HTML5 anyways (did you see any of that
pop up in your search?). Wake up...it's time (for it) to die.


## Fingerprint/Anonymity

So each browser has a fingerprint that can be manipulated to be more or less
obscure based on how much information is given out. [Panopticlick.eff.org](Panopticlick.eff.org) is a very
effective site at demonstrating just how unique your browser is. 

### Javascript redux

Sometimes NoScript isn't enough. Sometimes you have to set `javascript.enabled`
to "False" in about:config. Maybe you should do that. Maybe you should do that
right now. Up to you. 

### User Agent String

Ever wonder what your browser's metaphorical license plate number is? Well, 
there's not really one. But your year-make-model would be comparable to your 
User Agent string. Change this and no one will know how you got where you were
going, but you showed up to the party. Unfortunately in Iceweasel, the setting
doesn't automatically exist to change this. So make it.

Go into `about:config` and search `useragent`. Click in the whitespace below the
two-three results and select "New" --> "string". Enter in
`general.useragent.override` as the preference and leave the string value blank.
You are now a veritible low-rider on the interwebz.

EDIT: See Security Plugins for Firefox for the addon version.


## That about wraps it up

Don't forget there are four parts to an iceweasel setup, and this is only one
of them. The other three are crucial as well in order to get a reliable,
well-functioning web browser up and running. 
