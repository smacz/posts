---
layout: post
title: "ELI5 Network+ Objectives"
date: 2017-01-29
category: SysAdmin
program: Network+
process: Metacognition
repo: 
image: Network_cert.jpg
description: "A good way to study for tests is to figure out which objectives you can easily explain because you know them, and which ones you can't because you don't. This is a quick overview of just that for my own studying purposes."
references:
  - title: Exam Objectives N10-006
    link: https://certification.comptia.org/docs/default-source/exam-objectives/comptia-network-%28n10-006%29_examobjectives.pdf
  - title: /r/CompTIA
    link: https://reddit.com/r/CompTIA
  - title: Wikipedia
    link: https://wikipedia.com
---

# [{{ references[1].title }}]({{ references[1].link }})

First off, I'd like to thank [{{ references[2].title }}]({{ references[2].link }}) for the sidebar. They've included the exam objectives, as well as study guides for all of the certifications. Also, the community there is quite knowledgable as well.

Anyways, the Network+ Exam Objectives are spelled out topic by topic. There should be no surprises on the exam as to the subject matter. Therefore, it would behoove me to read through the objectives to get an idea of what I'm up against.

Also given is a breakdown of the percentage of the total test that each section will make up. The range is from 16%-24% for five sections. There's not really going to be an advantage studying one over another. Likely, most of the questions will blend more than one of the areas together. So I'm just going to go through it all.

# 1.0 - Network Architecture

## 1.1 Explain the functions and applications of various network devices

* Router
    * Routes packets on layer 3 (IP Address)
    * used for subnetting networks
    * can create VLANs
* Switch
    * routes packets on layer 2 (MAC Address)
    * sometimes routes on layer 3
    * LAN-only
    * can recognize VLANs
* Multilayer switch
    * Switch that can make routing decisions on layers 3+
    * alternate types:
        * web switch
        * content switch (CDN)
        * load balancer (layer 4)
* Firewall
    * Block or allow ports from being accessed
    * typically whitelist paradigm
        * blacklist paradigm was obsolete by the 80's
    * Can be stateful or stateless
        * stateful means that it tracks connections between sources and destinations
        * stateless means that it evaluates each packet without taking anything previous into account
    * Can be hardware or software
        * Hardware:
            * Cisco
            * Juniper
        * Software:
            * iptables (Linux)
            * nftables (Linux)
            * Windows Firewall (Windows)
            * PF (BSDs)
        * PFSense is a hybrid model
* HIDS
    * Host Intrusion Detection System
    * Used to monitor for suspicious behavior
        * Network and packets
        * Program Access
        * System State
        * System Memory
        * Known Checksums
    * Typically uses a database of which system objects to monitor
    * Examples:
        * RKHunter
        * OSSEC
        * Fail2Ban
        * Anti-Virus
        * Many, many more
* IDS/IPS
    * Intrusion Detection/Prevention System
    * AKA: NIDS (Network Intrusion Detection System)
        * If not on host, then in network
    * Placed at strategic points in internal network to monitor traffic
    * Performs analysis based on known attacks and unusual activity
    * Can misreport
        * False Positive = Alarm when no intrusion happened
        * False Negative = No alarm when intrusion happens
* Access point
    * AKA: Wireless Access Point
    * Extends range of or implements wireless router
* Content filter
    * Application filtering based on OSI Layer 7 
    * Typically blacklist paradigm - can filter by:
        * DNS
        * IP Address
        * Email addresses
        * Filetype
* Load Balancer
    * Spreads the incoming traffic across multiple servers
    * Can act as a SSL termination point
* Hub
    * Layer 2 packet handling device
    * Virtually non-existent except in legacy networks
    * Can connect segments of a LAN
    * Broadcasts packets to all connected machines instead of routing the packet
    * Can facilitate collision detection with a jam signal to all ports
* Analog Modem
    * MODEM = Modulator/Demodulator
    * Sends digital signals in an analog form, and receives analog signals that it can convert to digital
* Packet Shaper
    * AKA: QoS
        * Quality of Service
    * Prioritizes packets of certain types over others when sending and receiving
    * OSI layer 7
* VPN Concentrator
    * Heavy-duty VPS termination point
    * Can be Site-to-Site, or Host-to-Site

## 1.2 Compare and contrast the use of networking services and applications

* VPNs
    * Site-to-Site
        * Terminated with special hardware
        * access to LANs on each side
        * Typically corporate or other large enterprise
    * Host-to-Site
        * AKA: Remote Access
        * Host gets access to Site's LAN
        * Tunnel from Host to site is secured at the host's NIC or equivalent software
        * Tunnel at the Site end terminates at hardware
    * Host-to-Host
        * Terminated by software or host-specific hardware
        * Secured channel, no access to LAN
        * Typically inefficient
    * Protocols
        * IPSec/L2TP
            * BASICALLY: Corporatized OpenVPN with limited cipher options
            * Payload is encrypted using 3DES or AES
            * UDP Packets only
            * Supports NAT Transversal
        * GRE
            * AKA: Generic Routing Encapsulation
            * BASICALLY: Tunneling protocol that encapsulates a variety of protocols inside virtual point-to-point links over IP
            * can be used in conjunction with PPTP
            * can literally encapsulate almost anything
        * SSL VPN
            * BASICALLY: VPN on a Browser
        * PTP/PPTP
            * BASICALLY: PPP frame encapsulated in TLS
            * Encapsulates PPP frames in IP datagrams
            * TCP connection for Tunnel Management
            * Data can be encrypted and/or compressed
            * Encryption keys generated from MS-CHAP v2 or EAP-TLS authentication
        * OpenVPN
            * BASICALLY: What you should be using
            * OpenSSL Library provides encryption
                * 3DES
                * AES
                * RC5
                * Blowfish
                * Twofish
            * Uses UDP by default on stable connections, UDP on unstable connections
* Authentication, Authorization and Accounting Protocols
    * RADIUS
        * AKA: Remote Authentication Dial-In User Service
        * Released in 1991
        * Uses UDP - ports:
            * 1812 & 1813 or
            * 1645 & 1646
        * Used in EAP
        * Authentication is not separate from authorization
        * Only password field is encrypted
        * Primary Use = Network Access
    * TACACS, XTACACS, TACACS+
        * AKA: (Extended) Terminal Access Controller Access-Control System (Plus)
        * Released in 1993
        * Uses TCP - port:
            * 49
        * Can separate authentication, authorization and accounting
        * TACACS+ will encrypt the entire packet
        * Primary Use = Device Administration
* RAS
    * AKA: Remote Access Services
    * VPN
    * SSH
    * VNC
    * Remote Desktop
* Web Services
    * Ports
        * HTTP = 80
        * HTTPS = 443
    * Used to retrieve web pages and other information located on the web
    * Client-Server model
* Unified Voice Services
    * VoIP
    * Redirect Numbers to central number
    * Only for voice communication
    * CONNECTION_PROTOCOL: UDP
    * Examples:
        * Skype
        * Google Voice
* Network Controller
    * Used in SDN Networks
        * AKA: Software Defined Networking
    * Programmable

## 1.3 Install and configure the following networking services appliances

* DHCP
    * Static vs Dynamic IP addressing
        * Static IP can be specified on the host or the server
        * Static IP doesn't change no matter how long the client is disconnected
        * Dynamic IP may change every time a lease time expires
        * Lease time is set for expiration of temporary client's ownership of an IP address
    * Reservations
        * X.X.X.0 is reserved for the network address
        * X.X.X.255 is reserved for the broadcast address
    * Scopes
        * Consecutive range of IP addresses that the DHCP server can lease to clients on a subnet
    * Leases
        * Lease times are specified by the admin
        * DHCP clients w/o a static IP address keep their IP address (w/o renewing) for the duration of the lease time
    * Options
        * DNS servers
            * Host the nameservers where packets can retrieve their routing addresses
            * can be used in conjunction with DHCP services
        * Suffixes
            * DNS suffixes (.com, .edu, .net) can be handed out to various authorities that point to IP addresses
    * DHCP Relay
        * Proxies the client's requests in a VLAN
        * re-routes DHCP requests to appropriate server
* DNS
    * DNS Servers
        * Maps IP addresses to human-readable IP addresses
    * DNS Records
        * A
            * IPv4 WWW address - typically port 80
        * MX
            * AKA: Mail Exchange
            * Points to the mail server for that domain
        * AAAA
            * IPv6 address for domain
        * CNAME
            * AKA: Canonical Name
            * Used to alias one name to another
        * PTR
            * AKA: Pointer
            * Points to a Canonical Name
            * Does not follow the CNAME to retrieve the IP address, just returns the CNAME
            * Used for Reverse DNS
* Proxies
    * Proxy
        * Changes the outbound destination of internet traffic to itself
        * Can forward response packets to originator of request
        * Masks origin of sender
        * Intercept all outgoing communications
    * Reverse Proxy
        * Can be used to obfuscate location of hosting
        * Acts as MITM that service provider controls
        * Accessors information does not go past proxy
        * Intercept all outgoing communications
* NAT
    * AKA: Network Address Transversal
    * Setup at the edge of a subnet - at the router typically
    * Similar to reverse proxy - takes original senders address and changes it, but can forward response back to original sender
    * PAT
        * AKA: Port Address Translation
        * Resolves conflicts that would arise through two different hosts using the same source port number to establish unique connections at the same time
        * Does not touch the IP address, only the port number
    * SNAT
        * AKA: Static NAT
        * 1:1 mapping of addresses
    * DNAT
        * AKA: Destination NAT
        * ALIAS: Port Forwarding
        * ALIAS: DMZ
        * Transparently changes the destination IP address of an end route packet and performing the inverse function for any replies
* Port Forwarding
    * Setup at the edge of a subnet - at the router typically
    * Typically utilized to translate connections incoming on a predetermined port to be forwarded to an internal IP address at a specific port

## 1.4 Explain the characteristics and benefits of various WAN technologies

* Fiber
    * Medium upon which signals can travel through
    * Uses light as the medium
    * can produce faster data transmission than copper
    * SONET
        * AKA: Synchronous Optical Network
        * Multiple digital bit streams synchronously using lasers or LEDs
        * Bidirectional Ring network
    * DWDM
        * AKA: Dense Wavelength Division Multiplexing
        * Optical signals multiplexed within the 1550 nm band
        * Developed to replace SONET
    * CWDM
        * AKA: Course Wavelength Division Multiplexing
        * Optical signals multiplexed within the 1270 through 1610nm band
        * Limited throughput as opposed to DWDM
* Frame Relay
    * Uses packet switching
    * Physical and data link layers of digital communications
    * Originally for ISDN infrastructure
* Satellite
    * Uses satellites to relay digital messages
    * High latency/throughput
* Broadband cable
    * Internet signal is carried across coaxial cable
    * Higher speeds than DSL for neighborhood
    * All connections share one "common" pipe
* DSL/ADSL
    * AKA: (Asymmetric) Digital Subscriber Line
    * Dedicated client line to ISP and back
    * ADSL promises higher download to client than client upload
    * Uses ordinary phone lines
* ISDN
    * AKA: Integrated Services Digital Network
    * Digital transmission over traditional circuits of the public switched telephone network
    * Circuit-switched telephone network system
    * Provides access to packet-switched networks
* ATM
    * AKA: Asynchronous Transfer Mode
    * Designed to handle both traditional high-throughput and real-time, low-latency
    * Maps to OSI layers 1-3
    * Competes with IP
    * used over SONET, PSTN and ISDN
* PPP/multilink PPP
    * AKA: Point-to-Point Protocol
    * Encapsulation protocol for network layer traffic
    * Can only go from one fixed point to another fixed point
    * Multilink can aggregate multiple WAN links into one logical channel
* MPLS
    * AKA: Multiprotocol Label Switching
    * Data-carrying technique for high-performance telecommunications networks
    * Based on short "path labels" rather than network addresses
    * Avoids the necessity for routing tables
    * Can encapsulate:
        * T1/E1
        * ATM
        * Frame Relay
        * DSL
* Cellular Radios/Channel Access Method
    * Divides the finite RF spectrum among multiple users as efficiently as possible
    * Cellular network - client searches for cells in the immediate vicinity
    * GSM
        * AKA: Global System for Mobile Communications
        * Works in a time-sharing paradigm like on a large computer
        * Separates used frequencies by cell
    * CDMA
        * AKA: Code Division Multiple Access
        * Uses digital modulation called "spread spectrum" which spreads the voice data over a very wide channel
        * All codes are pseudorandom
        * Decreases cell range and battery life due to pseudorandom computation
    * LTE/4G
        * AKA: Long Term Evolution
        * Standard for high-speed wireless communication
        * All-IP flat architecture
        * Latest Development
        * Fourth Generation
    * HSPA+
        * AKA: Evolved High Speed Packet Access
        * Extends and improves performance of 3G mobile networks
        * As high as 337 Mbit/s down 34 Mbit/s up
    * 3G
        * AKA: Third Generation
        * At least 200 kbit/s and can reach several Mbit/s
        * Introduced in 1998
    * Edge
        * Revision of the older 2G GSM based transmission methods
        * Easy to upgrade from existing 2G GSM infrastructure
* Dialup
    * Uses public switched telephone network (PSTN)
    * Computer or router uses modem to encode and decode information to and from audio frequency signals
* WiMAX
    * AKA: Worldwide Interoperability for Microwave Access
    * Enables the delivery of last mile wireless broadband access as an alternative to cable and DSL
    * Initially ~40 Mbit/s, and up to 1 Gbit/s for fixed stations
* Metro Ethernet
    * ALIAS: MAN
        * AKA: Metropolitan Area Network
    * A shared Ethernet network that partitions up a subnet into accessibility points within a geographically close region
    * Can be used by businesses to connect their own offices to each other
    * Can be used as:
        * Pure Ethernet
        * Ethernet over SDH
        * Ethernet over MPLS
        * Ethernet over DWDM
* Leased Lines
    * T-Carrier
        * Carrier System for digital transmission of multiplexed telephone calls
        * T1
            * Can transmit up to 24 telephone calls simultaneously over a single transmission line of copper wire
            * ~1.5 Mbit/s
        * T3
            * Multiples of basic T1 line
            * ~44.7 Mbit/s with 672 channels
    * E-Carrier
        * Revised and improved T-carrier technology
        * Steadily replaced by Ethernet
        * E-1
            * Operates over unshielded twisted pair or coax cable
            * ~2 Mbit/s down and ~2 Mbit/s up
            * Frame:
                * 32 time slots of 8 bits
                * time slot 0 = transmission management
                * time slot 16 = signaling
        * E-3
            * ~34 Mbit/s
            * Multiplexing of E1
    * Optical Carrier
        * Specifications for transmission bandwidth over SONET networks
        * OC-3
            * ~155 Mbit/s
            * ALIAS STS-3
            * ALIAS STM-1 (SDH)
        * OC-12
            * ~622 Mbit/s
            * Commonly used by ISPs as WAN connections
                * Not for backbone, but for regional or local connections
            * Also used by mid-sized (below Tier 2) internet customers
                * web hosting companies
                * small ISPs
* Switching Mechanisms
    * Packet Switched
        * Move data in separate small packets
        * Originally for data
        * Go through any route - allow cohabitation with all other communication
        * Lesser call and video quality
        * Used in VoIP technology
            * AKA: Voice over Internet Protocol
    * Circuit Switched
        * Require dedicated point-to-point connections
        * Originally for phone calls
        * Go through the same route - deny all other communication
        * Better call and video quality
        * Used in PBX
            * AKA: Private Branch Exchange

## 1.5 Install and property terminate various cable types

## 1.6 Differentiate between common network topologies

* Mesh
    * Each node relays data for the network
    * Connected with many redundant interconnections
    * Partial
        * Some nodes are organized in a full mesh, but others are only connected to several in the network
        * More federated than mesh
    * Full
        * All nodes are connected to all other nodes
        * Greatest amount of redundancy
* Bus
    * Nodes are directly connected to a common linear (or branched) half-duplex link
    * Very easy to connect
    * Requires less cable length
    * Works well for small networks
    * Entire network shuts down if there is a break in the main cable or T connectors
    * Large amount of packet collisions
* Ring
    * Each node connects to exactly two nodes
        * The one before and the one after
    * Unidirectional, or bidirectional (SONET)
    * Unidirectional can be disrupted entirely by the failure of a single link
* Star
    * ALIAS: Hub and Spoke
    * One central node
    * All communications go through the center node
    * Break of transmission line at node will isolate only that node
* Hybrid
    * Combination of any of star, ring, bus, mesh
    * Expensive and difficult for managing
* Point-to-Point
    * One node directly connected to another node
    * Not expansible
    * component of many networks
* Point-to-multipoint
    * ALIAS: One-to-many
    * typically describes a central node (cell tower) that facilitates multiple outbound connections
* Client-Server
    * Client communicates to server for data, route, communication
* Peer-to-Peer
    * Does not require intermediate node to facilitate data, route, or communication

## 1.7 Differentiate between network infrastructure implementations

* WAN
    * AKA: Wide Area Network
    * Accesses the resources of larger, outside connections
* MAN
    * AKA: Metropolitan Area Network
    * In-between WAN and LAN, acts as a conduit that aggregates many LANs output and input into one stream or pipe
    * Typically used to facilitate large organizations - campuses or corporations
* LAN
    * AKA: Local Area Network
    * Typically one or many local subnets operating under the control of one authority
    * Local to one geographical location
* WLAN
    * AKA: Wireless Local Area Network
    * Same as LAN, but operates on wireless by definition
    * Facilitated by wireless APs and/or routers
    * Hotspot
        * Portable WLAN that connects to a WLAN or LAN or MAN on the outside
        * Connects to individual devices on the inner network
* PAN
    * AKA: Personal Area Network
    * Defines one actor's sphere of influence without resorting to LAN
    * More confined than LAN
    * Bluetooth
        * Wireless protocol that connects various devices together
        * Uses short-wavelength UHF radio waves in the ISM band from 2.4 - 2.485 GHz
    * IR
        * AKA: Infrared
        * Line-of-sight
        * Electromagnetic radiation
        * Longer wavelengths than those of visible light
        * 700 nm to 1M nm
    * NFC
        * AKA: Near Field Communication
        * Works within 4cm (1.6in)
        * Used in contactless payment systems
        * Low-speed connection
        * Used to bootstrap more capable wireless connections
* SCADA/ICS
    * AKA: Supervisory Control and Data Acquisition
    * AKA: Industrial Control Systems
    * High-level process supervisory management
    * Programmable logic controllers
    * PID controllers
    * Used mainly to interface with machinery
    * SCADA is a form of ICS
    * Monitors and controls industrial processes
    * ICS server
        * ALIAS: OPC Server
        * Communicates via any number of protocols to control ICS machines
        * Manages ICS machines
    * DCS/Closed Network
        * AKA: Distributed Control System
        * Primary solution for process automation
        * System compromised of independently control groups of devices spread across the entire processing or manufacturing system
        * Can operate autonomously
    * RTU
        * AKA: Remote Terminal Unit
        * Electronic device used to support communication between a remote station and a control center
        * Less sophisticated than a PLC
    * PLC
        * AKA: Programmable Logic Controller
        * Specialized small computer designed to replace existing automation control devices
        * Replaced electromechanical relays and mechanically operated timers, mechanical counters and switches
        * Replaced relays
* Medianets
    * Coordination of media relay
    * VTC
        * AKA: Video Teleconferencing
        * Technology that facilitates the communication and interaction of two or more users through video and/or audio using IP and SIP
        * SIP
            * AKA: Internet Protocol
            * AKA: Session Initiation Protocol
            * Signals and controls multimedia communications session
            * Maintains connectivity of video and audio communications
            * Governs establishment, termination and other essential elements of a call
            * Can be used for creating, modifying and terminating sessions
        * WebRTC
            * Collection of communications protocols and APIs that enable real-time communication over peer-to-peer connections

## 1.8 Given a scenario, implement and configure the appropriate addressing schema

* IPv6
    * Auto-configuration
        * Plug and play setup
        * NICs can have multiple addresses
        * Allows for stateless auto-configuration
        * Uses EUI-64 for stateless
    * DHCP6
        * DHCP for IPv6
    * Link Local
        * Network address only valid for communications within the network segment or broadcast domain
        * Prefix of fe80::/64
    * Address Structure
        * 128 bits
        * Written in hex pairs
        * Separated by colons
    * Address Compression
        * Can drop any trailing zeros
        * One or more contiguous total zero hex pairs can be shortened to two colons
        * Cannot be more than one two colon shortening per address
    * Tunneling 6to4, 4to6
        * NAT between addressing protocols
        * Allows fill IPv6 connectivity to computer systems which are on the IPv4-based internet
        * Teredo, miredo
            * Software to do the translation
* IPv4
    * Address Structure
        * 32 bit address
        * Broken up into four sections of three digits 0-255
        * Separated by periods
    * Subnetting
        * Breaking up address space allocation by netmasks
    * APIPA
        * AKA: Automatic Private IP addressing
        * Windows-only
        * Address for NIC if DHCP server is not available
        * Range from 169.254.0.1 - 169.254.255.254
    * Classful A, B, C, D
        * A, B, C are for three different network sizes
        * Sizes are in descending order
        * D was for multicast
    * Classless
        * Broken up with bitmasks/netmasks
        * Uses CIDR notation
    * Private IP addresses
        * Behind an internet-facing router
        * 10.0.0.0/8, 172.16.0.0/12, 192.168.0.0/16
    * NAT/PAT
        * AKA: Network Address Translation
        * AKA: Port Address Translation
        * Taking an internal IP/Port and having the router forward everything for that specific machine to it without exposing the internal IP
    * MAC Addressing
        * AKA: Media Access Control
        * Hardware addressing
        * Changeable by software
    * Multicasting
        * Sending multiple similar packets simultaneously to separate hosts
    * Unicasting
        * Sending one packet at a time to a host
    * Broadcast
        * Sending a special packet to a special IP address in a network to locate a service
        * Typically used for bootstrapping and keepalive signals
    * Broadcast domain vs collision domains
        * Broadcast domain is the area/network in which a broadcast packet can reach
        * Collision domain is the area/network in which two packets could conflict with one another due to limitations on sending and receiving

## 1.9 Explain the basics of routing concepts and protocols

* Loopback interface
    * Where the computer can communicate to itself
    * Sockets and other quasi-networking devices can use it to run programs
* Routing loops
    * Having more than one way to get somewhere and then running back and forth between the two options
    * Having two switches connected together is a good way to form a loop
    * Mitigated with Spanning Tree
* Routing tables
    * Instructions on which group of networks is out which port
    * Forwarding instructions
* Static vs dynamic routes
    * Static routes are set up ahead of time manually by a human
    * Dynamic routes are calculated in real time by software
* Default route
    * Where a computer sends a packet if it can't find it in it's subnet
    * The gateway to the larger network
    * Typically the first router it's subnet is contained by
* Distance vector routing protocols
    * Dynamic routing protocol
    * Packet-switched
    * Informs it's neighbors only about changes in the network topology
    * RIPv2
        * AKA: Routing Internet Protocol
        * Max 15 hops in route
        * Uses UDP packets
        * Uses multicast to spread routing table
* Hybrid routing protocols
    * ALIAS: Balanced-Hybrid routing
    * Uses distance-vector for accurate metrics to determine paths
    * Uses report routing when there is a change in the topology
    * BGP
        * AKA: Border Gateway Protocol
        * Standard for internet routing
        * Scales very well
        * Can do multihoming
* Link-state routing protocols
    * Every node constructs a map of the network
    * Calculates best path to everywhere else on the network to determine routing tables
    * OSPF
        * AKA: Open Shortest Path First
        * Classified as an interior gateway protocol
        * Restricted for a single routing domain
        * Governed by link metrics of every router
    * IS-IS
        * AKA: Intermediate System to Intermediate System
        * Classified as an interior gateway protocol
        * The de facto standard for large service provider network backbones
        * Each router builds a database of the network's topology
        * Layer 2 protocol
* Interior vs exterior gateway routing protocols
    * Exterior protocols are meant to operate between autonomous systems
    * Interior protocols are meant to operate inside one autonomous system
* ASN
    * AKA: Autonomous system numbers
    * ISP officially assigned network number
    * Unique allocated to each autonomous system for use in BGP routing
* Route redistribution
    * Allows a network that uses one routing protocol to route traffic dynamically based on information learned from another routing protocol
    * Can happen when companies merge
    * Usually a temporary solution until one standard is chose and the data permanently acquired by one protocol from the other
* High Availability
    * Sub-second routing
    * Less redundancy and more streamlined communication
    * Graceful restart
    * VRRP
        * AKA Virtual Router Redundancy Protocol
        * Automatic assignment of available IP routers to participating hosts
        * Hosts see all available routers as one virtual router; that is the default gateway
        * Master Router = Physical router that is forwarding packets at any given time
        * Provides information on the state of the router, not the routes
    * VIP
        * Virtual IP
        * VIP addresses are advertised to other routers when hosts behind that router are using NAT
        * Used for connection redundancy
    * HSRP
        * AKA: Hot Standby Router Protocol
        * Fault-tolerant default gateway via failover to multiple neighbors
        * Multicast hello and priorities packets to other routers
        * Is not a routing protocol as it does not advertise IP routes or affect the routing table
* Route aggregation
    * ALIAS: Supernetwork
    * ALIAS: Prefix aggregation
    * Opposite of subnetting
    * Combines one or more networks into one entry in a routing table
* Routing metrics
    * Hop Counts
        * Traced by traceroute{,6}
        * How many routers a packet has to pass through to get to it's destination
    * MTU
        * AKA: Maximum Transmission Unit
        * Largest size that a packet can be without having to be fragmented
        * Effects, and is effected by the bandwidth
    * Costs
        * Money money money
    * Latency
        * The speed of the packet, or more precisely, the delay in reaching its destination
    * Administrative distance
        * The span of one admin's control of his network
        * How many subnets are under one's control
    * SPB
        * AKA: Shortest Path Bridging
        * ALIAS: 802.1aq
        * Intended to simplify the creation and configuration of networks, while enabling multipath routing
        * Replacement for the older spanning tree protocols - does allow layer 2 multipaths
        * Mitigates human error during configuration
        * Uses a link state routing protocol

## 1.10 Identify the basic elements of unified communication technologies

* VoIP
    * AKA: Voice over IP
    * Transmits audio over the internet with little or no latency
    * Uses UDP
    * Initiated by SIP
* Video
    * Moving pictures
* Real-time services
    * Synchronous
    * Received by users as soon as it is published
    * Presence
        * Advertising availability as online
        * Used in chat software
    * Multicast vs unicast
        * Sending packets multiple places simultaneously or one at a time

* QoS
    * AKA: Quality of Service
    * ALIAS: Packet Shaping
    * DSCP
        * AKA: Differentiated Services Code Point
        * ALIAS: DiffServ
        * Simple, scalable and course-grained
        * Classifies and manages network traffic
        * IP packet header
    * COS
        * AKA: Class of Service
        * Group similar types of data together and assign priority levels
* Devices
    * UC servers
        * Provide centralized setup and addressing of devices
    * UC devices
        * End points of the communication
    * UC gateways
        * Specialized devices to facilitate heavy loads of UC technology

# 1.11 Compare and contrast technologies that support cloud and virtualization

* Virtualization
    * Virtual switches
        * Ways to interface multiple VMs on one machine with it's internet connection
        * And with each other
    * Virtual routers
        * Ways to create networks inside of one machine
    * Virtual firewall
        * A last-line firewall able to be put right in front of the VM
        * Is itself typically a VM
    * Virtual vs physical NICs
        * Virtual NICs have their own separate MAC addresses than the physical NIC
        * Virtual NICs can use a virtual router or a TAP device
    * Software-defined networking
        * Allowing software to control networking software via API calls
        * Works well with configuration management
        * Can be agentful or agentless
* Storage Area Network
    * A network partitioned by itself to hold the data for the applications
    * iSCSI
        * AKA: Internet Small Computer Systems Interface
        * IP-based storage networking standard for linking data storage facilities
        * Facilitates data transfers over intranets
        * Can work over long distances
    * Jumbo Frame
        * Allowing larger-than-normal frames to pass in order to get more data throughput
        * Only feasible with hardware/networks under ones own control
    * Fibre Channel
        * High-speed network technology
        * 1-128 gigabits/s
        * Operates as one big switch
        * Can combine block storage devices into one
    * Network Attached Storage
        * Storage accessible over the network
        * Zyxel NAS540
* Cloud concepts
    * Public IaaS, SaaS, PaaS
        * Multi-tenant environment
        * Pay-as-you-go
        * No Contracts
        * Shared hardware
        * No control of hardware performance
        * Self-managed
    * Private IaaS, SaaS, PaaS
        * Single tenant environment
        * Hardware, data storage, network can be designed for the customer
        * Customizable
    * Hybrid IaaS, SaaS, PaaS
        * Crosses isolation and provider boundaries
        * Intermixes public servers with private data storage or vice-versa
    * Community IaaS, SaaS, PaaS
        * Shared infrastructure between several organizations
        * Have common concerns
        * Managed internally or externally
        * Hosted internally or externally

## 1.12 Given a set of requirements, implement a basic network

* SEE: Rest of blog

# 2.0 Network Operations

## 2.1 Given a scenario use appropriate monitoring tools

* Packet/network analyzer
    * When packets are successfully sent, but are not getting to their destinations
* Interface monitoring tools
    * When you may be experiencing signal loss or intermittent connectivity
* Port Scanner
    * To assess the ports available to outside the machine
    * To assess the presence and effectiveness of a firewall
* Top talkers/listeners
    * See frequency and amount of data traversing the network
* SNMP management
    * To send commands to and gather information from remote networking devices
    * Agentfull
    * Trap
        * Sending unsolicited SNMP messages
        * Done when there is a significant event by a specific device or devices on the network
    * Get
        * Retrieve data from a given host on the network
    * Walk
        * Probing a device
        * Ask what values it can supply
    * MIBS
        * AKA: Management Information Base System
        * Database and centralization of SNMP information
* Alerts
    * Email
        * Long term
        * Repetitive
        * Status updates
    * SMS
        * Emergency
        * Immediate
* Packet flow monitoring
    * Slowing network speeds
    * Too many collisions
* Syslog
    * Operating system events
    * Application logging
    * Centralizable
    * Easy to make reports from
* SIEM
    * AKA: Security Information and Event Management
    * Real-time security alerts
    * Generated by hardware, applications and networks
* Environmental Monitoring Tools
    * Temperature
        * Server room thermo reports
    * Humidity
        * Don't let it get foggy in there
* Power monitoring tools
* Wireless Survey Tools
* Wireless Analyzers

## 2.2 Given a scenario, analyze metrics and reports from monitoring and tracking performance tools

* Baseline
    * This is the network load under normal operation
    * Can be latency, bandwidth, etc
* Bottleneck
    * Make sure whatever is the limiting factor isn't slowing down
* Log Management
    * Aggregate
    * Centralize
    * Sort
* Graphing
    * Using stats to compile together a representative chart
* Utilization
    * How much is being taken up, and how much is available
    * Bandwidth
        * Make sure your lines aren't saturated
    * Storage
        * Make sure you have enough disk space left for the foreseeable future
    * Network Device CPU
        * Make sure you're not using up too much processing power on one device
        * Upgrade or duplicate
    * Network Device Memory
        * Memory is always going to be high, but make sure there's enough disposable memory for a purge
    * Wireless channel utilization
        * If necessary add another AP
        * Check if there are too many collisions
* Link status
    * Make sure they're up
    * They have the right IP
    * They have correct routes
* Interface monitoring
    * Make sure the DHCP TTL is long enough
    * Make sure there are no errors
    * Make sure they're not over utilized
    * Make sure the packets sent are the ones getting through
    * Make sure you're not at half-duplex unless you're sure you want it
    * Make sure the packets are not being corrupted

## 2.3 Given a scenario, use appropriate resources to support configuration management

* Archives/Backups
    * Son/Father/Grandfather Scheme
    * Long-term and short-term backups
    * Rotate your backups
* Baselines
    * Constantly reassess your baselines in full
    * Compare them to previous baselines
* On-boarding and off-boarding
    * Practice good user account management
    * Have a checklist
    * Do it in a timely manner
* NAC
    * AKA: Network Access Control
    * Appropriate permissions
    * No default username/passwords
    * Segment separate parts of the network
* Documentation
    * Everything
    * Wikis
    * Network Diagram
    * Blueprints
    * Asset tags
    * Inventory
    * Subnetting information
    * User manuals
    * Policies
    * Emergency Procedures
    * First-responder documentation

## 2.4 Explain the importance of implementing network segmentation

* SCADA/ICS
    * Should not have internet access
    * Control critical functions
    * Are not usually changed
* Legacy Systems
    * Usually require special hardware or software
    * Are usually inflexible to change
* Private/Public networks
    * Access to sensitive data
    * Access to those who need it
* Honeypot/honeynet
    * Fake network for attackers to exploit
    * Super monitored
    * Important training and recon
* Testing Lab
    * Making sure changes won't break anything
    * Extensive test cases required
    * Should not take up much hardware/power
* Load Balancing
    * Making sure network/servers are not overloaded
    * Redundancy in case of failure
* Performance Optimization
    * Best bang for your buck
    * Makes the users happy
    * Makes the boss happy
* Security
    * All of it
* Compliance
    * Should be inherent, I mean, it's not like the auditors know more about your network than you do
    * They certainly don't care more about the company than you.

## 2.5 Given a scenario, install and apply patches and updates

* Planned Downtime
* Change Window
* Network Package management cache
* Prioritize
* Benchmark before and after
* Test in dev
* Downgrade if experiencing problems
* Downgrade to known-good configuration

## 2.6 Given a scenario, configure a switch using proper features

* VLAN
    * AKA: Virtual LAN
    * 802.1q
    * Trunking appends a tag to the packet
    * Broadcast domain that is partitioned and isolated in a computer network at Layer 2
    * Can span switches/routers
    * Default (ALIAS: Native) VLAN may be already utilized
    * Check which ports are using which VLAN
    * Frames into the default VLAN are carried untagged
    * Default VLAN is usually for management
    * VTP
        * AKA: VLAN Trunk Protocol
        * Distributes new VLAN through entire network once configured
* Spanning Tree
    * 802.1d / Rapid Spanning Tree 802.1w
    * Prevents routing loops by blocking circular connections from being used
    * Can automatically adjust to an outage or other change in topography
    * Flooding
        * ALIAS: Unicast Flooding
        * MAC address is not yet learned by the switch
        * MAC address has been relocated by network topology change
    * Forwarding
        * Switch forwards frames based on MAC address
    * Blocking
        * Port state when spanning tree is first enabled
        * All switches in network initiate with this
    * Filtering
        * ALIAS: BPDU Filter
        * Stops the acquisition of BPDU packets
* Interface configuration
    * Trunking
        * Tagging ports to be part of a VLAN
    * Port Bonding
        * Like link aggregation that expands the ability of one port to handle twice the load
    * Port mirroring
        * Having all packets that transverse that port be also seen by another port
        * Essential for traffic monitoring of a specific port
    * Speed and duplexing
        * Port speed depends mainly on the hardware, but can be configured in software as well
        * May want to limit for some
        * Duplexing is having the ability to both send and receive packets at once
        * Most current devices have full-duplex capabilities
    * IP address assignment
        * Mainly done through a router, not a switch
        * Layer 3 switches can forward packets based on IP addresses
        * Some switches can also assign IP addresses
    * VLAN Assignment
        * Most assignment is done at the router
        * Most switches can interpret VLAN packets and allow or deny switching based on the tag
* Default Gateway
    * Where to route the packet in an IP address knowledgable switch if the MAC address is unknown on the broadcast domain
* PoE and PoE+
    * AKA: Power Over Ethernet
    * 802.3af, 802.3at
    * Can provide power to devices, such as cameras or monitors
* Switch management
    * User/passwords
        * Change the default
        * Have a management user, don't operate as root
    * AAA Configuration
        * Have the switch report back to an AAA server
        * Authentication done through the AAA server
    * Console
        * Many vendors have vendor-specific commands
        * GUI configuration is common
    * Virtual Terminals
        * Nobody likes BusyBox, but it can really get down there
    * in-band/out-of-band
        * Band is whether you're on the interior of the network or the exterior
        * Security is really important if you expose the management interface to the WAN side of the router/switch
* Managed vs unmanaged
    * Plug n' play vs pre-setup
    * Most managed switches require manual configuration
    * Only managed switches can provide QoS and other advanced features
    * Only managed switches can provide an API for SDN

## 2.7 Install and configure wireless LAN infrastructure and implement the appropriate technologies in support of wireless capable devices
