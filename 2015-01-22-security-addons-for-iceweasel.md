---
layout: post
title: "Security Addons for Iceweasel"
date: 2015-01-22
category: Battlestation
program: Firefox
process: Config
description: "Walking through the addons that I use on Iceweasel that pertain to security of my system and my online presence."
---


## What is a browser, *really*?

So browsers are INCREDIBLY insecure. I like to view them as a portal to the internet, but not as, say, ssh or wget is. No, this is a portal that has to anticipate all use-cases, and basically handle anything that any web-dev has to throw at it. And trust me, they have a shit ton of weapons in their arsenal. Not only does it need to render everything flawlessly, it has to do it securely and quickly. Now, in all practicality, no tool exists that is perfectly flexible, secure, and fast; it's always a compromise between those three factors. You, however, can help Iceweasel by making a few small sacrifices, something a mature person should have no problem doing. And in doing so, you'll secure yourself, you'll secure the web, and you'll promote responsibility world-wide. Think I'm overexaggerating? Check out how much revenue comes out of web advertising and scamming every MONTH. Even the playing field with your knowledge.


## Peace-of-Mind vs Ease-of-Use

These two may seem to always be at odds with each other, and to a point it's reminiscent of the ongoing struggle of cryptographers vs cryptoanalysts, never seeming to end in this cat and mouse game, but they can occasionally play nice with each other. It's like I said, it taks a little sacrifice. But there are ways to work around it, and the time invested upfront will lead to a mutually beneficial compromise between security and user friendliness. Keep that in mind while I outline these following ideas.

Keep in mind, before (but mainly after) installing/settings these addons/configs/whatever-this-morphs-into, *Read The Furnished Material*. There may be functionality out the wazoo that you're not aware about because you were too excited to get started searching for the latest celeb gossip. Don't be that person. RTFM.


## Extra Credit

Read through the 13 pages on [TweakGuides.com](http://tweakguides.com/Firefox_1.html)

(It is meant for Windoze users, but is applicable in most respects to Linux users also. Well, page 4 and beyond anyways.)


## NoScript

First up, the most disruptive and necessary plug-in that is required. Many will scoff when they figure out what this is, and I would advise them to dig deeper if they would, but for the rest of us, listen up. XSS (CSS, or Cross Site Scripting) is one of the most dangerous and commonly-used practices to glean data off of internet users. It's worse than cookies. Much, much worse. (And we all know how revealing those could be from back in the day) So read up on some of the links, and then, with (hopefully a brand-new, fresh install of) Iceweasel pulled up, press `Alt-D` and go to `about:addons`.

This is the main page that we will start on until we get our security nailed down. Read (get in the habit of groking what you install), and install the "NoScript Security Suite" (whatever version it may be). Iceweasel should prompt you to restart. Do so, donate (if so inclined, this IS a FOSS world, and this IS how it works), RTFM, and take a look at the FAQ about this tab popping up every time that NoScript is upgraded. It gets annoying every week or so to see this tab popping up with a new release, trust me. The FAQ will walk you through disabling that, which I recommend.

EDIT: You also have the option of setting `javascript.enabled` in `about:config`. See my Iceweasel Security Settings post for more information.

### Menu Config

The menu can be a little bit confusing at first, and a wrong click can open up a tracker permanently and never stop it again until you specify otherwise. This is all about having a sane default blocking setup. There are only a few items that I leave checked to show on the drop down menu. Go to Appearance and feel free to uncheck all but these:

- Contextual menu
- Temporarily allow [...]
- Full Addresses (http://www.noscript.net)

To access the list, either right click anywhere on the webpage (that's not a link) and navigate to NoScript, click the NoScript button on the toolbar if it's still enabled, or a combination of `Shift` and `F10` will bring up the right-click menu allowing you to navigate down to view the NoScript tracker list.

This will leave up options to temporarily allow each tracker one at a time - being able to view their entire address of where they came from. this is helpful to distinguish exactly what you're looking at. Also there is the option to temporarily allow all on the page to run, and then the alternate option to turn them all off is available after one or more are turned on. It's quite intuitive this way.

### Whitelisting

If you don't want you favorite site's friendly tracker blocked however, you do have the option to whitelist it. Check out the relevant tab in the Options dialog box that you hopefully still have up, and delete any that are either unfamiliar or straight up malicious. (google has slid theirs in the default whitelist list...here's where you can tell them where to shove it) Add your own as well (reddit uses theirs quite extensively...on their own damn site though, which is just fine by me). 

Once you're done with those tasks, go to the 'General' tab and deselect 'Temporarily allow top-level sites by default'. Play around with the other settings and see if you have a preference either way about any of them, then start browsing and seeing what works for you -- what you have to whitelist and what settings you may want to change.

## Ghostery

From their [intro](resource://firefox-at-ghostery-dot-com/ghostery/data/walkthrough.html) page:

> Ghostery looks for third-party page elements (or "trackers") on the web pages you visit. These can be things like social network widgets, advertisements, invisible pixels used for tracking and analytics, and so on. Ghostery notifies you that these things are present, and which companies operate them. You can learn more about these companies, and if you wish, choose to block the trackers they operate.

Ghostery is best described in [Gary Kovacs'](http://ted.com/talks/gary_kovacs_tracking_the_trackers) TED talk. Basically, it reminds you the footprint that your browsing is leaving behind, and can help you create the online persona that you intended to have, no that your employers (the advertising agencies) want you to create.

Ghostery also has a *Ghostrank* feature that can send "anonymous" data back to "Ghostery HQ" about the tracking elements. Where they are, who they are, yadda yadda yadda. Unfortunately, I am not about to throw my weight behind this due to it not being an open-source project. I want to be able to (have the experts) SEE what is actually happening behind the scenes to see if I'm being swindled here or not. For the time being then, I always opt-out of Ghostrank. A pity, because contributing as a user to an open-source project is one of the more noble endeavours in this life. To their credit though, they do not check the 'enable' box by default. They leave that up to you to do if you are so inclined. I am slightly assuaged by that fact, but not enough to enable to feature.

My favorite part about Ghostery is that it provides an "Alert Bubble" that pops up in the bottom right corner of your browser when you navigate to a new webpage, showing the third-party page elements that have requested that your personal data be redirected through their servers so that they can glean any information on you that they may deem relevant. Keep in mind that NoScript will block most of these, but it's a fun exercise every now and again to check the two against each other. You will quickly also see what sites continuously attempt to build a profile on you, and which ones permeate the most sites. 

If you run across a page element that Ghostery identifies yet NoScript does not block, Ghostery has it's own blocker that can be configured to prevent data being collected on your behavioral data. Tell you what, if I had three cars -- one from my insurance agency, one from Wal-Mart, and one unmarked -- following me to work and back home every day, I would be a little uneasy about it. Same thing with my browsing history. Simple as that. Ghostery identifies those cars for me, and lets me get them off my tail if need be.

Keep in mind not all trackers are run by malicious entities, but most malicious entities run trackers.


## uBlock Origin

uBlock Origin is a god-send.

Literally, it takes away the facade that the advertisement agencies make you suffer through, and makes the browsing experience all-in-all better. There's not much to say about the morals or ethics that this reflects, it just cleans up the web, plain and simple.

There's one thing that irks me. As much as you will notice it once it is installed, there is *so* much more that it can do if configured correctly.  Spending (minimal) time with the manual, you can greatly enhance your browsing experience and focus on research and productivity and all of those other buzzwords as you figure out what Bradgalina's doing these days and how big Kim Kardashian's butt really is with all of the clickbait offending your face.

If you haven't heard of this addon, you haven't been using the internet. If you haven't been using this addon, you haven't been truly using the internet. It's just as good, if not better, as it claims to be.

### Ads for Revenue

So, let's try a little experiment. Install uBlock Origin, and go to [Free Talk Live's page](https://freetalklive.com). Notice that you still see the advertisements that generate the revenue for the Free Talk Live team. This is because they do not use [ad networks](https://martechtoday.com/martech-landscape-what-is-an-ad-network-157618), but they put individually approved ads on their site. The philosophical reason for adblocking is three-fold:

1. Ad networks are historically poor at weeding out [malvertising](https://en.wikipedia.org/wiki/Malvertising) which share malware via ad scripts
2. Ads cause additional delays in page loading times due to the additional resources that are required to retrieve per page
3. "Content Producers" do not directly endorse the ads nor have direct responsibility to the products success or failure

Considering that the Free Talk Live team is know to endorse the products that are advertised on their site, they are directly responsible for their products' abilities. They are also able to vet the ads that are shown to their users, and the code that is run in their browsers. Lastly, the static nature of their ads (`jpeg`s, `gif`s, etc.), means that the load times of their pages are minimized by not having to query external ad networks for additional content to be served. All-in-all, this makes Free Talk Live advertising model sustainable, responsible, and minimal, while maintaining integrity towards their belief system.

## Better Privacy

Better Privacy blocks cookies, special kinds of cookies: *Local Shared Objects*...LSO's for short. So why is their icon a pie with the red circle and the slash through it? I have no freakin clue. But visit their home site for a rundown if what LSO's do and how they come to be and how Better Privacy works.

Operationally, every time you close Iceweasel, you will see a dialog box flit onto your screen for a fraction of a second, and then disappear. This just means that nothing was found and you're good to go. If something is, it will ask you if you want to delete it. Somewhat annoying that it doesn't do it automatically, but since LSO's are rare, there won't be many instances in which you will need to take any action.

EDIT: There is an option where that particular aspect is disabled. After disabling it, I find that I want it back, because I know that if I closed the browser but didn't see the dialog box, there was another window open somewhere, as it only runs after the last Iceweasel window is closed. I believe it is on the first dialog box that pops up after you install it.

Better Privacy operations in a security-concious manner by defaulting to treating every LSO as malicious and only leaving alone any that are on it's whitelist -- by default blank. Once again, this is all just to convince you to RTFM, as it IS very well documented, and easy to use with minimal configuration, and that's only if you feel like playing around with it. Hell, you don't even have to configure it if you don't want to, it's pretty good OOTB.

The first time I installed it, it removed 11 LSO cookies. Jeez oh man, this internet thing. We need decentralization more than ever! But that's a post for another time...

## HTTP Nowhere

Have you ever (in your previous browsing experience) seen a little lock icon in the URL bar before the website address? That means the site request and fulfillment of that request from the server to your browser was encrypted. If you've ever used Paypal or Amazon to order your books or waterbottles or whatever, you're probably put in your credit card number. I don't know about you, but I wouldn't want that floating over the internet in a file that anyone that is scanning the lines can intercept and read without any trouble whatsoever. That's what the media likes to call hacking. I like to call it poor internet browsing hygiene. The same way it's hard to put a shredded document back together, it's hard to unencrypt a file that is sent over HTTPS which utilizes the SSL protocol. As a side note - take a look into what happened and the outcry when the Heartbleed bug was discovered in OpenSSL.

[Why Should I care about HTTPS?](http://lifehacker.com/5745086/why-should-i-care-about-https-on-facebook-or-other-web-sites)

Not only does this prevent hackers spying on my personal info, but it also prevents advertising agencies from building up a profile on me without my knowledge, and the government from collecting evidence against me without a warrant. But then we're getting into the argument of why or why not encryption should be implemented universally. I strongly suggest that you educate yourself about this debate, for for the time being, HTTP Nowhere will protect you from prying eyes.

### Allowing HTTP

Many sites still don't use HTTPS, and that is a damn shame. However, I know that we all have different needs and requirements, so I would recommend that instead of disabling HTTPNowhere entirely, just uninstall it and install HTTPSEverywhere instead. This one is more lax, but does force a SSL connection if possible. At least your making security your preference and default, if not your requirement.

One point about this, it is only available through [eff.org](https://www.eff.org/https-everywhere) and will have to be downloaded from the site. One of the answers in their FAQ's mentions why. Read it. And then read the rest of them.


## Web of Trust

Web of Trust is a decentralized alternative to certificates in order to verify legitimate sites and expose illegitimate ones. 

Have you ever found a search result that seems to match EXACTLY what you were looking for, only to find that the site had only mirrored what you had put into the search engine in order to get you to click on their site? Have you ever wondered WHY they would go through the trouble to decieve you in such a way? Me neither, but it's probably not good, and I've found that Web of Trust makes these sites easily identifiable along with others that I may have suspected of malicious intent, but never been able, nor had the skill to prove so. 

Have you heard of the wonders that the buzzword 'crowdsourcing' has been able to create? Or this 'decentralization' concept that conspiracy theorists tout so often? WoT integrates both these and more into it's business model. Basically, it doesn't have one. The users are the ones who verify sites, and report back.  It is entirely driven by the end-users voluntary recommendations, and simply a aggregator thereof. Let me ask you this, if you consider a product on, say, Amazon to purchase, at what point do you look at the consumer reviews? Before or after the description of the item from the vendor themselves? And how often is your eye drawn to the five stars in order to determine just how many of them are completely filled in? These are reviews from the community, and WoT aims to be a rating system for the internet in the same fashion, taking into account many more variables than a hardback copy of *The Cathedral and the Bazaar*. 

Two pros and one con are readily discernable. First up, Web of Trust does not slow down browsing whatsoever. There's no time between firing up DDG and figuring out which sites are reputable and which are not. The currency of the new economy (could very well) be reputation. Also, it's FOSS software. So have at it. It's hosted on github. I like when things are hosted on github. It makes it simpler. Lastly, it is not comprehensive. Not all of it's users have reviewed every site on the internet. Nor will they ever. But being a responsible user, you can improve WoT, and enhance your experience while simultaneously enhancing others'.


## DuckDuckGo Plus

So this one is more likely to rattle more feathers than all of these other suggestions put together:

> I hate Google.

Now, I'll give you this, the EASIEST thing in the world to say (today) is "Just Google it!" Now, don't get me wrong, Google couldn't be as good as it is today at interpreting what "Hertz chicago chaep" means, and even spell correct me in the process without gathering feedback from its users. Unfortunately, the way that it does this in not only highly invasive, but also perverse and unlawful.  That being said, people need to find websites that they didn't know about before. Before you jump to Bing or Yahoo, I would point you in the direction of DuckDuckGo, a barebones browser that can provide reliable service without tracking privleges. The addon here switches your address bar/toolbar/searchbar into a DuckDuckGo tool, with quality answers just an query away. 

Full disclosure. It is not as good as Google. However, it is not as cluttered as Google, and the answers are still comparable to Google searches. To its credit, if you successfully trip it up completely, it will suggest that you attempt your search in another search engine. It is not proud, but is being made better every day by people who just use it. There is no need to sit down and code in order to contribute here. 

Now if you're thing about searching for videos or images web-wide, or even on a specific site, you're in luck! DuckDuckGo will deliver your query to another search engine for your searches there, and display the results. And by going through DuckDuckGo, you get the same results, without revealing any of your personal information to those particular search providers, DDG will act as a buffer for you, while still providing relevant data to the search providers.  There's a right way and a wrong (invasive) way to evolve a web crawler. 

DuckDuckGo does it the right way.

## Decentraleyes

If you're using `noscript`, you'll see that there are plenty of 'CDN' networks that are utilized for the display of websites, however the content is not gotten from the source of the website (the webserver), but rather a third party's storage. It prevents a lot of requests from reaching networks like Google Hosted Libraries, and serves local files to keep sites from breaking. This is more of a paranoid's/philosophically-driven user's plugin, but it has _zero_ impact on the loading time of sites, so I say...why not, right?

Most of the programs that scripts reach out to get from external sources are able to be served by Decentraleyes' local files. Of course, now we have to trust the local files, but inspecting local files is magnitudes of levels easier than inspecting every request that is served by a CDN.

#### This addon is being re-written for Firefox's multi-process functionality, and is expected to be compatible in-time.

## Lock the Text

This is simply a "Let's make sure we write our notes in code before we pass it in class" kind of an app. And it works very well. If you compose emails in a web browser and save them as drafts (instead of a desktop client as you should...) then there is NO reason you should not have this.

The thinking is pretty ingenious: using a shared secret (passphrase/key) you can use of of the most secure block ciphers (AES) to encrypt any sort of text, and then whoever can access the text and provide the right key can read it. Whoever can't, can't! Store text encrypted online or pass messages using a shared secret.

This software doesn't claim to have everything figured out. How do you share a key/passphrase you ask? However you want to. That's up to you. What this software does though, it does well: change text into gibberish. Try it out and see if teacher can read the note in front of the whole (NSA) now!

[Lock the Text](lockthetext.sourceforge.net)

## User Agent Switcher

There may not be a "virtual license plate" that you can be tagged by, but your user agent string certainly can be compared to a year, make, and model identification. In order to obfuscate this, you can either blank it manually (see Security Configs for Iceweasel post), or you can install User Agent Switcher plugin that does it for you...and more!

Say for instance that I want the interwebz to think that I'm browsing on a PS3.  I can copy the provided user-agent list from the addon, and switch between that, and iPhone, or even a google search bot. They won't know what I'm using. This can also be set to null or default if need be. Either way, this is the easiest way to obfuscate yourself on the web.


## Blur (Formerly DoNotTrackMe)

It's too centralized for my taste...but check it out. It may just be your cup of tea.

[Cnet](http://download.cnet.com/Blur-formerly-DoNotTrackMe-for-Firefox/3000-11745_4-75653399.html)

[Abine](http://www.abine.com)


## That about wraps it up

Don't forget there are four parts to an iceweasel setup, and this is only one of them. The other three are crucial as well in order to get a reliable, well-functioning web browser up and running. 
