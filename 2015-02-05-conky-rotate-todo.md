---
layout: post
title: "Conky rotating TODO.txt"
date: 2015-02-05
category: Battlestation
program: Conky
process: Script
description: "Make the 'TODO' section of my conky rotate categories in order to save desktop space and do something cool with linux sysutils and shell scripts."
---

## Text file

I'm gunna try this with a [text file](../../brainstorm/conkyrotatecategory.txt), because that's the best idea that I've got
at this time. The text file will be at `~/.conky/rotateconkytodo.txt`. It will 
have to contain the categories (the ones with the `+` (plus) sign before them), 
and will have to alphabetize them. That could probably work with a shell script. 

If I could make an ENV VAR to go along with that, I could use that to store the
number of the incarnation of the rotation and have that particular line read
from the list. That would make more sense than storing the variable in the text
file.

## while read line

	while read line; do
		some thing
	done


This. Is. Awesome. This is why I love linux. 

Too bad grep already reads line by line. Now THAT is why I freakin love linux.
(Well, in this case it really IS more adaquate to say GNU/Linux...)

## Using grep

	cat ~/todo.txt | grep -o "+[[:alpha:]]*[[:space:]]" >>
		~/.conky/categories.txt


This will print out all of the categories. Now to deduplicate them...

## Deduplicate

This one uses to of the "small and powerful" shell utilities - sort and uniq.

	sort categories.txt | uniq > rotatetodo.txt


`uniq` only removes duplicate files if they are adjacent to each other, so the
files have to be put through `sort` first, and then a very nice-looking `.txt`
file is produced. 

I tried using the same filename, but apparently that is illegal, because I ended
up with a blank file. So none of that.

## Removing the `+`

There is no need to, it works perfectly fine leaving that in.

## Making sure we're getting a new category

So there's a bit more to this script than some fancy-schmancy shell utils being
put to good use. I'm going to set an environment variable that is relatively
unique, and use a modulus of the number of lines in the sorted/uniq'd text file 
to rotate through the categories. 

	rotation=`date +%M`
	numofcats=$(cat ~/.conky/rotatetodo.txt | wc -l)
	let "v = rotation % numofcats"

Use the minute that the script was run in to generate the category. The easiest
way now is to run this script once a minute. The first line here could obviously
be modified to have a sample of the seconds and minutes run and converted into 
minutes in decimal form, but that's way too much for the time being, and conky
doesn't need to be refreshed that much. Hell, it could be every two, five, or
ten minutes if you want. Just adjust the values of rotation so they increase
linearily.

Don't ask me why I used a "v", it wouldn't get outta my head. That ultimately
stores the number category that is to listed. 

The cats in `numofcats` stands for categories. I'm a dog person.

On a personal note, `wc -l` is a cool function of wc (word count) that I was 
previously unaware of to count lines in a file.

## Next we gotta do something with the "v"

So in the man page for the `awk` command it states,

> NR -- current record number in the total input stream.

So wtf is a record number? I guess that lets us get the line number, because
this works:

	current=$(awk "NR==${v}+1" ~/.conky/rotatetodo.txt)

The `+1` is necessary; otherwise it starts at zero and displays ALL of the
categories, and skips the last one in the rotation.

## Output the command

	todo.sh -Pd ~/.todo/config-conky ls $current

Short, sweet, to the point.

## Run every conky refresh

Now that the script is pretty much constructed, I have to have it run every time
that conky refreshes, so that I can set the refresh rate to half a minute, a
minute, a day, whatever and have it come up with a new (the next) category each
time. So, in `.conky/conkyrc_TODO`:

	${execp ~/bin/rotateTodoFiles.sh}	

Then edit:

	update_interval 60.0

Kick back and watch it run flawlessly.

## Keeping with current trends

If you ever find a need to wipe out a category entirely, or it just so happens
that all the jobs are completed, and you don't want that category to show up in
conky, you're in luck. All you have to do is add:

	rm ~/.conky/rotatetodo.txt
	rm ~/.conky/categories.txt

to the end of the executable, and it will generate brand new category files
every time it's called on to switch the display - aka it's always up to date.

## Extra Credit

Copy that script, but put the output aligned right after the TODO: that preceeds
the todo list.
