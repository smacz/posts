---
layout: post
title: "Plans for my server"
date: 2015-04-16
category: HomeLab
program: Network
process: Architect
description: Walking through the addons that I use on Iceweasel that pertain to security of my system and my online presence
---

## What it is

It is a place to store my reference material. It is a place to have my email on.
It is a place where I can be sure that my content is mine and secure.

## What it is not

It is not a platform for others to comment on. It is not going to be hosting any
forums. It is not going to be a gathering place for people. Not yet anyways.

## Email

This is first and foremost to be an email server. I need a place to securely
work with email. I plan on utilizing a Unspyable for more sensitive endevours,
but for day-to-day security, nothing beats having an accessible server that I
physically have control over. I'll probably botch this the first time or three
that I try to set it up, but no worries, I'm in no rush.


## Blog

I love having my blog. It's both technical and non-technical. I write in the
abstract a lot, and cover a medium range of subjects, all that impact me. All
that I wish others would be aware of. Either it applies to my system or my life.
But more specifically it applies exactly at the time of posting. I also already
have a (not-quite-but-almost) kickass setup that's hosted on github for the time
being. It's looks are ready to be finalized. As soon as I figure out what the
hell to do with CSS.

It's a place where I can record what had been done, what's been done and how it 
ended up. Its purpose is tri-fold. 

### Recovery/Replicate

If the worst ever happens, and I need to set up something that had already been
an established cornerstone of my operation, I should always be able to look back
at my posts and find out how I did it in the first place. Also, I should be able
to post updates as I expand and finesse my system.

Also, if I am called upon to set up a similar system, I can always follow steps
that are in my posts. This is strictly applicable to my technical posts of
course. 

### Social Service

Others should be able to learn vicariously through my experiences, but also
duplicate my results and solve problems based on my posts. This just means that
my posts should be succinct, concise and accurate. Also they should adhere to
several guidelines that will follow in a separate post (hopefully) detailing how
to write technical posts. What a good ratio of code to explanation is. If
screenshots are a good idea, and if so in which circumstances. Also how to limit
the scope of a post that may have the potential to expand infintesimally.

### News

There are new developments everyday. And I'm not just talking about in the
Linux-sphere. Politics, Science, Social Issues, Economics, you name it, it
probably impacts me in some sort of way. Of course, if it impacts me and I care
about it, that's postable material. I try to keep myself up-to-date with the
outside world that I despise, but so desparately wish to be a significant part
of. I don't want to forget why I feel the way that I do about certain things.
This can be my quick refresher.

### Personal Progress

The blog serves a selfish purpose too. There have been some very well-documented
benefits to keeping a journal, a memoir, or a blog. It serves to show that the
past was not in content-less. Or if it was, it serves as a motivational tool to
get off my ass to do something noteworthy. Either way, I will be able to see my
personal progress. That's why, no more than...oh let's say...a month after any
given post has gone up will it be editable. It will be appendable to a link with
any updates and/or further reflections upon that subject, but it is meant not
only for others, but for me, to reassure myself that I am indeed growing and
maturing in one way or another...I hope.

## Wiki

There are several things that a blog is good for and several things that it's
not. Giving up-to-date information and opinions is one thing, but maintaining a
repository of knowledge is another. Look at the great ones. #!. The Arch Wiki.
Wikipedia. They house material that changes with the times and needs to be
updated accordingly. They make great references: indexable and searchable and
interlinkable. They are impersonal and detail, well, details more than actual
step-by-step instructions. If you want to know the inner workings and configs of
a system, view its' wiki. 

Most overviews and generalizations will be transferred out of posts and put onto
the wiki. Step-by-step instructions will be reserved for blog entries.
System-specific things will remain blog-centric, but overarching ideals
generally useful specifig commands/config options will be committed to the wiki.

It's a fine line what goes where, and I will only be able to figure out how this
approach will work with time, but since I don't plan on going anywhere and plan
on learning much, this should be a fun ride.

## Community

I prefer to be part of a community instead of hosting my own. I am no leader, at
best I'm tech support, at worst I'm a noisy n00b. I believe in the reddit
participation model. I don't believe in site-specific comment sections. I
believe in forums. I believe in open editing. Where does that leave me? I don't 
know. But I should probably try to figure something out. Let me first get a 
solid base up and running though. We'll go from there.
