---
layout: post
title: "The Nothing Simulator"
date: 2016-12-23
category: Battlestation
program: FlightGear
process: Install
repo: 
image: flightgear.png
description: "I was really excited about trying out a flight simulator, never knowing that I wouldn't even once get off of the ground. What a complete and utter waste of time spent solely to the end purpose of frustrating my entire being."
references:
  - title: The Flightgear Manual
    link: http://flightgear.sourceforge.net/getstart-en/getstart-en.html
  - title: Flightgear Forum (duplicate of this post with responses from the community)
    link: https://forum.flightgear.org/viewtopic.php?f=17&t=31302&p=301879#p301879
---
I recently decided to try out a flight sim and was really stoked that Flightgear was a thing. However, at the end of about three or four hours of trying to enjoy this program, I noticed that I was more frustrated than anything.

For background, I am running Arch on a Lenovo T430 (beefed up by some hardware mods) and the graphics, startup time, etc went as smoothly as could be expected. The in-game experience is what had me tearing my hair out.

First of all, since it didn't install to the desktop, I had to figure out how to run it - with what command. That was surprising since I know of no other program named "flightgear" and would never have thought to type "fgfs". But that was an easy complication.

When I did start the program up, I started in a large, open area that was very bluish in hue, in a plane that was turned off, and realized I had no clue how to do anything. Luckily you guys have some very thorough tutorials. However, I haven't been able to ascend into the air even once, having been frustrated in my attempts to do so.

For instance, when I selected the _very first_ tutorial (RE: Preflight Checklist) I found that I could not advance past the step of removing the wind speed cap (${terminology}) that is required directly after stepping outside of the plane at that point. No matter how many times I clicked it would not move. I must have restarted the program about five times taking different approaches (CTRL-C for wireframes, etc) but not being able to manipulate it at all. Fortunately, I found that I could bring up the checklist and press the button indicated with a greater than symbol to accomplish the task. Ok, it's a work-around anyways.

So I am now able to continue with the tutorial, and come around to the other side of the plane. At that point I needed to check the cleanliness of the fuel to see if it was light blue or clear. Now, doing this on the _right_ side of the plane went perfectly fine, however, I must have accomplished the task faster than the program anticipated, because it kept prompting me to repeat the action, as if it had not registered the action. I attempted to, however, since it was an automatic pop-up, nothing that I could click would let me do it again. Now, that last sentence _may not_ be true, but I wouldn't know. I certainly know that there was no button on the checklist ('>') to autocomplete the action. But even more frustrating was that for every button with a question mark on it there was the same non-action across the board. Not one of the - what I can only assume would be - help buttons did a single, solitary thing. So here I am, restarting the entire program because I got stuck in a simulation.

After I eventually completed that one, the next tutorial - not automatically loaded or anything - was starting up. This was pretty basic, straight forward, and only took me two attempts. Initially, I messed up the magnetos switch. This is the one that that both turns the engine on as well as selects between the right and the left "magnetos" I guess. Anyways, when required to turn it to the left, it got turned all the way off. I mean, that one's on me - sure. But now that meant that I had to do everything prior to starting the plane again. This, while the tutorial was still waiting for it to be turned to the left. Having only gone through the startup procedure for a Cessna 172p for the first time in my life about two and a half minutes ago, I decided to restart the tutorial rather than attempt to recall all of that information offhand. Luckily enough, the tutorials _are_ modular, but more on that later.

Like I said, compared to the rest of them, that tutorial was a breeze. The next (and last) one awaiting me was the Taxiing Tutorial. Keep in mind that I'm still in a big body of water, not knowing that this _wasn't_ how the program was meant to be experienced. See, I mention that because I got a whole face full of water at that point. When the tutorial started I heard the customary plane crash and - you know what? I hadn't mentioned that before. Each time I started, restarted, or finished either a tutorial of the program itself, there was a crash noise that went off. Two crash noises if it was a tutorial, and just one if it was restarting the program entirely. That was jarring the first several times when I wasn't expecting them, but I got used to the aural assault eventually.

Anyways, where was I. Oh yes, I got a face full of water. And sky. Maybe that's because I was situated facing the propeller on top of the hood of the Cessna. That was _quite_ the surprise. Also, not being able to manipulate any of the controls (or even see them for that matter, what with being on the hood of the plane and all) really threw me for a loop. Luckily I still had the instructor letting me know which buttons to press to open the throttle and steer. So, naturally, I did.

The message that I received from the instructor was to open up the throttle, scale it back once the plane started moving, and wait for the ground to change color. Only two things wrong with that. How was I supposed to know if I was moving if all I saw were two shades of blue split at the horizon and how was I supposed to move. Needless to say, I spent too long (by any reckoning) with the throttle just wide open and chillin'. Finally I decided to restart the game after cycling through all of the views of this beautiful plane stuck in a body of water only to return back to the hood instead of the cockpit every time.

After restarting it, I got back to that same simulation, and the engine didn't autostart for me. I honestly don't have any kind of snarky remark for that because it did that twice and then never again. Huh. _However_. The plane would not move. Come hell or high water, that plane would not move. Brakes, tie-downs, you name it, I checked it. That plane (still in the middle of the god forsaken ocean by the way) would just not move. And every time I restarted that tutorial, I heard the sound of plane crash twice in a row, and then was faced by a non-moving plane. I tried putting it in another location - which was supposed to be the included KSFO airport. I tried setting all of the necessary environment variables. I tore through the logfiles, all of which amounted to squat. There was no way in hell that the plane I was sitting in would move no matter the cajoling or pleading that I did with it.

It was at that point that I gave up. I figured, why torture myself over this. If this program doesn't want to be used, why should I continue to attempt to use it? The killer was...the absolute bitch of it was that [according to the manual ch. 8.3]({{ page.references[1].link }}): "Once FlightGear is started you will see the [following window](http://flightgear.sourceforge.net/getstart-en/getstart-en13x.png) and hear the sound of an engine." Mine started in a big body of water with the sound of a plane crash. Ominous, no?
