---
layout: post
title: "Making use of my extra monitor"
date: 2016-09-23
category: HomeLab
program: KVM
process: Config
description: "Since I use my laptop screen and only one of my dual monitors for my daily driver, I have the second one hooked up to Virtualizor, my KVM host. I would _love_ to have it output stats about the machines that are running - either graphically or just a terminal dislplay."
---

## Xorg on host

So I'll have to put `X` on the host. I _should_ try out wayland, but I'll save that for another day.

### Install

```
# yum install virt-manager xorg-x11-server-Xorg xorg-x11-server-common xorg-x11-server-utils xorg-x11-server-xinit xorg-x11-drivers xterm
```

### Ratpoison

https://copr-be.cloud.fedoraproject.org/results/shassard/ratpoison/epel-7-x86_64/ratpoison-1.4.8-1.el7.centos/


.xinitrc

```
xset dpms 0 0 0 &
xset s off &
ratpoison &
virt-manager --no-fork
```

`virt-manager` will fork into the background without the --no-fork option set. This would then kill `X` as it has no process in the foreground of the `.xinitrc` to keep it running.

### Gsettings

I hope you're running tmux, 'cause to run these, you'll have to have X running and virt-manager running. (I'm not sure about that last part, but it works like that.) Also, that weird bit about `DISPLAY=:0` _needs_ to be inside of the loop and executed before every `gsettings` command. It's freakin' stupid, but over ssh, necessary. In fact, for pretty much every command that deals with the output, you'll have to precede it with `DISPLAY=:0`.

```
# for i in host-cpu-usage cpu-usage memory-usage network-traffic disk-usage; do \
    DISPLAY=:0 gsettings set org.virt-manager.virt-manager.vmlist-fields $i true; done

# for i in enable-memory-poll enable-net-poll enable-cpu-poll enable-disk-poll; do \
    DISPLAY=:0 gsettings set org.virt-manager.virt-manager.stats $i true; done

# DISPLAY=:0 gsettings set org.virt-manager.virt-manager.details show-toolbar false

```

### GTK

```
# mkdir /etc/gtk-3.0
```

/etc/gtk-3.0

```
[Settings]

gtk-application-prefer-dark-theme = true
```

## End result

Now I can go boot up my server, and when I can ssh into it at all, I can run `startx` and the `CTRL-C` and still have it running. Awesome. Persistant resource monitor.


#### TODO:

Start on boot
    - Systemd magic

Rotate screen
    - Compiling retired kernel module
