---
layout: post
title: "Setting up FreeNAS on an HP Proliant Microserver G7"
date: 2017-07-27
category: HomeLab
program: FreeNAS
process: Install
repo:
image: freenas.jpg
description: "After bricking my first NAS box, I decided to get one that was a bit more hardware-friendly, as I have never had much success with firmware hackery. The Microserver seemed like the perfex box for the job."
references:
  - title: FreeNAS 11 Documentation
    link: http://doc.freenas.org/11/
---

# Why did I spend my money on another box?

Well, for starters, I am still a little bit peeved that my last experiment with building a NAS box went awry. However, the best case scenario for that box would have been Debian, but with the BSD-based FreeNAS OS, I get an array of storage-specific features that have been fine-tuned for home and enterprise environments alike.

I was able to snag it for less than $250 on eBay. It's a nice box, sleek and utilitarian. There are many USB ports on the front, and a couple on the back - most of them USB 3.0. The previous owner had put in a 4x1GB Ethernet PCI board so I have a total of five ethernet ports on the thing. Presumably way more than I'll ever need, especially considering the virtualization that it will mainly be used to power.

# Hardware Setup

The purchase had come with a 250GB HDD - nothing special, just a run-of-the-mill Seagate drive. However, seeing as I had invested in four 2TB WD Reds, I started putting those in.

I quickly noticed that the drives that I had did not come with any screws to mount them in the drive casings. The previous box had snap-in casings, but the HP had ones that required screws. So I salvaged the ones that I could from my other drives laying around. These were laptop drives, so the screws were smaller, but they worked well enough for the time being.

At first boot, only three of the drives were recognized by the OS. I pulled them, and found one where the screw had come loose, letting the hard drive hang at an angle that would not let the contact to the SATA port be made. I reinstalled the drive in the casing, slid it back into the box, and voila! It worked perfectly.

Thinking ahead a bit, since FreeNAS prefers to be run off of flash storage, was reading up on tips and tricks, and found that the HP has an internal USB port that can be used to house the OS. I zero'd out a 16GB USB stick and placed it in there to be installed to.

# OS Setup

## Installation

So the reason that I zero'd out the internal USB drive is that the OS cannot be installed to the same USB drive that the installation is initiated from. So I downloaded the iso and `dd`'d it to another USB stick. I booted up the system, and it instantly recognized it as the only disk with an OS on it and started the installation.

As I am writing this afterwards, I don't exactly recall which steps were required for setup, however it was all keyboard-driven and there were a minimal of setup options to go through. Networking, root passwd, hostname, and installation media were the only ones that stick out to me now. The last being the internal USB stick.

After the installation, the boot-up screen went through its own setup process, and displayed the IP address where I could access the GUI. I didn't need the externally mounted USB stick with the installation iso anymore, so that got repurposed for some other, later use.

## Post-installation

Accessing the GUI and logging in with the previously set root password prompted the setup wizard, which set the storage configuration. After much research, I found out that I would want RAIDZ2 (ZFS RAID6) instead of RAIDZ1 (ZFS RAID5) with four disks. This is to allow two disks to fail and sill have the ability to recover data. This in itself wasn't such a big draw, except for the fact that if one drive fails, the replication process upon replacing the failed drive is very intense on the rest of the disks, and the risk of a second disk failure during the replication process is higher than normal.

After the setup there, I configured a NFS share initially, which I would ultimately remove once I figured out the volume (ZFS 'pool') scheme that I was going to pursue. However, there was much more setup that would need to go into the share itself before it was usable as I had envisioned.

# Share configuration

## FreeNAS

There were a couple things that I needed to do to get a valid setup to mount a simple NFS share on my laptop. First, I had to create a user that mirrored my own on the FreeNAS box. I set the UID and the GID to the same as the user on my laptop, and gave that user sudo permissions.

Then I created a new NFS share, according to another naming scheme. There are several reserved words that cannot be used to name a volume (like `data`), but `freen54-hub` - to specify the box and the network - seemed sufficient for my purposes. At that point, I changed the ownership of the share to my new user, as `Mapall`.

> The Maproot and Mapall options are exclusive, meaning only one can be used–the GUI does not allow both. The Mapall options supersede the Maproot options. To restrict only the root user’s permissions, set the Maproot option. To restrict permissions of all users, set the Mapall options.
>
> --[FreeNAS 11 Documentation - Sharing](http://doc.freenas.org/11/sharing.html)

I also allowed sharing of all subdirectories, in the event that I wanted to split up the share's location on my filesystem.

## localhost

In order to mount the share as a user, I had to specify the mountpoint in my `/etc/fstab`, and pass the `user` option to allow users to mount it. I also had to specify the mount point where the share resided on the FreeNAS server. And then the type had to be specified in order to have mount recognize the share type of NFS.

```
freen54.hub:/mnt/volume-default/freen54-hub /media/freen54-hub nfs rw,noauto,user 0 0
```

At that point, I was able to mount it as my own user without root privileges. For the time being that's all I needed, and I subsequently proceeded to copy the entirety of the contents of my external HDD onto the share using rsync with what appeared to be local pathnames. This is brilliant, and I can't wait to do more with this in the future.
