---
layout: post
title: "Secured, encrypted, bootable USB"
date: 2016-09-25
category: Battlestation
program: RESPIN
process: Install
image: usb.jpg
description: "Since becoming my daily driver, I absolutely have fallen in love with using live USBs for everywhere outside of my own house. It's also a bit more relaxed and functional than my typical keyboard-driven, ultra-secure, FOSS-only desktop. Which is good, because I have to actually _do_ things for college. Ugh."
resources:
  - title: A better way to create a customized USB drive with Ubuntu Live on it
    link: https://rudd-o.com/linux-and-free-software/a-better-way-to-create-a-customized-ubuntu-live-usb-drive
  - title: Make your own iso, with Remastersys
    link: http://crunchbang.org/forums/viewtopic.php?id=36322
  - title: Install Resiable Encrypted LMDE
    link: https://bitbucket.org/tlroche/install_resizable_encrypted_lmde
  - title: tmuxinator
    link: https://github.com/tmuxinator/tmuxinator
  - title: Open a command in a new instance of various terminal emulators
    link: https://superuser.com/questions/917544/open-a-command-in-a-new-instance-of-various-terminal-emulators
  - title: Create a USB stick with persistence
    link: https://forums.bunsenlabs.org/viewtopic.php?id=89
  - title: Debian jessie persistent live USB
    link: http://crunchbang.org/forums/viewtopic.php?pid=438311
  - title: SuperGrubDisk Wiki - Loopback.cfg
    link: http://www.supergrubdisk.org/wiki/Loopback.cfg
  - title: Boot from non-ubuntu live ISO images
    link: https://askubuntu.com/questions/141940/how-to-boot-from-non-ubuntu-live-iso-images-like-fedora-or-centos
  - title: ArchWiki - Multiboot USB Drive - Debian
    link: https://wiki.archlinux.org/index.php/Multiboot_USB_drive#Debian
  - title: Run ISO files directly from the HDD with GRUB2
    link: https://www.maketecheasier.com/run-iso-files-hdd-grub2/
---
# Setting up an encrypted, secured USB flash drive

This will go through the steps of how to set up the drive for it to be secured with a passphrase. An encrypted persistant storage device if you will.


## Unpartition & Wipe Disk

### cfdisk

Now, I just like to reset the partition table _before_ I wipe the drive. Why? No real technical reason. It won't save space, money, or time, but it gives me time to visually review what I'm destroying. 

If you need a step-by-step for deleting all partitions using cfdisk, you can just stop here. No reason for you to go any further.

### Dd

From [Dd - Destroyer of Disks](http://www.noah.org/wiki/Dd_-_Destroyer_of_Disks):

> If you want to not worry about remembering the exact amount of the disk to erase and you do not care about erasing other data on the drive, then simply blast away the first megabyte or so. It doesn't matter.

> `dd if=/dev/zero of=/dev/sdXXX bs=[4]M count=10`

Aaaaand boom goes the dynamite. I mean, it's not `shred` or DBAN, but I feel better about it now.


## Partition Disk for Encrypted Volume

### /boot

> It is important to note from now that in _almost_ every case there has to be a separate partition for `/boot` that must remain unencrypted, because the bootloader needs to access the `/boot` directory where it will load the `initramfs`/encryption modules needed to load the rest of the system.
-- [dm-crypt - ArchWiki](https://wiki.archlinux.org/index.php/Dm-crypt/Drive_preparation#Partitioning)

So set that up around 750MB. -> `/dev/sdb1`

### LVM -> `/dev/sda2`

#### /

Root isn't going to be there. Instead, it'll point to the iso that sits there. Should be labeled as `/dev/sda5` as the first one in the LVM.

Round up however you'd like, and put the size of your `.iso` as the size of the partition. Like I said, round up a bit though - relative to your device space.

#### Storage

The rest of the storage will be sitting here. `/dev/sdb6`


## Filesystems and Installation

### Formatting Filesystems and Adding Labels

```
# mkfs.ext3 -L "BunsenLabs" /dev/sda5
# mkfs.ext3 -L "/boot" /dev/sda1
```

As with anything we're dealing with tonight. Careful.

> If I were given 6 hours to chop down a tree, I'd spend the first four sharpening the axe.
-- Abraham Lincoln

### Install Grub2 on Target

>1. Mount the target partition. You can probably just reconnect the target to the host; if that does not produce "the usual" automounting behavior, >try, e.g., udisks --mount /dev/sdb1
>
>2. Make directory= /boot with, e.g., sudo mkdir -p /media/me/LiveUSBmultiISO/boot
>
>3. Install GRUB2 with sudo grub2-install --boot-directory=/media/me/LiveUSBmultiISO/boot /dev/sdb . You should get the result
>
> > Installation finished. No error reported.
>
>4. Own the target with, e.g., sudo chown -R me:me /media/me/LiveUSBmultiISO

The grub2-install command will be the hardest to tweak.

Follow steps on [linked site](https://bitbucket.org/tlroche/install_resizable_encrypted_lmde/src/HEAD/create_LMDE_liveUSB.rst?fileviewer=file-view-default) until you get to the grub.cfg part.

    * Make & Populate target directory
    * 

#### grub.cfg

There's a file - `/etc/grub.d/40_custom` - that will be verbosely copied into `grub.cfg` when running `grub-mkconfig`. This is where the `menuentry` for the ISO is going to go.

In the entry below, the `isofile` variable points to the file _on the filesystem_, not necessarily where it is compared to the root of the OS. In fact, there may be a way to install this without installing an OS on the stick. But following this down, we can see that the `loopback loop` is pointing to `(hd0,msdos1)`, which is the first partition on the first hard drive, aka `/dev/sda1`. Therefore, even though this is an entry for my arch install, and the file's absolute path is actually `/boot/ISOs/RESPIN.iso`, the `isofile` strips off `/boot` because `/dev/sda1` is mounted on `/boot`.

Anyways, after that, get the UUID of the partition that's being used with `blkid`. Then, make sure that there's no differences between the kernel and the initramfs locations inside of the iso. If necessary, you can mount the iso on a filesystem to inspect it.

```
menuentry 'RESPIN' --class os --class gnu-linux --class gnu --class os --group group_main {
    set isofile='/ISOs/RESPIN.iso'
    insmod ext2
    insmod loopback
    insmod iso9660
    loopback loop (hd0,msdos1)$isofile
    search --no-floppy --fs-uuid --set=root 1b7278cf-5f84-4e4c-812b-3d13230805db
    linux (loop)/live/vmlinuz boot=live fromiso=/dev/sda1/$isofile noconfig=sudo username=ghost hostname=live
    initrd (loop)/live/initrd.img
}
```

# Encrypt Disk

### cryptsetup

* "apt-get %s"
* "dnf/yum %s"
* "pacman %s"

## https://www.cyberciti.biz/hardware/howto-linux-hard-disk-encryption-with-luks-cryptsetup-command/

## http://xmodulo.com/how-to-create-encrypted-disk-partition-on-linux.html
