---
layout: post
title: "Backup to external HDD"
date: 2015-02-22
category: HomeLab
program: Backup
process: Script
description: Using my new 1TB HDD (nicknamed Bespin) to backup my worthless info...the hard way.
---

Time to backup to the external HDD. It was obtained about a week ago. I picked
up a 1TB seagate cheapie external HDD from my favorite computer shop. Let's get
it up and running.

## Partition it

First let's mount and format it. It used to be used for windoze. I was more than
happy to liberate it.

	sudo fdisk -l

Which vomits when you have an encrypted LVM apparently. Jeez. But it worked. The
hard drive is mounted on `/dev/sdb1`. It's also NTFS partitioned. Let's `ext4`
this bitch the cli way. Well...curses anyways.

	cfdisk /dev/sdb

It doesn't carry over my terminal semi-transparency. Harumph. It'll have to work
for now. 

If you don't know how cfdisk works...play around with it a little. It's super
intuitive. Better than gparted even. The only thing I couldn't figure out was
how to set the label of a given partition. I want to be able to label them Son,
Father, Grandfather, and UCC. It apparently is impossible to use cfdisk to do
this because linux. Anyways, I'll figure this all out later.

The partition scheme I decided on is as follows:

Primary | 75 GB | Son | FS Type: 83 |
Primary | 75 GB | Father | FS Type: 83 |
Primary | 75 GB | Grandfather | FS Type: 83 |
Extended |
Logical | 20 GB | OS | FS Type: 83 | *Boot Flag*
Logical | 5 GB | swap | FS Type: 82 |  
Logical | 750 GB | UCC | FS Type: 83 |

Write the changes to disk. [tldp.org](http://tldp.org/HOWTO/Partition/requirements.html) says that I don't necessarily have to
have my bootable partition as a primary partition, but it is recommended for
recovery. Since I will always have a computer here to recover it in order to
boot it in the first place, I'm not worried about it.

## Format it

Now before we mount it, unless we want the shell complaining about us needing to
specify a filesystem type in order to mount it, let's write the filesystem type
to the partition itself. I mean...format the filesystem, of course.

	mkfs -t ext4 /dev/sdb{1..7} (except 4)

This is done individually because I don't think the time invested in finding out
if this can be automated or not would pay off. At scale, perhaps. `/dev/sdb4`
does not need to be formatted by the way, because that is only the extended
partition which doesn't need to get formatted. 

## Mount it

Now we should be able to mount.

	sudo mkdir /mnt/Father
	sudo mount /dev/sdb2 /mnt/Father
	cd /mnt/Father

Now you can play around. I plan on creating base directories for all of my
distros, so that when I save them, I can save the information in their own
specific filesystem. Also, then I'll have the UCC that can be shared across any
computer instead of being tied to one single machine. And I'll have it backed up
to a safe location. Totally a necessity because that has all of my references,
my hopes and dreams, and all of my music. Critical shit right there.

## Backup to it

Lastly, just copy all of the files into to the correct folder. Aka:

	cp -ar ~/Backup/1-Son/* /mnt/Son/CrunchBang/

That's the command for the most recent backup of my...dying :,(... #! system
anyways.

Follow the correct aging steps for Son/Father/Grandfather/(Fiery Death) 

## UCC Ideas

Remember that the UCC is rsync'd, and it's easier to continuously update it
rather than to resave it every time. That's some SERIOUSLY wasted electricity.
There may be a way to save all of the data on the HDD, at least like all the
music, big, and irrelevant files, while maintaining a local text file copy of
the files maintained at last backup. In that case, UCC would only be a
incremental backup every time.

UCC would not be deleted from the HDD or local disk automatically, ever. The log
(text file thing) should be updated if need be though. 

Also, there could be a breakdown of the files if a specific "set" should be
installed onto a given system, such as:

* Music
* Music - Dubstep only
* Music - Jazz only
* Coding - Python only
* Coding - Bash only
* Sysadmin
* PaleoMan
* Decentralization - Entire folder and subfolders content
* Decentralization - Bitcoin subfolder content only

## Backup Automation Ideas

As far as automation goes, I will presumably still save the files locally then
transfer them to the HDD manually for the time being. What's simmering on the
back burner is this though:

* Automatically mount the HDD partitions to the correct mount points
* Select the correct distro to store the data in (what kind of identifier to use?)
* Transfer system backup files to HDD
* Error-check previous step
* Wipe system backup files from local drive
* Sync UCC
