---
layout: post
title: "LibreElec in an HTPC"
date: 2018-02-01
category: SysAdmin
program: LibreElec
process: Install
description: "I wanted to give my parents a better TV experience, seeing as they spend enough time there. So I figured that I'd create a LibreElec box for them. I did, and put in a couple extra things too."
references:
  - title: Libvirt and LibreElec without a GPU to pass-through
    link: https://lime-technology.com/forums/topic/54763-libreelecopenelec-krypton-templates/?page=2&tab=comments#comment-549997
  - title: Chromium 59 for Netflix
    link: https://forum.libreelec.tv/thread/109-chromium/?postID=56543#post56543
---

#### Note: You can test out LibreElec modifications in a libvirt VM using [{{ references[0].title }}]({{ references[0].link }}).

# Installing Chromium for Netflix

```bash
cd ~ && wget https://ubrukelig.net/files/browser.chrome.59.0.3071.115-1.zip
```

`Addons` -> *open box in upper left corner*, -> Install from zip file -> Allow installation from unknown sources -> Home -> Chrome zip file

Set homepage to netflix.com

See if pointer still works.
