dns: '*.hobbithole.blue'
---
Create 3 servers and get them into DNS

[Install Proxmox](https://pve.proxmox.com/wiki/Install_Proxmox_VE_on_Debian_Stretch)

```bash
echo "deb http://download.proxmox.com/debian/pve stretch pve-no-subscription" > /etc/apt/sources.list.d/pve-install-repo.list

apt update && apt dist-upgrade

apt install proxmox-ve postfix open-iscsi ntp ssh ksm-control-daemon openvswitch-switch

shutdown -r now
```

Navigate to `https://_<your ip>_:8006/`

```bash
cd /var/lib/vz/template/iso

wget https://nyifiles.pfsense.org/mirror/downloads/pfSense-CE-2.4.4-RELEASE-p1-amd64.iso.gz

wget https://dl.fedoraproject.org/pub/alt/unofficial/releases/29/x86_64/Fedora-Xfce-Live-x86_64-29-20181029.1.iso
```
