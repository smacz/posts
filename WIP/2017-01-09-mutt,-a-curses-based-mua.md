---
layout: post
title: "Mutt, a curses-based MUA"
date: 2017-01-09
category: Battlestation
program: Mutt
process: Config
repo: 
description: "Mutt is the easiest mua to operate inside of a terminal, once you can get used to the keybindings that is. However, its power comes in its ability to work many a script, plugin, and setting to death. From the man page: 'All mail clients suck. This one just sucks less'"
references:
  - title: Gmail in linux terminal (with Mutt)
    link: https://nicolabenedetti.wordpress.com/tag/mutt/
  - title: The Homely Mutt
    link: http://stevelosh.com/blog/2012/10/the-homely-mutt/
  - title: The Woodnotes Guide to the Mutt Email Client
    link: http://therandymon.com/woodnotes/mutt/using-mutt.html
  - title: Using Multiple IMAP accounts with Mutt
    link: http://www.df7cb.de/blog/2010/Using_multiple_IMAP_accounts_with_Mutt.html
  - title: The Mutt E-Mail Client
    link: http://www.mutt.org/doc/manual
  - title: "Notmuch: What email should be"
    link: https://www.youtube.com/watch?v=pBs_P_1--Os
  - title: Using Notmuch with bare Mutt, the old fashioned way
    link: https://notmuchmail.org/notmuch-mutt/
  - title: Take Control of your email with mutt, OfflineIMAP and Notmuch
    link: https://hobo.house/2015/09/09/take-control-of-your-email-with-mutt-offlineimap-notmuch/
  - title: Switching from mutt to neomutt
    link: https://hobo.house/2016/08/08/switching-from-mutt-to-neomutt/
  - title: "ArchWiki: Mutt"
    link: https://hobo.house/2016/08/08/switching-from-mutt-to-neomutt/
  - title: The Mutt FAQ
    link: https://dev.mutt.org/trac/wiki/MuttFaq
---

## Sidebar

Per the ArchWiki:

> A sidebar is included by default in Arch. You can choose to display the sidebar on startup, or to prompt it manually with a key:

```
set sidebar_visible = yes
bind index,pager B sidebar-toggle-visible
```
