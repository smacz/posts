---
layout: post
title: "Desktop Network Virtualization"
date: 2018-05-03
category: SysAdmin
program: KVM
process: Install
description: "Following on the heels of my full network setup, I decided to virtualize my desktop setup."
references:
  - title: "Red Team Laptop and Infrastructure"
    link: 'https://hackingand.coffee/2018/02/assessment-laptop-architecture/'
  - title: "Fedora Magazine: Getting Started with the Openbox Windows Manager in Fedora"
    link: 'https://fedoramagazine.org/openbox-fedora/'
---

Inspired by the "Hacking and Coffee" blog post [{{ references[0].title }}]({{ references.link }}) I decided to virtualize my laptop into an entire network.

After too much research, I decided that I'm going to stay vanilla KVM with a minimalist DE and put the heavy stuff in the VMs. Basically it was between KVM and Xen. I am very _very_ happy with the KVM tooling and performance, so I decided to stick with that. And while Proxmox will be good for cluster virtualization, I didn't want to rely on a web browser to access the spice viewer.

So I got fairly comfortable with Openbox during my time with #! a.k.a BunsenLabs, and figure going with a pure WM is going to be my lightest option for a host environment. Pair it with tint2 by following the rough sketch outlined in 


