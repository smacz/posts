---
layout: post
title: "Snap Installs"
date: 2018-12-26
category: Sysadmin
program: Infrastructure
process: Install
repo:
image:
description: "I am compelled to write this up since it has been such a joy to work with snaps on servers! I am looking forward to trying out a couple more features in upcoming releases, but I think they've got the basics down."
references:
  - title: 'How To Install and Configure Nextcloud on Ubuntu 18.04'
    link: 'https://www.digitalocean.com/community/tutorials/how-to-install-and-configure-nextcloud-on-ubuntu-18-04'
---

# Workspace

## Tool installation

## Project Structure

# Manual Workflow

```bash
$ sudo snap install nextcloud
```
## POC success

Yeah, it's that easy.

# Round 2

## MySQL Setup

Once the MySQL One-click droplet is spun up, you can set up the database by accessing it at `<ip_address>:phpmyadmin` with the following:

```SQL
CREATE USER 'nextcloud'@'mysql.hobbithole.blue' IDENTIFIED BY '<password>';
CREATE DATABASE IF NOT EXISTS nextcloud;
GRANT SELECT, INSERT, UPDATE, DELETE, CREATE, DROP, INDEX, ALTER, CREATE TEMPORARY TABLES ON nextcloud.* TO 'nextcloud'@'mysql.hobbithole.blue' IDENTIFIED BY '<password>';
FLUSH privileges;
```

Then get an SSL cert for the instance

```bash
# certbot run
```

## Snap Install

So now again we install the snap, and configure it with a couple other things:

```bash
# nextcloud.occ config:list
# nextcloud.occ config:system:set dbname --value=nextcloud
# nextcloud.occ config:system:set dbhost --value=mysql.hobbithole.blue
# nextcloud.occ config:system:set dbuser --value=nextcloud
# nextcloud.occ config:system:set dbpassword --value='<password>'
```

# Round 3

