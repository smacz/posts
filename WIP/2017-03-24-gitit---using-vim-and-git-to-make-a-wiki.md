---
layout: post
title: "Gitit - Using Vim and Git to make a Wiki"
date: 2017-03-24
category: HomeLab
program: Gitit
process: Install
repo: 
description: "I like my blog, but I'd rather prefer a wiki to keep track of tips, tricks, and other things that I need to document about my network. Gitit has a list of features (not even necessarily addons) that make it very attractive. Let's see if we can get it up and running."
references:
  - title: How to install Gitit on CentOS 7
    link: https://www.vultr.com/docs/how-to-install-gitit-on-centos-7
  - title: Gitit as my personal wiki
    link: https://nathantypanski.com/blog/2014-07-09-personal-wiki.html
  - title: Gitit User's Guide
    link: http://wiki.csie.ncku.edu.tw/Gitit%20User's%20Guide
---
