---
layout: post
title: "FreeNAS IMAP Jail"
date: 2020-04-18
category: HomeLab
program: FreeNAS
process: Install
repo:
image: freenas_jail.png
description: "Creating a FreeBSD Jail in FreeNAS for an IMAP service"
---
# Naming

Before we start off doing anything, we'll have to figure out a naming convention. My name that I'll probably be using for this is IMAPArchive. Simple and explicative.

# Networking

Considering that we are going to be putting this on an isolated network, this is necessary. Since the HomeLab right now has several other services running, we're going to put this on the `172.16.6.0/24` network. This is because it is for a different use-case than OCBackups, and it is meant to be publicly accessible, unlike my Media share.

I created a new VLAN interface in pfSense, assigned it to an interface, and configured its IPv4 stuff. I was able to find my masking tape and made sure to appropriately label the ethernet as I hooked it up from the switch to the server. Then I set up the VLAN on the switch, to tag all packages coming in.

# Creating the Jail

I found out at this point that the new stable was updated. I decided that this would be a good time to perform an upgrade, as it's just about time for me to go for a walk, so I'm going to do that and see what it's like when I get back. It looks like contrary to what I though, the `FreeNAS-11-STABLE` train will only include `11.1.X` releases, where I would have to upgrade to newer minor versions to get the new stuff. Seeing as the `11.3` series just hit stable at the beginning of this year, this would be a perfect time to migrate to that. Let's see how we do...

So far, it's been painless. It seems to work just fine. I'm sure I'll have fun testing it out extensively, but for now, let's get that Jail up and running. And the interface is much _much_ prettier.

## Create the network bridge

So here is the up-to-date documentation: [15. Jails](https://www.ixsystems.com/documentation/freenas/11.3-U2/jails.html). A quick summary would be to go back and remove the IP address from the interface. Then select `VNET` and `DHCP` from the list. Once we're done, go back into the network configuration and select our interface's name for the `vnet_default_interface` setting. That ensures that we will have our jail use the host interface that's hooked up to the port on the switch to automatically tag our packets with the correct VLAN. Since we're pairing the jail with a physical interface, we have to go to that interface and Disable Hardware Offloading. That prevents the interface reset when the jail restarts. Don't forget to select **[X] Auto-start**.

To explicitly spell it out, when setting up the jail quickly, it will look like it chooses an arbitrary interface, and the IPv4 Default Router selection is greyed out. However, after setting up the container, you can choose the correct interface, and then it will switch the IPv4 Default Router selection to "auto", and as long as there is a DHCP server on the network, you'll be getting an IP address when it starts up.

#### I spent more hours than I'll admit fumbling around because I forgot to RTFM and set the PVID on the port on the switch.

Looking at my storage now, I could see that I had two new datasets, `iocage`, and `jails`. Between the two, they were less than 1GB. We'll see how that grows when I put new stuff in it.

# Setting up IMAP

## Installation

FreeNAS exposes a shell to the jail in the UI, so there's no need to enable SSH. I'm going to try out [courier-imap](https://svnweb.freebsd.org/ports/head/mail/courier-imap/), since I don't have previous experience with it, it's lightweight, it uses Maildirs, and looks to be up-to-date.

So when I first went to install `courier-imap`, the `pkg` command had to be set up, as it was not there initially.

```
# pkg install courier-imap
```

## Configuration

Now I have to configure it.

```
# pkg install vim
```

And then get to work.

The configuration file is located at `/usr/local/etc/courier-imap/imapd`. One reason why BSD is so fun to use is that all of the files that admins install separately are going to be located in `/usr/local/etc`, 99% of the time. That's just where they go if they've been installed. They don't share space with the system configuration files in `/etc`, they have their own space.

# Setting up `getmail`

Now that we have the IMAP server set up, we're going to have to put something into it! This we can do by using `getmail` to retrieve the messages that we have on our various IMAP servers.

## Installation

Recently, [getmail6](https://github.com/getmail6/getmail6) was created by forking getmail, to allow for running with `python3`. Since we will be compiling this from source, we are going to need `git` and `python3`:

```
# pkg install git python3
```

Then we clone down the repo and build it from source:

```
# git clone https://github.com/getmail6/getmail6.git
# cd getmail6
# python3 setup.py build
# python3 setup.py install
```

## Quick Config

