---
layout: post
title: "Centralized Syslog Server"
date: 2016-07-23
category: HomeLab
program: rsyslog
process: Install
description: "The new systemd journal isn't bad, but a classic rsyslog central server is the easiest way to keep tabs on running services."
---

### Instead of going to each server and pulling up the logs manually, a central syslog server can ease the burden of keeping up with all active services and display notifications and, well, logs as a general overview that's easy for one admin to monitor. Much more feasible than keeping 15 different tmux sessions up.

## Base server

Since this is another VM in my VMLab, I installed the `guest-agents` package group on top of the base install (`@base` and `@core`) so that I could get info from it. I also grabbed the `anaconda-ks.cfg` that was generated and put into `/root/` so that I could use that as a future template for bringing up VMs. After all, it's just a kickstart template that's derived from the choices made during the install. I'm sure I could trick it out in the future (and certainly intend to) but for the time being, at least I can to a (relatively) unattended install of a base system.

#### NOTE: It does include the hash of the root password in there (the same that goes in /etc/shadow) so if I wanted to have a default for each install I _could_, but I would probably want that to be changed for each new server. I'll have to consider how I'll architect that.


## Installing rsyslog

Since `systemd-journald` is setup by defaut, I'll have to make sure that I get rsyslog integrated in there for the local machine before anything else. I mean, I don't want to _stop_ journald, and I would continue to have it collect logs, but I want to make sure that it passes them on to rsyslog too.


