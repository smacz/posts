> IT operations teams often have a problem with prioritizing their work in the most optimal way. This is due to the enormous amount of tasks IT departments have to deal with. Therefore, using a kanban board to visualize workflows can give a significant advantage to IT operations teams.
>
> -- [kanbanize.com](https://kanbanize.com/kanban-resources/kanban-software/kanban-board-examples/)

But beware the Overprocessing pitfalls:

> Choosing the best tool for a particular task is not always easy, and we are often biased towards the wrong solution. This is especially true when we consider our positive bias towards complexity. A more complex tool might often look more appealing just on the merit of the fact that it supposedly has more features. Unfortunately, we often fail to realize that redundant features are almost always a hindrance. In Lean, we call this Overprocessing.
>
> -- [Shmula.com](https://www.shmula.com/when-not-to-use-kanban-swim-lanes/22585/)

# Boards

# Tasks/Cards

Tasks are both the most regimented and most arbitrary aspect of any kanban board. There is a fine balance that needs to be achieved in the scope of the individual tasks that are displayed on the board.

It is _incredibly_ important to distinguish what must go in the main board, and what must go in the individual tasks as "subtasks". The reason being that if too granular tasks are put onto the main board, the board will get cluttered very quickly. Also, if the tasks are not granular enough, you'll end up with one task sitting in WIP forever without every really getting "Done".

## Tasks that are not broad enough

This is the lesser of two evils, as even though the board gets cluttered, we can still visualize work via the kanban board. However, this is an easy way to lose track of progress and spend too much time planning, and too much time quibbling about who does what which inflates the importance of what should have been quick, easy accomplishments.

This can be helped by transitioning some of those smaller tasks to subtasks of other, more broader-scoped tasks (see below).

## Tasks are too broad

That might be the most demoralizing thing ever -- especially considering the good feeling that comes with moving things "done" as well as seeing that report of "Done" tasks at the end of the week, etc. This is easiest to do on everything except assembly-line/helpdesk boards, which naturally have well-fitting task scopes.

The easiest way to defeat this is to use tags (see below) to tag a particular task to an overarching project(see above). Otherwise, each project would necessitate its own board, which itself would also get overwhelming -- to the point of being unhelpful and almost (but not quite) as laborious as anything called ITIL.

Both of these mistakes can kill the benefits of a kanban board before it even gets started. Luckily, Subtasks and tags offer mitigations for both of these issues.

## Subtasks

These should be tasks that the project manager has in mind that are steps to the accomplishment of a particular task, but that do not necessarily have to be visible on the project board.

## Tags

Since tags are by their nature arbitrary (in any given system, check out [NotMuch](https://notmuchmail.org/)), there should be some kind of rule of thumb for creating them that is universally applicable, otherwise you'll run into edge-cases.

In the event that you're using a priority-based project board (see Priority, under Swimlanes below), which visually represents priority using swimlanes, there are two ways to use tags that are _not_ necessarily mutually exclusive: Projects/Systems and Work Profile.

### Projects/Systems

Another way to think of projects is to think of what systems you're working on. For instance, if I have a CI/CD system that I want to implement which I name "Deploys-a-lot", then everything that is involved with the setup, integration, maintenance, deployment, and support of that system gets categorized with the "Deploys-a-lot" moniker. Using tags as projects allows a given task to be a member of multiple projects/systems at once. This is typically the case, as a failing ticket for a software deployment can be tagged with both the "Deploys-a-lot" tag, and any other system(s) that may be involved.

This is also handy for generating reports of completed work, as it is already organized into projects that managers like to keep track of. Also, in this scenario one-off work can either be tagged as something or left completely untagged, and still be completely valid and not look out-of-place on the board.

### Work Profile

This one is harder to explain, but is implemented by many mature projects. It might be easier for me to list the more commonly-used tags first:

- Bug
- Feature
- Documentation

There is a handy feature in Github wherein a project can provide default templates for issue reports. These tags should be an indication of what kind of default template of information one should expect to see in the description of the task.

For instance, a bug report should always have information on how to replicate, what is effected (scope), and expected behavior. Alternatively, a feature request should have a description of the intended behavior, with what is effected, and the value proposition to the project. Having the ability to normalize the information that expected given the tag eliminates the mental triage that has to be undergone upon the initial review and potential prioritization of a given task.

### Other

There is one usage of tags that is commonly implemented that I am not a fan of. Tags that are made to be removed and indicate the stage of work (e.g. 'needs_triage', 'needs review'). While this may be helpful in issue trackers that are _not_ connected to a board, it defeats entirely the purpose of having a board, as the stage of work is that which the board is supposed to reflect.

Lastly, there is also a subset of 'Projects/Systems' that I like to refer to as 'components' that tags are used for. These components split up projects/systems into arbitrary references such as 'module', 'core', 'plugin', etc. In the example of [Ansible's issue tracker](https://github.com/ansible/ansible/issues?page=3&q=is%3Aissue+is%3Aopen) (simply because it's a project that I know well), where they implement module tags, in the event of a global 'Ansible' board, these should be the specific modules, which can then be more accurately reflected as their actual name, and given a 'file_module' tag, or an 'azure_module' tag. However, these are more prominent in software development organization, and are rarely found in project boards.

# Columns

## WIP Limits

# Swimlanes

There are various things that a kanban swimlane can represent. But before you go dividing tasks, do keep in mind that swimlanes must represent parts of the process that run in parallel.

Examples of how swimlanes may be categorized are:

- Priority
- Date-driven requirement
- Teams/departments/individuals

## Priority

Priority-based swimlanes draw attention to the different levels of task urgency, and puts focus on the most urgent items. Having a visual representation paints a clearer picture, and highlights what tasks need attending to first.

The best implementation I've seen for an IT Operations board has the following swimlanes:

- Critical/Escalation
- Incidents/Tickets
- Business Projects
- Internal Projects
- Maintenance/Recurring Tasks

This prioritizes by scope of impact and visibility. However, it also lends itself to make visible the often-overlooked tasks -- namely the maintenance, critical, and incident swimlanes. This is definitely more of a "Master Process" board, whereas a simplified version may be as follows:

- Expedite
- Standard
- Low

## Teams/Departments/Individuals

Depending on the nature of your project, and the size of your team, allocating groups with their own swimlanes would prove to be useful. This gives teams a chance to take control of their own tasks, while having an overview of the entire work process. Smaller teams may even choose to divide swimlanes between individuals.

This leans towards overprocessing when a project is assigned to a single team or if one SME per team is working on a project (Cross-team organization). In that case, I prefer to assign tasks to individuals, and go from there.

# Calendar

> Normally the Classic Personal Kanban system does not include the latter - a calendar of the foreseeable days ahead - however, that's the logical place for all tasks to go that have any sort of due date or up-and-coming deadline. Therein lies the secret to minimizing continual list revisions when looking to transfer tasks from our backlog of stuff to do into our list of tasks to be tackled today.
>
> -- [productivitymashup.com](https://www.productivitymashup.com/blog/2014/7/17/kanban-calendar-preamble)

Calendars are a way to visualize the _one_ thing that cannot be visualized by a typical kanban board -- how much longer do I have to do this thing?

You can say that kanban boards are a way to work on flows rather than due dates. I would say to start living in the real world -- everyone has due dates.

You can also say that most kanban implementations are able to display due dates right on the board, and that should be enough. However, using that logic, we could also argue that we don't need to visualize work in a kanban board when a simple to-do list will suffice! There is something visceral about calendars that can't be replicated by simple due dates.

# Dependency Graph

A dependency graph that visualizes bottleneck tasks (blockers of many other tasks) is a must-have, as it can be used to determine what priority tasks can be given. In the event that a single task blocks a significant percentage of the backlog or waiting tasks, that task can be expedited or at least increased in priority. This is a measurable standard of how to determine the priority of tasks and bring visual attention to that change in priority as it is represented on the board. And isn't that the whole goal of kanban boards in the first place?
