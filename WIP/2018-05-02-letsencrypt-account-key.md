---
layout: post
title: "Let's Encrypt Account Key"
date: 2018-05-02
category: SysAdmin
program: Letsencrypt
process: Script
description: "Let's Encrypt stores your account key (not your server's private key) in JWF, or 'JSON Web Key' or something, to a PEM format to use in pfSense with their new ACME addon"
references:
  - title: "Let's Encrypt ACME package"
    link: 'https://doc.pfsense.org/index.php/ACME_package'
  - title: "How to load a private key from a JWK into openSSL"
    link: 'https://stackoverflow.com/questions/24093272/how-to-load-a-private-key-from-a-jwk-into-openssl'
  - title: "Github - kalifabian/jwk2pem"
    link: 'https://github.com/kaifabian/jwk2pem'
---

`pfSense` recently added a package that handles Let's Encrypt certificate renewal for you. Of course I'd love to avoid bringing down my webserver every three months to get a new cert. Automate the shit out of everything.

Anyways, having a cert already on my server, I decided I wanted to transfer it over to the pfSense installation. To spare you the details, the first step (that is required by every subsequent step) is to import a key. So I knew that my key had to be on my old server, but I quickly found out it wasn't in PEM format, it was in JWF. So I found a Stack Overflow post on how to convert it to PEM: [{{ references[1].title }}]({{ references[1].link }}. Yeah, I don't think so. Great write-up though.

So I figured that someone must have written a python library for this. Now, while it's not a library, [{{ references[2].title }}]({{ references[2].link }}) does a great job at getting me a PEM-formatted from a JWK-encoded key. It has a couple bells and whistles too, but just passing the path to the file, I was able to get the PEM-formatted key returned to `stdout`. That was enough for me to copy and paste into the account keys setup.

Of course though, our router in this instance is a reverse proxy for the webserver that sits behind it in the DMZ. So when we're looking at doing verification:

> The FTP webroot method is useful when the firewall is performing [...] reverse proxy duty for handling traffic for the domain.
>
> We recommend using this method when no DNS update method is available for use by the firewall.

So I'll want to spin up a stand-alone `ftp` server for this. Luckily, it's going to be similar to the one that we use for the website build.
