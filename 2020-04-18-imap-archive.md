---
layout: post
title: "IMAP Archive"
date: 2020-04-18
category: HomeLab
program: FreeNAS
process: Architect
repo:
image: envelope.png
description: "Let's store all my email that's 180 days or older at home but allow my email clients to access it."
---
# Rationale

Ever since I ran my own email server back in 2016, I thought to myself, "What's the point?" An honestly, it's a question that I don't ask myself enough. What's the benefit of what I'm doing? Not only to myself, but to others. What would be the purposed of having a self-hosted email service. What is the use of using SMTP smart hosts? What's the benefit to using multiple accounts? Why would any other system be better for me to store my email?

## Email as a store of information

Completely unrelated to my own personal email considerations was the result of an (IMO) disastrous policy at $CORP. There was a policy where emails got deleted after 2 years. After being there for almost three, I am beginning to see what a terrible effect this is having on my longevity. For a company that I would estimate is around 75% emailed communication (considering email, tickets, wikis, and project trackers), we are losing a lot that should be indexable and may very well be relevant for years after it is deleted. There should be no reason for this to be the case considering how cheap storage is, and the business need that it presents.

## Come back when you have a warrant!

There is one additional consideration here, in that [warrants are not necessary to obtain emails that on remote services for longer than 180 days](https://www.mcclatchydc.com/news/politics-government/congress/article133340764.html)

> Ambiguous language in a 1986 law created the loophole by extending Fourth Amendment protections against unreasonable search and seizure only to electronic communications sent or received fewer than 180 days earlier.

It would seem that storing the email on a personal computer of mine would prevent the email from being considered "abandoned." So being able to completely remove it from those services and store it somewhere seems like it's going to be a necessity.

## Avoid the lock-in

I can't tell you how many people are reticent to abandon their Google or Yahoo! accounts simply because they have years worth of email communication stored in there. And while yes, it may be that there are too many vendors who are able to reach out to that email address since it has been so long-lived to give it up, it is also partially that the entirety of their online history could conceivably be locked in that one email address. Let's take away one of the hurdles and at least have the email backed up somewhere. How about at your place?

Well, what can I do about this? Given what I know about email, namely that it's made up of several different protocols, without having to actually run a mailserver out of my house, am I able to store my email in a way that still allows it to be accessible to me?

# Requirements

So there seem to only be a couple of requirements that I have:

- The emails are stored at home.
- The emails are accessible.

# Solutioning

Let's try to get these two birds with one stone, shall well? Luckily enough, I now have a FreeNAS server, inherently a storage appliance, that has the ability to segment various services from the internal network in order to expose them to the WAN. If this doesn't sound ideal, I don't know what does.

## Technology

- BSD Jail to house the service and data
- IMAP service to allow storage and query of the emails
- Script to archive the emails from the servers to avoid any email remaining there over 180 days
- Networking to expose the service to the WAN
- IMAP clients (instead of web clients) for email retrieval

To touch on that last point, I've been using email clients instead of webmail for years. It just seems more performant, and easier to use in a windows manager like Cinnamon or likewise. My workflow benefits from having a stand-alone email client. Also, I just found out about [FairEmail](https://github.com/M66B/FairEmail), and it is the first email app that has gotten it 100% correct. I can't say anything good enough about it. However, if your workflow requires you to have a web client, keep in mind that [Nextcloud has a Mail application](https://apps.nextcloud.com/apps/mail) that can connect to IMAP servers.

# Determination

This seems to be quite feasible, and should be relatively easy to accomplish. The fun parts are going to be setting up IMAP in a BSD Jail. I have minimal experience with FreeBSD, so this should provide me a comprehensive journey through the stack. Also, this should give me a good reason to put something personal into my Rundeck instance since I'll most likely be running some kind of an ansible/python script to retrieve emails.

Stay tuned for the installation and configuration!
