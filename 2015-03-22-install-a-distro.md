---
layout: post
title: "Install a Distro"
date: 2015-03-22
category: Battlestation
program: "Operating System"
process: Install
description: A high-level overview of what I mean when I say "Install your favorite distro".
---


So it's not that very hard to make a installable CD/DVD/USB of a linux distro to
install/tryout/etc. There are two basic ways to do it. First however, a little
prep work.


## Get .iso file

Every disto has an `.iso` file that is a bootable filesystem in a condensed
form. The easiest way to find out where they are is by going to [DistroWatch](distrowatch.com).
Fine the distro that you want to demo/install and distrowatch at the top of that
distro's page should give you a link to a download mirror. Learn what your
target computer's architecture is (i386 vs x86\_64) and obtain the appropriate
one.

Now even better than a download mirror is using a magnet link, also know as a
torrent file. If you can torrent the `.iso`, then you can leave the torrent up
and help the next guy who wants to download the distro that you are currently
enjoying on your other install. You'll probably be looking to Transmission or
Deluge to handle that job.

For right now, let's assume that you've got the `.iso` file and that you've 
saved it to `~/Downloads/distro.iso`.


## Get some install medium

I just recently was baffled trying to install a distro onto an old tower. Turns
out it didn't have a DVD drive, just a CD drive and a USB port or seven. So the
DVD's that I burned to launch a live session wouldn't work. No big deal, if you
have the right medium at hand.

CDs work for all of the minimalist distributions, and most of the System Repair
distros that are out there. They try to make them less than the 700MB that CD's
are limited to. Any more than that and you've got to look otherwhere.

DVDs are the logical successor to CDs. If you're going to shill out for DVDs,
you may as well just get a USB or two. They can always be re-written and plus
they look much cooler and sophisticated when used. Either way it's the same
though.


## Install `.iso` file onto the medium

Whichever `.iso` file you choose and whichever medium you end up going with,
eventually you're going to have to put the one on the other. The command-line
way to accomplish this on linux is:

	dd if=/dev/sdb of=~/Downloads/distro.iso

Where `/dev/sdb` is the bootable medium you're creating the image. Keep in mind
it's not a partition thereof, it's the actual storage unit itself. (Aka, it'd be
`/dev/sdb` not `/dev/sdb1/`.) **Read up on** `dd` **before trying this. Linux
assumes that you're the expert and that you really want to do what you're
telling it to do. So make sure you're telling it the right thing.**

Otherwise you got a couple other ways of going about doing this. I'm running
CrunchBang at the moment (based off of Debian) and preinstalled is `xfburn`, a
nifty GUI that makes it easy to create `.iso` disks. 

There are plenty more disk-burning utilities out there in the linux world,
including ImgBurn, Brasero, and others. Check out what your distro comes with,
and use that. 


## Boot from medium

This is the most difficult part. Well, most frustrating at least. Boot up the
target PC to install/demo Linux on, and then get ready to press either:

* Esc
* F9
* F10
* F12

...or anything in between. Those are just the ones that I've seen most often.
See if you can't drag your fingers across the 'F' row as soon as you see the
first graphic pop up onto the screen and hope you don't break anything.

If that's too inaccurate for you, either look up the BIOS that you'll be booting
and see what buttons to press, or boot it up once, look for the button that it 
says to press to get to "Advanced Settings" or whatnot, and reboot. Now you know 
what button to spam when it reboots. 

Once you've got into the BIOS settings, see if it will allow you a one-time
"Boot From X" (CD/DVD or USB...depending) or if you have to change the boot
settings to put your intended drive above the harddrive in the boot order. If
that's the case, no big deal, just make sure you disable that default later if
you execute a successful install.

Every BIOS is different, so once you've made your selection, save and exit.
Hopefully at that point your distro will boot up onto your target machine. If it
doesn't, troubleshoot. Don't be afraid of search engines. Look for compatability
issues if nothing shows up, (ahem...DVD in CD tray...) and work your way
backwards in the steps to see where you might have gone wrong. 

The most frustrating is when your boot medium doesn't show up as an option in
the BIOS. If that's the case, save yourself some time and make a USB (use
unetbootin) and if that doesn't work, you may just be SOL. But keep searching,
keep in mind computers always have a logical reason for doing (or not doing)
things. It may just be the case that you are unaware of that reason temorarily
until you unearth it. Then you can get on with fixing it.


## LiveCD vs Install

Some distros will let you try out the system on your hardware via a Live
environment. Do this if possible. Nothings worse than having to go through two
installs instead of only having to go through one. Try it out. Keep a system
monitor handy to see how many resources you take up at any given time. Test out
the internet browser's impact on the  RAM and CPU usage, and the same for media 
players and anything else intensive that you may be working on (video editing 
comes to mind). 

After you're done with the Live playground, reboot and install. The install
wizard of whatever distro you're using should make it fairly easy...at least if
it's not Arch or Gentoo...the uninitiated may have problems with Slackware...and
Fedora has been giving me a headache lately. I do highly recommend anything
debian-based though. Or CentOS if you want something practical and boring.

After the install, make sure that you eject the install medium so that you're
not booting into another live session or another install session once you
reboot. No need to go through that again.


## Finishing touches

There you have it. If you've installed a bootloader successfully, you'll be able
to access your new distro on your new partion for your new distro. Go ham.

Do us all a favor if you torrented the `.iso` and seed the most recent one so
that others will be able to share in the experience, and spread your new-found
expertise around and preach the good word of FOSS.

If this concludes your first install, save your first install medium as a
momento. I still have the first #! disk that I had installed onto my old-ass
laptop that kept getting the blue screen of death. Good luck and may the FOSS
be with you.
