---
layout: post
title: "My Collection of Tunes"
date: 2017-04-11
category: Battlestation
program: MPD
process: Metacognition
repo: 
image: jbl-3-series.jpg
description: "The background to my blog here is actually the image that was playing when I was being introduced to Ghosts of Paraguay. I've learned about a lot of new bands through just searching for them, and lurking in various other channels, and I thought I'd share my personal collection here for any who were interested."
---

The output here is from the utility `tree`, and organized at the top-level by Genre, then by Artist, and finally Album. I hope someone gets something out of this!

```
Chillstep
├── 88_Ultra
├── Alabama_Shakes
├── Asa
├── Blackmill
│   ├── Miracle
│   ├── Reach For Glory
│   └── Singles
├── Carbon_Based_Lifeforms
├── CMA
├── Detz
├── Emancipator
├── Ghosts_of_Paraguay
│   ├── Ghosts Of Paraguay – Ember (2014) [CHILLOUT, AMBIENT, DUBSTEP]
│   └── zaida
├── Lorn
├── Obeson
├── ODESZA
│   ├── In_Return
│   └── Summer's Gone
├── Ollie_Macfarlane
├── Owsey
├── Phaeleh
│   ├── Afterglo
│   ├── A_World_Without
│   ├── Fallen_Light
│   ├── Falling
│   ├── Fire_and_Isolate
│   ├── Free_Tunes
│   ├── Inside_EP
│   ├── Reconcile
│   ├── Reflections
│   ├── The_Cold_in_You
│   ├── Tides
│   └── Within_the_Emptiness
├── Resotone
├── SizzleBird
├── Sorrow
├── STS9
│   ├── Ad Explorata
│   ├── Artifact
│   ├── STS9_Peaceblaster
│   └── When The Dust Settles EP
├── Stumbleine
└── Tycho
    ├── Tycho - Awake (2014) [MP3 320] politux
    └── Tycho - Dive (2011)
FutureBass
├── Big_Gigantic
│   ├── A_Place_Behind_The_Moon
│   ├── Brighter_Future
│   ├── Nocturnal
│   └── The_Night_is_Young
├── C2C
│   └── Tetra
├── Coyote_Kisses
│   └── Singles
├── Daft_Punk
│   ├── Daft Punk - Random Access Memories (2013) [FLAC]
│   ├── Discovery
│   ├── Homework
│   ├── Human_After_All
│   ├── Random Access Memories
│   ├── Random_Access_Memories
│   └── Singles
├── Electric_Mantis
├── Flume
│   ├── Flume (Deluxe Edition)
│   └── Skin
├── Giraffage
│   ├── Comfort_2011
│   └── Needs_2013
├── Gramatik
│   ├── Gr4m4tik - Expedition 44 (2008-2009)
│   ├── Gramatik - Beatz & Pieces Vol. 1 (2011)
│   ├── Gramatik - #digitalfreedom (2012)
│   ├── Gramatik - Dreams About Her EP (2008)
│   ├── Gramatik - No Shortcuts (2010)
│   ├── Gramatik - Official & Bootleg Remixes + Colabs & Rare Tracks (2009-2012)
│   ├── Gramatik - Street Bangerz Vol. 1 (2008)
│   ├── Gramatik - Street Bangerz Vol. 2 (2009)
│   ├── Gramatik - Street Bangerz Vol. 3 (2010)
│   ├── Gramatik - The Age Of Reason
│   └── Gramatik - Water 4 The Soul EP (2009)
├── GRiZ
│   ├── Chasing_the_Golden_Hour
│   ├── End_of_the_World_Party
│   ├── Good_Will_Prevail
│   ├── Mad_Liberation
│   ├── Rebel_Era
│   │   └── GRiZ - Rebel Era (2013) [FLAC]
│   └── Say_It_Loud
├── Haywyre
│   ├── Draw_the_Line
│   ├── Dubsonic
│   ├── Encompassing
│   ├── Of_Mellows_and_Revelations
│   ├── The_Voyage
│   └── Two_Fold
├── Hocus_Pocus
├── Jazz_Liberatorz
├── Josh_Martinez
├── Justice
│   ├── A Cross The Universe
│   ├── Cross-†
│   ├── CrossPlus
│   └── Productions & Remixes
├── Kill_Paris
│   ├── Kill Paris – Foreplay (2014) [OWS081]
│   └── Kill Paris - To A New Earth (2013)
├── Krix
├── Modestep
│   ├── Evolution_Theory_(Deluxe_Edition)
│   ├── London_Road_2014
│   ├── Singles
│   └── Sunlight_EP
├── NERO
│   ├── Promises
│   ├── Welcome_Reality_+
│   └── Welome_Reality_+
├── Pfunk_Pilot
├── Pretty_Lights
│   ├── Pretty Lights - A Color Map of the Sun (2013) [FLAC]
│   │   ├── Pretty Lights - A Color Map of the Sun (Disc 1)
│   │   └── Pretty Lights - A Color Map of the Sun Live Studio Sessions (Disc 2)
│   ├── Pretty Lights - Filling Up The City Skies (2008) [FLAC]
│   │   ├── Disc 1
│   │   └── Disc 2
│   ├── Pretty Lights - Glowing in the Darkest Night (2010) [FLAC]
│   ├── Pretty Lights - Making Up A Changing Mind (2010) [FLAC]
│   ├── Pretty Lights - Passing By Behind Your Eyes (2009) [FLAC]
│   ├── Pretty Lights - Spilling Over Every Side (2010) [FLAC]
│   ├── Pretty Lights - Taking Up Your Precious Time (2006) [FLAC]
│   └── Pretty Lights - The Hidden Shades (2014) FLAC
├── RJD2
├── Royksopp
│   ├── Junior
│   ├── Senior
│   ├── The_Inevitable_End
│   │   ├── CD1
│   │   └── CD2
│   └── The_Understanding
├── Sound_Providers
├── Subtact
├── The_Glitch_Mob
│   ├── Singles
│   └── We_Can_Make_the_World_Stop
├── Thievery_Corporation
│   └── The_Richest_Man_in_Babylon
│       └── Artwork
└── Wax_Tailor
    ├── 3065 -- Wax Tailor - Wax Tailor Remixes (2008)
    ├── 3129562 -- Wax Tailor - Positively Inclined - The Way We Lived (2007)
    ├── 3133892 -- Wax Tailor - We Be ~ There Is Danger (2008)
    ├── 3208902 -- Wax Tailor - In The Mood For Life (2009)
    ├── 9848124 -- Wax Tailor - Hope & Sorrow (2007)
    ├── Lab'007 -- Wax Tailor - Lost The Way (2004)
    ├── LAB'008 -- Wax Tailor - Que Sera ~ Where My Heart's At (2004)
    ├── MOLE047-6 -- Wax Tailor - Que Sera (2006)
    ├── MOLE069-2 -- Wax Tailor  - Tales Of The Forgotten Melodies (2006)
    ├── MOLE073-8 -- Wax Tailor Featuring ASM - Say Yes (2009)
    ├── MOLE079-8 -- Wax Tailor - This Train Leave It EP (2009)
    ├── UC 332 -- Wax Tailor - Our Dance ~ Walk The Line (2006)
    └── Wax Tailor - Nuit Zébrée Olympic Nantes
Glitch
├── Celldweller
│   ├── Celldweller
│   ├── Live_Upon_a_Blackstar
│   ├── Singles
│   ├── Soundtrack_for_the_Voices_in_My_Head,_Volume_01
│   ├── Soundtrack_for_the_Voices_in_My_Head,_Volume_02
│   ├── Space___Time
│   ├── The_Beta_Cessions
│   ├── The_Complete_Cellout,_Volume_01
│   └── Wish_Upon_a_Blackstar
├── Dodge_and_Fuski
│   └── Singles
├── Flux_Pavilion
│   ├── Excision,_Datsik___Flux_Pavilion
│   │   └── Singles
│   └── Singles
├── Gammer
├── Gemini
│   ├── Graduation
│   ├── Mercury
│   └── The_Blue_EP
├── Knife_Party
│   ├── 100%_No_Modern_Talking
│   ├── Haunted_House
│   ├── Rage_Valley
│   ├── Rage_Valley_31
│   └── Singles
├── KOAN_Sound
│   ├── KOAN_Sound___Asa
│   │   └── Singles
│   ├── Max_Out
│   ├── Singles
│   └── The_Adventures_of_Mr._Fox
├── Madeon
│   └── Adventure
├── Minnesota
├── MitiS
├── Pendulum
│   ├── Baron_feat._Pendulum
│   │   └── Singles
│   ├── Bulletproof___Pendulum
│   │   └── Singles
│   ├── DJ_Fresh___Pendulum
│   │   └── Singles
│   ├── Freestylers___Pendulum
│   │   └── Singles
│   ├── Immersion
│   ├── In_Silico
│   ├── Singles
│   └── subPendulum
│       └── Pendulum
│           ├── Hold Your Colour [2005]
│           ├── In Silico [2008]
│           ├── Other
│           └── Pendulum - Immersion [320Kbps]
├── Porter_Robinson
│   ├── Language
│   ├── Singles
│   └── Spitfire_-_Bonus_Remixes
├── PrototypeRaptor
├── Savant
│   ├── Alchemist
│   ├── Cult
│   ├── ♥_(heart)
│   ├── ISM
│   ├── Mindmelt
│   ├── Overkill
│   ├── Overworld
│   ├── Singles
│   └── Vario
├── Savoy
│   ├── Singles
│   └── Supertrail_EP
├── SirensCeol
│   ├── Singles
│   ├── The_Method_To_The_Madness
│   ├── The_Ocean_EP
│   └── The_Ocean_EP_53
├── Skrillex
│   ├── Foreign_Beggars_feat._Skrillex
│   │   └── Still_Getting_It_(VIP_mix)
│   ├── Kaskade___Skrillex
│   │   └── Singles
│   ├── Korn_feat._Skrillex
│   │   └── Singles
│   ├── More_Monsters_and_Sprites
│   ├── Reptile’s_Theme
│   ├── Singles
│   ├── Skrillex___Damian_Marley
│   │   └── Make_It_Bun_Dem
│   └── Skrillex___The_Doors
│       └── Breakn'_a_Sweat_(Zedd_remix)
├── Sub_Focus
│   ├── Sub Focus - Torus [Deluxe Version] (2013)
│   └── Tidal Wave EP
├── Teminite
│   └── Teminite – Firepower (2014) [JMRECORDS01] [DRUMSTEP]
├── Tut_Tut_Child
│   └── Singles
├── Wolfgang_Gartner
│   ├── Bounce___Get_It
│   ├── Candy
│   ├── Casual_Encounters_of_the_3rd_Kind
│   ├── Fire_Power___Latin_Fever
│   ├── Hot_for_Teacher_EP
│   ├── Killer___Flam_Mode
│   ├── Love___War
│   ├── Shapes
│   ├── Singles
│   ├── Undertaker
│   ├── Weekend_in_America
│   ├── Wolfgang_Gartner___Francis_Prève
│   │   └── Yin___Yang
│   ├── Wolfgang_Gartner___Tom_Staar
│   │   └── Singles
│   └── Wolfgang's_5th_Symphony
├── Xilent
│   ├── Choose_Me_EP,_Part_1
│   ├── Choose_Me_EP,_Part_2
│   ├── Evolutions_Per_Minute___Tenkai
│   ├── Singles
│   ├── Skyward_EP
│   └── Ultrafunk_EP
└── Zomboy
    ├── Game_Time_EP
    ├── Reanimated
    └── The_Dead_Symphonic_EP
Jazz
├── [1964] Oscar Peterson Trio - + 1 Clark Terry (320kbps)
├── 1965. Herbie Hancock - Maiden Voyage
├── 1973 - Chick Corea And Return To Forever - Light As A Feather
│   └── Artwork
├── Abdullah Ibrahim (Dollar Brand) African Marketplace(jazz)(flac)[rogercc][h33t]
├── Art Blakey A Night In Tunisia (jazz)(mp3@320)[rogercc][h33t]
├── Cannonball Adderley - Cannonball Adderley's Fiddler On The Roof (1964) [EAC-FLAC]
│   └── Artwork
├── Cannonball Adderley - Somethin' Else (1958) [EAC-FLAC]
│   └── Artwork
├── Charles Mingus Blues and Roots(jazz)(mp3@320)[rogercc][h33t]
├── Charles Mingus - The Black Saint and the Sinner Lady (1963) 320Kbps - Jazz # DrBN
│   ├── Cover
│   └── Music
│       └── Charles Mingus
│           └── The Black Saint and the Sinner Lady (1963)
├── Chet Baker - The Italian Sessions (1962) [Jazz][mp3 320][h33t][schon55]
├── Deepspace5
├── Diana_Krall
│   └── DianaKrall
├── Dizzy Gillespie & Arturo Sandoval - To A Finland Station (1983) [EAC-FLAC]
├── Duke Ellington
│   └── 1960 - Piano In The Background (2004) FLAC
│       └── Art
├── Duke Ellington - Ellington At Newport 1956 - 2CD
├── Ella_Fitzgerald
│   └── Ella Fitzgerald - Mack The Knife - Ella In Berlin [MG V-4041 Universal Music-Verve 2010 Vinylrip 24bit96kHz][FLAC]
├── Freddie Hubbard - Ready For Freddie (1961) [EAC-FLAC]
├── Herbie Hancock & Chick Corea - An Evening With Herbie Hancock & Chick Corea - In Concert (1978) [2CD] [FLAC]
│   ├── Artwork
│   ├── Disc 1 Of 2
│   └── Disc 2 Of 2
├── Herbie Hancock - The New Standard (1996) [EAC-FLAC]
├── Horace Silver - In Pursuit of The 27th Man (1972) [EAC-FLAC]
├── Jaco Pastorius - The Birthday Concert
├── Jazz_Hop_Cafe
├── John_Coltrane
│   ├── John Coltrane - A Love Supreme 1965 - Verve 2008 - 320Kbps - Jazz # DrBn
│   │   ├── Cover
│   │   └── Music
│   │       └── John Coltrane
│   │           └── A Love Supreme [Verve Reissue]
│   ├── John Coltrane - Giant Steps 1960 - 320Kbps - Jazz # DrBn
│   │   ├── Cover
│   │   └── Music
│   │       └── John Coltrane
│   │           └── Giant Steps
│   ├── John Coltrane & Johnny Hartman - Coltrane and Hartman-1963- 320Kbps - Drbn 163 - Jazz
│   │   ├── Artwork
│   │   └── CD
│   │       └── John Coltrane and Johnny Hartman
│   └── John Coltrane - My Favorite Things 1961 - 2004 - 320Kbps - Jazz # DrBn
│       ├── Cover
│       └── Music
│           └── My Favorite Things [Rhino Bonus Tracks]
├── Joni Mitchell - Blue [mp3-320]
├── Joshua Redman-2013-Walking Shadows
├── Joshua Redman - Wish
├── Keith Jarrett - My Song (1978) [EAC-FLAC]
│   └── Artwork
├── Lee Morgan - Cornbread (1965) [EAC-FLAC]
│   └── Covers
├── Louis Armstrong - New Orleans Nights (1950) [Jazz][mp3 320][h33t][schon55]
│   └── Scans
├── Maynard Ferguson & Big Bop Noveau
│   └── 1996 - One More Trip To Birdland (2003 SACD 24 88.2) FLAC
│       └── Art
├── Mercy, Mercy, Mercy!
├── Michael Brecker - Don't Try This At Home (1988) [EAC-FLAC]
│   └── Covers
├── Miles Davis - Sketches of Spain (1960) 320Kbps - Jazz # DrBN
│   ├── Cover
│   └── Music
│       └── Miles Davis
│           └── Sketches of Spain (1960)
├── Mittens
├── Nujabes
├── Rahsaan Roland Kirk - Talkin Verve
├── Roy Hargrove
│   └── 2008 - Earfood FLAC
│       └── Covers
├── Sonny Rollins - Saxophone Colossus 1956 TQMP
├── Soular Energy
├── Stan Getz & Charlie Byrd - Jazz Samba 1962 - 320Kbps - Drbn - Bossa Nova Jazz
│   ├── Artwork
│   └── CD
│       └── Stan Getz-Charlie Byrd
│           └── Jazz Samba [Bonus Track]
├── Thad Jones & Mel Lewis - Live At The Village Vanguard (1967) [EAC-FLAC]
├── Thad Jones & Mel Lewis - Potpourri (1974) [EAC-FLAC]
├── The Beginning
├── The_Buddy_Rich_Big_Band
│   └── Big_Swing_Face-1996
├── The Tony Bennett_Bill Evans Album
├── Time Out
├── Top of the World
├── Wayne Shorter - Adam`s Apple (1966) [EAC-FLAC]
│   └── Artwork
├── WeatherReport__Heavy__Weather
├── Wynton Marsalis - Big Train
└── Wynton Marsalis - Standard Time
    ├── Marsalis Standard Time, Vol. 1
    ├── Standard Time, Vol. 2_ Intimacy Calling
    ├── Standard Time, Vol. 3_ The Resolution of
    ├── Standard Time, Vol. 4_ Marsalis Plays Mo
    ├── Standard Time, Vol. 5_ The Midnight Blue
    └── Standard Time, Vol. 6_ Mr. Jelly Lord
```
