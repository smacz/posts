---
layout: post
title: "Running a Bitcoin Unlimited Node"
date: 2017-03-10
category: HomeLab
program: Bitcoin
process: Install
repo: https://andrewcz.com/gitlist/bu_node.git/
image: bitcoinnodestats.png
description: "I decided that since I have the available bandwidth, technical skills, and hardware to do so, that I would run a node. I chose to support Bitcoin Unlimited because I believe that software provides for a free market instead of a fee market."
references:
  - title: Bitcoin Unlimited
    link: https://www.bitcoinunlimited.info
  - title: Running a Bitcoin Node on Debian Wheezy
    link: https://www.development-cycle.com/2015/01/running-a-bitcoin-node-on-debian-wheezy/
  - title: How to build and install Bitcoin on CentOS 7
    link: https://www.ndchost.com/wiki/how-to-install-bitcoin-on-centos-7
  - title: Bitcoin Unlimited
    link: https://www.bitcoinunlimited.info
  - title: How to set up a Bitcoin Unlimited full node
    link: https://keepingstock.net/how-to-create-a-bitcoin-unlimited-full-node-on-a-hosted-server-5bc858a614e3
  - title: Running Bitcoin
    link: https://en.bitcoin.it/wiki/Running_Bitcoin#Bitcoin.conf_Configuration_File
  - title: "Miner Guide: How to Safely Hard Fork to Bitcoin Unlimited"
    link: https://medium.com/@ViaBTC/miner-guide-how-to-safely-hard-fork-to-bitcoin-unlimited-8ac1570dc1a8
  - title: Running a Full Node
    link: https://bitcoin.org/en/full-node
  - title: How to install Python 3 on CentOS 7
    link: http://idroot.net/tutorials/how-to-install-python-3-on-centos-7/
  - title: How to use Django with Apache and mod_wsgi
    link: https://docs.djangoproject.com/en/1.10/howto/deployment/wsgi/modwsgi/
  - title: RPC binding commands
    link: https://github.com/bitcoin/bitcoin/pull/3695
  - title: Ansible Template within a template
    link: https://github.com/ansible/ansible/issues/4638
---

# Wow, that was an ordeal.

Not only did it take _forever_ to sync the entire blockchain, it also just so happened that I downloaded the client:

1. Several hours after 1.0.1.0 was released
2. Several days before [the `assert(0)` bug](https://www.reddit.com/r/btc/comments/5zgefe/this_was_an_orchestrated_attack/)

The second of which, occurred while I had less than 10,000 blocks to go to get caught up. Luckily I wasn't shut down, but I upgraded my client as soon as possible nonetheless. Regardless of all that, I still believe that all of this was worth it, and I am very glad at where I am today, running BTC Unlimited node in support of my favorite cryptocurrency.

## Compiling BTC Unlimited

I did, and there was absolutely no benefit over grabbing the binaries, as usual (though don't ask me about `rtorrent-vi-color`). The links below can help anyone out who wants to, but it's hardly worth it, and very time consuming on top of an already time-consuming process.

## CPU cores

I found out that when I only allocated 1 core to process the download, it became a bottleneck. I upped it to two, and it was solidly at ~75% for the rest of the duration of the download. After the download finished, the VM continues to run at around 75% utilization.

# Bitcoin Unlimited itself

There's not much to get super excited over. It's only a platform on which other things were meant to be built upon (see below). I highly recommend `bitcoin-cli help` && experimenting over any type of documentation. What I read was a waste of time. However, finding out when my node was synced was of much importance to me while I was waiting.

Luckily enough there's the perfect op-code for that - `getblockcount`. There are ways of scripting this, but since I had no good output, I figured I'd just look at it every now and again. Bitcoin.info has the current block count that I was able to compare it against to see how close I was to completion - it took longer than it'd like. However, I did realize that I was only leeching off of the network when I checked the connection count. When `getconnectioncount` returned only 8 peers, I went looking online. It appears that is the maximum number of download connections that are available at any given moment. Maxing those out is an indication of not having an port open to receive connections (port 8333 by default). After NATting it and taking care of `iptables`, the connection count rose quickly and hovered consistently around 30, give or take a few.

# Bitcoin Node Stats

Here's where the fun began. And by fun I mean hours of figuring out what Django is, how it works, what was going on here, etc. But in the end, it was worthwhile because it already had a dark color scheme that I didn't have to tweak, and it came with nifty charts and a color coded connection list. It is, in a word, primo.

redirect

static_root static_url

python manage.py collect static

disabling admin

local_settings.py and settings.py

static/js/plots.js (/bu-node/)static/

installing python3

getting apache to run python3 in its env
