---
layout: post
title: "Firewalling Off Backups"
date: 2020-03-04
category: HomeLab
program: pfSense
process: Configure
repo:
image: vlans.jpg
description: "I wanted to set up VLANs in order to firewall off a Freenas share that is going to be able to be utilized by external services."
references:
    - title: "Configuring VLANs on pfSense"
      link: 'https://www.highlnk.com/2014/06/configuring-vlans-on-pfsense/'
    - title: "How to create and configure VLANs in pfSense"
      link: 'https://www.iceflatline.com/2013/09/how-to-create-and-configure-vlans-in-pfsense/'
    - title: "How to configure VLAN on Zyxel Switch"
      link: 'https://support.zyxel.eu/hc/en-us/articles/360001390814-How-to-configure-VLAN-on-Zyxel-Switch'
    - title: "Virtual LANs (VLANs)"
      link: 'https://docs.netgate.com/pfsense/en/latest/book/vlan/pfsense-vlan-configuration.html'
    - title: "9.2.3 VLANs"
      link: 'https://www.ixsystems.com/documentation/freenas/11.3-U1/network.html#vlans'
    - title: "11.11 S3"
      link: 'https://www.ixsystems.com/documentation/freenas/11.1/services.html#s3'
    - title: "ACME package"
      link: 'https://docs.netgate.com/pfsense/en/latest/certificates/acme-package.html'
    - title: "Dynamic DNS configuration with pfSense"
      link: 'https://www.cloudns.net/wiki/article/268/'
    - title: "Reverse Proxy with pfSense and Squid"
      link: 'https://travellingtechguy.eu/reverse-proxy-with-pfsense-and-squid/'
---

# I should be using a router for this

In all honesty, I should be using a router for this. I am firewalling off several different interfaces on a pseudo-SAN device, and wanting to put a firewall in between them in order to segment the traffic allowed between what is exposed, and what is accessible by the internal network. However, being the cheapskate that I am, I already have a capable switch, which means that it's VLANs for me.

# Update your firmware

Just a reminder to all of the sysadmins out there, you're not a real sysadmin if you're running out of date systems.

Of course, I say that, and am guilty of that myself. I find it's better to automated upgrades if at all possible, but shit happens. I found out that my switch was running firmware from 2013. After downloading the latest update, and a couple of hard resets later, I was running the latest updated version of firmware on my switch. I figured I'd rather do that _now_, before I configure everything, rather than later after it's all set up and where a hard reset would set me back days.

# VLANs

## pfSense

As would be expected, **pfSense** makes plenty of room for VLANs. This is (at this point) basic functionality for a router. I was able to create three VLAN interfaces and associate them with the physical interface on my whitebox router that hooked up with my switch a la [{{ page.references.0.title }}]({{ page.references.0.link }}). This included setting up DHCP servers on each of those interfaces in the appropriate subnets. We'll get into firewall rules soon enough, but right now we should go over to the switch to get that set up.

## turnout

I named my switch turnout, due to the fact that old railroad switches, the ones that get thrown at the last minute to avoid total disaster, were called "turnouts" back in the day.

It'd be much nicer if I was able to get the pfSense interface names (`em1.X`) to line up with the VLAN tags with the port numbers on the switch. Fortunately, I have a small network, so keeping track of that isn't going to be vital, and should be easy to look up in the case that... I forget. Which means I should probably note this down in a wiki or some other information store that allows for me to aggregate an up-to-date inventory and more specification-intense write-up than would be appropriate for a blog. However, noteworthy is that the setup in many of the devices that I had to deal with offered a "Description" field, which was helpful to make sure I noted which thing was which, consistently.

So something that I needed to understand in order for this to work is how the GUI was trying to tell me stuff, specifically the VLAN Port screen. This showed me which ports would accept various VLAN IDs. For instance, on the incoming port that I wanted the traffic to be tagged, I had to switch that to "Untagged", so it would take all untagged traffic, and tag it with the assigned VLAN (set in the Port tab of the page). Also, for every VLAN ID, I had to set the 'Tagged' field for Port 1, which was the trunked port that was relaying all of the traffic back to my upstream pfSense router.

## Freen54

I know it's been a while since I talked about Freen54, which is because it's been such a workhorse for me. Now I am asking it to do a bit more, but giving it some long-needed TLC in return.

So here's a good one. Only one interface on the box can use DHCP. It's not like that's going to cause much of an issue for me, but it's still weird. I get it messing with default routes and everything, but still, that's annoying when ProTense can just as easily use DHCP for those networks. I'll guess I'll assign the admin interface to use DHCP, since it's on the same network as the switch and other hardware, and the rest of the interfaces can be hardcoded to their various networks.

Apart from that, setting up VLANs is fairly straightforward. First you create a VLAN on the VLAN screen of Networking, and associate it with an interface. Then set up that interface's IPv4 address (to something within that VLAN), and you should be good to start pinging gateways and shit.

# Backup as a Service (BaaS)

That's sarcasm, obviously. I'm not creating an _'aas'_ here. I'm just providing regular-ass services using regular-ass networking tricks.

## S3

So FreeNAS comes with a built-in S3-compatible service that can be used to store/access files: [{{ page.references.5.title }}]({{ page.references.5.title] }}). This provides both API and HTTP access to files in there. This is ultimately the place where I am going to be storing my backups. I'll also be exposing this service to the interface that is connected to the VLAN that is only accessible from the WAN and the admin network. And to get to it from the WAN, I'll have to go through the pfSense router.

#### Whenever a change is made to the S3 service, make sure to restart the Freen54 server, it doesn't take kindly to changes on the fly.

## pfSense DDNS and Cert Generation

In order to set up SSL termination, I have to implement SSL cert generationon my router in the first place. This is easy enough with the [{{ page.references.6.title }}]({{ page.references.6.link }}).

Since my service was using ClouDNS to maintain DNS records as it is, I already have a Sub Auth ID that I can use to manipulate records in that DNS provider.

However, first, I wanted to make sure that my IP address was able to be updated by my router in case it changed using a kind of DDNS service provided by ClouDNS. Luckily, that was facilitated by pfSense with [{{ page.references.7.title }}]({{ page.references.7.link }}). However, since I don't have Premium (just Personal), I had to follow the instructions at the bottom of the page as a workaround.

That basically consisted of me creating it in ClouDNS, activating "Dynamic DNS", and then copying the URL to paste in pfSense.

After that, running the certificate generation script was successful.

#### Make sure to put the actual ID (not the username!!!) of the 'sub-auth' account

## Reverse Proxy

First I had to install the [{{ page.references.8.title }}]({{ page.references.8.link }}). The interface has changed a bit, but that walkthrough is still valid. Especially the part about having to _create_ the tunable of `net.inet.ip.portrange.reservedhigh`, since it is not present by default.

The thing that was difficult for me to suss out was the `http` to `https` redirect. This should be done fairly transparently. However, since this is typically going to be used as an API endpoint, there should only really ever be `https` connections made to it. However, in the case where someone was to try to get access to it via a WebGUI, they would most likely be logging in with `http`, so we just have it direct to the base URL instead of trying to be fancy and capture the path along with the domain.

For the certificate location, it _will_ use your `letsencrypt` cert, **however**, you should also ensure that you place the intermediate certificate in the specified spot. You can get this by going to **[Diagnostics]** --> **[Command Prompt]** and issuing: `cat /conf/acme/OCBackups.ca` and putting that in that location.

Keep in mind that you can't just start the service now. First, you have to Enable Squid Proxy on the main page, and then configure the Local Cache (literally just visit the page and hit 'SAVE') before you can start the service.

Also don't forget to allow that traffic in through your WAN interface in your firewall.

# Testing

Once everything is set up, go ahead and test with the following `curl` command:

```
$ curl -L -vvvv -H "User-Agent: Mozilla" http://<external url>/minio/login
```

You should get back the HTML of the login page. Congratulations! Your new S3-compatible object storage service is available to you from wherever you need it. Enjoy!
