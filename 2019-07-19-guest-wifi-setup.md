---
layout: post
title: "Guest Wifi Setup"
date: 2019-07-19
category: HomeLab
program: Networking
process: Architect
repo:
image: radio_waves.png
description: "Using DD-WRT is pretty fun, until you have to do fun stuff in it."
references:
---

# Setup

I'm trying to get a couple things done here:

- Have an unsecured Guest WiFi network
- Have a captive portal on the Guest WiFi
- Have a secured Confined WiFi network for consoles and IoT
- Have a secured Admin WiFi network

The one thing that has been tripping me up lately has been that as soon as I put the guest WiFi VAP security to `Disabled`, I get a crapshoot of networks actually available to devices. But let me back up a bit.

## Start Here

I had to hard reset my router. Tons of fun. Luckily, there were only really the two networks on there, and not much else. So recreating should be fine, right?

I started out naming the router and setting the local IP address/DHCP server, along with allowing remote access. Then I hopped back to my wired admin network for the rest.

In `Wireless --> Basic Settings`, I named the two physical interfaces, and set them up with WPA2 security. They're both bridged.

Next, in `Setup --> Advanced Routing` I set the Operating Mode to `Router`. This should mean that my packets get routed instead of NAT'd.

Now, I have a wireless client that is connected to one of the physical interfaces, and I am able to ping the router's IP. Great!

Now, I have a firewall that is the router's default gateway. It's a pfSense device that does everything, but let's just call it the firewall for the moment.

I can't ping that, and I know the firewall has an "Allow all pings from anywhere to anywhere" rule, so it shouldn't be blocked at that level. So, is it a routing problem? Let's take a look at the firewall on the router.

Under `Security --> Firewall`, I disabled the SPI firewall. After that, I was able to ping the firewall. Great! I am also able to `curl` to it's login page. Even greater!

Let's look at where those requests are coming from though. On the firewall, let's run a `tcpdump` of everything trying to connect to the firewall. It looks like they're coming from the internal IP addresses, which is great! So our router is acting as a router now.

Can we get anywhere? When I try to ping 8.8.8.8 I am able to. However, I am not able to ping `google.com`. It looks like the router is intercepting DNS requests. What I had to do was to uncheck `Use DNSMasq for DHCP` on the page `Setup --> Basic Setup`. Now I am able to resolve hostnames.

## Add VAPs

At this point, I have my router acting as I would like it to for my wireless client. Now I want to create two new VAPs and try to use them in the same manner.

At this point, I follow the instructions on [Guest WiFi + Abuse Control For Beginners](https://wiki.dd-wrt.com/wiki/index.php/Guest_WiFi_+_abuse_control_for_beginners) to set up the new network, Enable Forced DNS Redirection, and enable DHCPd for the guest WiFi. I rebooted the router, and now I can't even see the new VAP.

---

# Restart

So I had to hard reset yet again...yay. I ran into a couple things.

### Local IP Address

Here's a tip - do not reset your Local IP Address to something that is outside of the subnet that your client. This means `192.168.1.0/24` for default installs.

## Setup

Other than that, go ahead and set up the rest of the front page of setup appropriately. `Router Name`, `Hostname`, `Domain Name`, `Gateway`, `Local DNS`, NTP stuff, etc.

Next, we're going to set up our physical wireless interfaces. The details are as follows:

```yaml
Wireless Mode: AP
Wireless Network Mode: Mixed
Wireless Network Name: 'The_Method'
Wireless Channel: '6 - 2.437 GHz'
Channel Width: '20 MHz'
Wireless SSID Broadcast: Enable
TurboQAM: Enable
Explicit Beamforming: Disable
Implicit Beamforming: Disable
Airtime Fairness: Disable
Multicast To Unicast: Disable
Network Configuration: Bridged
```

Save the configuration, then go to **Wireless** --> **Wireless Security**. There, change both of the `Security Mode` fields to `WPA2-PSK`, and **Save**. The put in the same password for each field of `WPA Shared Key`. **Save**, and **Apply Settings**.

### Bridges

Next, to to **Setup** --> **Networking**. This will be the place where we set up the bridges between the networks. For the time being, just **Add** two bridges under `Bridging`, called `br1`, and `br2`, (`br0` should already exist). We don't need `STP` on any of them.

Next, set up the networking for `br1` and `br2` under `Port Setup`. Set up the `Label` as the names to call them.

Then, set up two new DHCP servers.

## Router IP Addressing

Then, go back to the front page, and set up the **Basic Setup** --> **Network Setup**. We're going to change the router's IP address to what we want it to be, in this case `192.168.0.1`. However, this means that we will lose not only our subnet, but in newer versions, we will lose DHCP as well. So we'll have to statically assign an IP address to our client machine, and continue using that for the time being.

In order to get rid of that, go back to the **Setup** --> **Networking** page, and at the very bottom, create a DHCP server on `br0` (where `VLAN1` should be), and that will restore DHCP functionality to your ethernet ports.

## VAPs

First create two Virtual Interfaces for each physical interface in the **Wireless** --> **Basic Settings** tab. 

Then go to **Setup** --> **Networking** --> **Assign to Bridge**. Assign `wl0.1` and `wl1.1` to `br1`, and `wl0.2`, and `wl1.2` to `br2`. This sets the 2.4GHz and 5GHz channels up on the same bridge, so at the IP layer, they can share a subnet. Fill out the **Network Configuration** piece for those two bridges, with their `Optional DNS Target` (aka upstream DNS server), `IP Address`, and `Subnet Mask`. Disable `Masquerade/NAT`, and enable `Net Isolation` and `Forced DNS Redirection`; we want these to be routable from the `hub` network for management purposes. Lastly on that page, in the **DHCPD** section, set up DHCP servers for those two bridges.

**SAVE** --> **Apply Settings**

#### NOTE: This is not going to work yet. Continue after SSH to learn more.

## SSH

**Services** --> **Secure Shell** 

```yaml
SSHd: Enable
SSH TCP Forwarding: Disable
Password Login: Enable
Port: 22
Authorized Keys: <Public SSH Key>
```

**Save** --> **Apply Settings**

**Administration** --> **Reboot Router**

## JFFS

We are going to enable JFFS so that we can store the custom `wlconf` binary that is provided [here](https://forum.dd-wrt.com/phpBB2/viewtopic.php?p=1145042#1145042).

Here's a copy-paste from the [wiki page](https://wiki.dd-wrt.com/wiki/index.php/JFFS):

>The steps to enable JFFS through the router web page are very specific. To avoid having to reset and reprogram your router, it's smart to make a backup here of your settings. If you follow these steps exactly, it should not lock up.
>
>1. On the router web page click on Administration.
>2. Scroll down until you see JFFS2 Support section.
>3. Click Enable JFFS.
>4. Click Save.
>5. Wait couple seconds, then click Apply.
>6. Wait again. Go back to the Enable JFFS section, and enable Clean JFFS.
>7. Do not click "Save". Click Apply instead.
>   - The router formats the available space.
>8. Wait till you get the web-GUI back, then disable "Clean JFFS" again.
>9. Click "Save".
>10. It may be wise to Reboot the router, just to make sure
>
>If you don't see the Enable JFFS option on your Administration page, it is likely because your router has insufficient flash memory to hold both DD-WRT and a JFFS partition. If you have a 4MB router, you may be able to work around this by using the mini version of DD-WRT

## `wlconf`

At this point, we need to get the modified `wlconf` (link above) and transfer it to the router. `scp` works just fine:

```bash
$ scp -r Downloads/wlconf.gz root@192.168.0.1:./
```

Then, log into the router, unzip it, and move it to an appropriate location under the `/jffs/` directory:

```
$ ssh root@192.168.0.1
DD-WRT v3.0-r40270M kongac (c) 2019 NewMedia-NET GmbH
Release: 07/11/19
Board: Asus RT-AC68U
root@192.168.0.1's password:


BusyBox v1.31.0 (2019-07-11 02:17:14 CEST) built-in shell (ash)

root@my-method:~# ls
wlconf.gz
root@my-method:~# gunzip wlconf.gz
root@my-method:~# ls
wlconf
root@my-method:~# mkdir /jffs/bin/
root@my-method:~# mv wlconf /jffs/bin/
root@my-method:~# chmod +x /jffs/bin/wlconf
```

Before we set everything up, go ahead and try it out to make sure that it works as expected:

```
root@my-method:~# stopservice nas; stopservice wlconf
root@my-method:~# /jffs/bin/wlconf eth1 up; /jffs/bin/wlconf eth2 up
[...]
eth2: Not supported
[...]
eth2: Invalid argument
[...]
root@my-method:~# startservice nas
```

#### The commands above were broken apart in a completely arbitrary manner

#### There will most likely be a lot of output after the `wlconf ethx up` commands. That is typical, and everything should still work despite those errors being thrown.

Test it out using a wireless client.

If everything is working up to now, let's make it resilient.

### Startup Commands

Getting back into the GUI, let's go to **Administration** --> **Commands** and put in the following, much like in the above ssh session:

```
sleep 30
stopservice nas
stopservice wlconf
/jffs/bin/wlconf eth1 up
/jffs/bin/wlconf eth2 up
startservice nas
```

**Save Startup**

We make sure to give it enough time to get up and running before we swap out the `wlconf` executables and move forward from there.

#### Extra bonus points if you're gutsy enough to symlink the original binary to the one on the JFFS mount.

## Security

And lastly, we want to make sure that none of our guests or our consoles/IoT devices can access the router's GUI, so we set an IPTables rule to prevent that. In that same screen:

```
iptables -I INPUT -p tcp -s 192.168.1.0/24,192.168.2.0/24 --dport 80 -j REJECT --reject-with tcp-reset
```

**Save Firewall**

#### Be warned that this _will_ lock you out if you're still on either of those subnets. You might have to start all over again if you mess this up.

## Cleanup

Feel free to reboot after cleaning up anything that was left over from this. If you followed this precisely, you shouldn't have anything else to do. If you're me though, you'll have to clean up all of your other failed attempts to make sense of this.

# Backup

Last but not least, now's as good of a time as any to take a backup of your config:

**Administration** --> **Backup** --> **Backup**

There. Now see? Was that so hard?
