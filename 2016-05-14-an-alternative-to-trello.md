---
layout: post
title: "An alternative to Trello"
date: 2016-05-14
category: HomeLab
program: Kanboard
process: Install
description: "Using the Kanban philosophy of project management, Kanboard is a self-hosted web-based project board to organize your life. It's a little bit of todo.txt, and a little bit of scrum; with a lot of devops."
references:
  - title: Get started with Firewalld in RHEL 7
    link: https://www.certdepot.net/rhel7-get-started-firewalld/
  - title: Kanboard Requirements
    link: https://kanboard.net/documentation/requirements
  - title: Using /usr/local
    link: http://hivelogic.com/articles/using_usr_local/
---

# Philosophy

I mean, you can just google it. It has a really cool history originating in manufacturing which translates great to computer work - the way I do it anyways. So let's get it set up

# Requirements

## Apache

Apache is simple, it's just the `httpd` daemon that's installed with the CentOS 7 "Basic Web Server" install set up, but it's easily downloaded with:

```
# yum install httpd
```

## Sqlite 3.X

Sqlite was also installed with the "Basic Web Server" install, but it is in the repos under `sqlite`. Easy enough, right?

#### NOTE:

I had attempted the setup with Postgresql but had not been able to get it working. That may become a project for a later date...I'll have to put it up in kanboard...

## PHP

PHP was the most annoying to set up. Kanboard requires a couple php extensions along with the base php install itself. There's not much configuration, and the Requirements page for Kanboard shows all of the plugins that need to be installed. I guess that it's just that I'm not that experienced in PHP. Anyways, it ultimately boiled down to:

```
# yum install php php-common php-gd php-mbstring php-pdo
```

## Kanboard-latest.zip

This is odd, because this can be done anywhere, and most walkthroughs say to do in in `~`, but I prefer `/usr/local`. [For reasons](g). Just creating a subdirectory of `kanboard` works, and it's easy to access, remember, and script.

That being said, the actual process is downloading, and moving the relevant files of the kanboard install to the web server directory. Accomplished by:

```
# mkdir /usr/local/kanboard
# wget -O /usr/local/kanboard/latest.zip http://kanboard.net/kanboard-latest.zip # see NOTE
# unzip /usr/local/kanboard/latest.zip -d /usr/local/kanboard/
# cd /usr/local/kanboard/kanboard
# mv /usr/local/kanboard/kanboard/config.default.php /usr/local/kanboard/kanboard/config.php
# mkdir -p /var/www/html
# cp -ar /usr/local/kanboard/kanboard/ /var/www/html/
```

#### NOTE:

Search for the bash alias `extract` - it's a great function that's in the nooks and crannies of the internet *coughredditcough* and get used to what programs it uses. I always make sure to have all of them on hand when working in any given environment if at all possible. That alias works wonders.

# Post-install

## Kanboard's data directory

Kanboard has a `kanboard/data` directory that is used to store, well, data. That directory needs to be writable by the webserver user - in this case `apache`.

```
# chown -R apache:apache /var/www/html/kanboard/data/
# chmod -R 777 /var/www/html/kanboard/data/
```

That in itself isn't complicated. However, there's a catch with SELinux. Namely that ["It's enabled with a lot of restrictions"](https://github.com/fguillot/kanboard/issues/335#issuecomment-60023568). Now, I'll mess with SELinux when I have a couple of hours or so I want to kill, but luckily the creator came up with the solution to this problem so I don't have to.

```
# chcon -R -t httpd_sys_content_rw_t /var/www/html/kanboard/data
```

#### NOTE:

Perhaps as a result of that (or a botched install for some mistype or another) I ran into another SELinux problem, where my entire directory had no access permissions. This was also fixed by a `chcon` command.

```
# chcon system_u:object_r:httpd_sys_content_t:s0 -R /var/www/
```

## Web Server

If you haven't already started the `httpd` daemon, you may start it up now.

```
# systemctl start httpd
# systemctl enable httpd
```

If you have already, make sure to restart it after applying the SELinux adjustment above.

## Firewall

If this is your only webpage on this server, then port 80 will have to be made accessible to the public. On CentOS 7, `firewalld` is installed by default and is managed by the `firewall-cmd` command.

There's a lot of cruft out on the internet about `iptables`, and IIRC `firewalld` is just a front-end for it, but it's easier to use, if not still a bit buggy. Anyways, I can get this up and running in two simple commands.

```
# firewall-cmd --permanent --zone=public --add-service=http
# firewall-cmd --reload
```

The second command is necessary to activate the change. Contrary to the `--complete-reload` option, current connections are not stopped.

## SSL/TLS

Obviously this site should be secured, however that requires a domain name to be registered. I have not done so yet, but when I do, I'll be able to use Let's Encrypt to set that up. I'll also have to change the webserver and firewall config to reflect those changes (at the very bare minimum least those two parts) if not the various applications themselves. That's for another time though. For now, enjoy the install.

# Further Exploration

And that's it. You should now have a working instance of Kanboard on your server. The default username/password combo is

> admin/admin

So for the love of all that's good, change it as soon as possible. Some files to take a look at would be:

* Sqlite database: `db.sqlite`
* Debug file: `debug.log` (if debug mode enabled)
* Uploaded files: `files/*`
* Image thumbnails: `files/thumbnails/*`


#### UPDATE

I'm still using it after getting into OSU and it's really be the thing that keeps me working. I can look back and see all the things that I've accomplished in the 'Done' column, and that motivates me. Also I can prioritize and know what thing or what thing out of a group of things that I should tackle next. It's a weight off my mind.
