---
layout: post
title: "Callback Service for Desktop Support"
date: 2017-07-08
category: Battlestation
program: Callback
process: Architect
repo:
image: internal_error.png
description: "Previously, I had set up a callback script to set up desktop support for my family members' desktops. Recently, I refurbished my buddy's Macbook Pro, and put Ubuntu on it. I figured I might as well set it up for him as well, with a couple refinements."
references:
  - title: Unity Launchers and Desktop Files
    link: https://help.ubuntu.com/community/UnityLaunchersAndDesktopFiles
  - title: Port Forwarding
    link: https://help.ubuntu.com/community/SSH/OpenSSH/PortForwarding
  - title: SSH tunnel via multiple hops
    link: https://superuser.com/questions/96489/an-ssh-tunnel-via-multiple-hops
---
My good acquaintance [yupyupp](https://haffner.me/) was talking about the fact that he puts callback scripts into all the boxens that he sets up for his family. In the event that they have an issue, with this method he has a root shell with which to debug whatever issue they may make for themselves. Now it's been awhile since I set this up for my parents, so I wanted to make sure that I wrote down for my future self exactly how it's done.

# Overview

So the 15,000-ft overview is that:

1. The damsel in distress opens a reverse shell to our server.
2. We `ssh` to our server.
3. We login to `damsel`'s boxen through the reverse shell.

As long as I grant my user sudo privileges on the remote boxen, I then can have a root shell to do the needful. I'm sure that there are many ways to achieve this, but there are several advantages to this scheme in particular:

- This requires no firewall rules for either party to implement
- This can be used regardless if one or both parties are in another network
- This is a non-permanent solution, and can be turned on and off at will

#### This setup makes heavy use of `ssh` keys and other expeditious processes. Might as well make it easy on everyone involved.

## Server setup

The first thing to do is to set up the server so that both entities can connect to the remote server. This requires keys on the `damsel`'s boxen at least, and then, keys on ours as well. So first, let's set up the necessary accounts. Of course, we don't want to log into the server as the root user, so whatever admin account you want to set up, you would want to take your deck (as Neal Stephenson might call your go-to desktop) and set up `ssh-keys` to log into the admin user with the keys.

Assuming that the admin user has many of the same powers as the root user, let's go ahead and create the `damsel`'s account that they'll be logging into.

```
# useradd damsel
# passwd damsel
```

### Password strength

So I _tried_ to go the high road and set up a randomized passwd for the `damsel` user, but after several times of _not_ being able to get `damsel` to log in, I was forced to change the passwd to something more typable. In this instance the setup was done over the phone (which was an accomplishment in itself), and after relaying the passwd - character by character - twice, I decided to just change the passwd to a line of one of our favorite songs (no spaces) in order to have `damsel` `ssh` in. Since only the first login is required to transfer the keys, after the keys are transferred I was able to change the passwd back to something secure without having to transmit the passwd to `damsel`.

## Damsel Setup

This is generic for Linux until we get to the desktop file. There are a couple moving parts here, so take a look at these.

### Script

This script starts the callback script if it isn't running, and kills the ssh tunnel if it is running.

```
#!/bin/bash

sessions=$(ps aux | grep -e "ssh.*R" | grep -v "grep" | tr -s ' ' | cut -d \  -f 2)

if [[ ${sessions} ]]; then
    for session in ${sessions}; do
        kill -9 ${session}
    done
else
    /usr/bin/ssh -NTq -o ServerAliveInterval=30 -o ServerAliveCountMax=3 -R <port>:localhost:22 -i /home/damsel/.ssh/id_rsa damsel@<remote server>
fi
```

In essence, this opens up a reverse tunnel to `<remote server>` in order for us to use to get to the desktop in question if it is not running already.

It is best to put this in `/usr/local/bin/callback.sh`.

### Desktop Icon

This script is fine on its own, however in order to make it easy for plebs, we are going to create a desktop icon to activate this. The desktop icon entry is going to look like:

```ini
[Desktop Entry]
Name=Callback
Exec=/usr/local/bin/callback.sh
Icon=/home/damsel/Pictures/callback.png
Terminal=false
Type=Application
```

Putting this in `/usr/share/applications/callback.desktop` will usually work and create the entry. However, this does involve `sudo`, and there is probably a way to make this a user-only entry. Especially considering that in the above entry, we rely on a file in `damsel`'s home directory.

### Initiation keys

Since we want the script to be able to be invoked by the user without any input, it is easiest to set up keys for the user to our `<remote server>`. As is applicable to the all account setups here, we can either use `ssh-copy-id` to transfer the keys (if we do allow passwd-based logins on the `<remote server>`) or just place the relevant `ssh` public keys into the `authorized_keys` file of the `damsel` user otherwise.

### Admin

In order to be able to ssh into the remote boxen as a privileged user **without** using `root`, we need to setup a user on the remote boxen that has sudoers privileges. This is the same method as adding any other use to a box - as root without `sudo`, or as `damsel` with `sudo`. Either way, on most linux distros we are looking at:

```
# useradd -D admin
# groupmod -aG sudo admin
```

Or something of the kind. Once again, any secured boxen would not allow `root` to log in via `ssh`, so we want an alternate user that requires a password to escalate their privileges.

### Keys

Here's a bit of finagling. In order to have our deck's user to have a password-less login, we should transfer our ssh-keys to the remote boxen's `admin` account. We would still need our passwd to get a root shell, but we can make it easier for ourselves to log in by setting up ssh keys. Also, we can still configure the `ssh` config to only allow key-based logins (as is best security practices) based on this setup.

### Openssh

Of course, the `ssh` daemon would have to allow logins. If it is not running, it needs to be started. Also, tweak the firewall to allow port 22 for `ssh` logins.

It looks like the installation of `openssh-server` processes `ufw` hooks to allow ssh access. It also automatically starts and enables the `sshd` `systemd` service.

## Deck Setup

### Manual

Starting from the base requirement, we must make sure that we can log in to the deck from the server. If we open the ssh tunnel from the deck we can just:

```bash
$ ssh <remote server>
$ ssh admin@127.0.0.1
```

We need `127.0.0.1`, as localhost isn't very helpful - usually because of `IPV6`.

### Automated

We can use `ProxyJump` in the `~/.ssh/config` file in order to set up an automated double-jump. First, we have to `ssh` into the remote server. Then, from there, we `ssh` into the localhost (on the remote server) on the port that we need. We can set this up automatically via:

```
Host damsel
    Hostname 127.0.0.1
    User admin
    Port <port>
    ProxyJump <remote server>
```
