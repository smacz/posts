---
layout: post
title: "Usability Addons for Iceweasel"
date: 2015-03-19 
category: Battlestation
program: Firefox
process: Config
description: Iceweasel gets a makeover thanks to some well-placed addons.
---


## What is a browser, *really*?

So browsers are INCREDIBLY insecure. I like to view them as a portal to the internet, but not as, say, ssh or wget is. No, this is a portal that has to anticipate all use-cases, and basically handle anything that any web-dev has to throw at it. And trust me, they have a shit ton of weapons in their arsenal. Not only does it need to render everything flawlessly, it has to do it securely and quickly. Now, in all practicality, no tool exists that is perfectly flexible, secure, and fast; it's always a compromise between those three factors. You, however, can help Iceweasel by making a few small sacrifices, something a mature person should have no problem doing. And in doing so, you'll secure yourself, you'll secure the web, and you'll promote responsibility world-wide. Think I'm overexaggerating? Check out how much revenue comes out of web advertising and scamming every MONTH. Even the playing field with your knowledge.

## Peace-of-Mind vs Ease-of-Use

These two may seem to always be at odds with each other, and to a point it's reminiscent of the ongoing struggle of cryptographers vs cryptoanalysts, never seeming to end in this cat and mouse game, but they can occasionally play nice with each other. It's like I said, it taks a little sacrifice. But there are ways to work around it, and the time invested upfront will lead to a mutually beneficial compromise between security and user friendliness. Keep that in mind while I outline these following ideas.

Keep in mind, before (but mainly after) installing/settings these addons/configs/whatever-this-morphs-into, *Read The Furnished Material*. There may be functionality out the wazoo that you're not aware about because you were too excited to get started searching for the latest celeb gossip. Don't be that person. RTFM.

## Extra Credit

Read through the 13 pages on [TweakGuides.com](http://tweakguides.com/Firefox_1.html)

(It is meant for Windoze users, but is applicable in most respects to Linux users also. Well, page 4 and beyond anyways.)

## Add-ons Manager Context Menu

This is a peasly little script that adds exactly what I would have expected mozilla to add. This is absolutely nothing out of the ordinary, and nothing that a competant hacker couldn't put together in a weekend or two. And that is exactly what competant hacker Zulkarnain K. did. This does nothing but round out the addon screen and give you full control over your software that's managing your addons. 

In all seriousness though, this is one addon to rule them all. Ok maybe not all seriousness, but this is something that even Micro$oft could've dreamed up. I fully expect this functionality to be integrated in the future. But in true FOSS spirit I'd advise to try this out and beta-test it so that what goes into the product will be fully mature. And indeed it seems to already be near if not at that point. 

## DuckDuckGo and NoScript / DuckDuckGo Lite

DDG is definitely the search engine that I default to, as it has a wide variety of uses and customizations. There's one thing that I like most about it though.  It doesn't track me. Sweet. So I use it mainly for that reason, everything else is just a perk. 

Unfortunately DDG uses Javascript to render results, and that doesn't work so well with NoScript. You can do one of two things. First off you can whitelist DDG and enjoy the full functionality. That's in all reality not a bad option as it's a (rare) trustworthy site (for the time being). Secondly, you have the option of installing DDG Lite which does not use Javascript to render results, and is my preferred way of accessing the search. 

### !Bang

If, for some morbid reason, you feel the need to search with another search engine (like a bing or youtube, or one of those non-privacy-respecting content-censoring big-brother-ing providers) there's a good chance that it's supported through DDG. Simply type in a `!` before your search query and it will search that provider for you. Try it out with youtube. You'll see your search results in youtube, and see the search query that was passed to youtube, and even be rerouted to youtube, but the search didn't track you because it had been sent not by your browser, but by DDG. That's what they do for you.

## Vimperator

Ok, So I've installed Iceweasel and have been walking you through the basics of security, but to be honest, I've missed my favorite plugin of them all so much that I have to put it in here, so I can write it up and move on. 

**At any time after installing Vimperator you get stuck, you can always type** `:help` **on the command line. Also** `Shift`+`Esc` **toggles the operabiliy of the Vimperator keybindings (turns them off if/when needed). That is all.**

Those of you who have ever fired up linux and used a text editor have probably heard of Vim. Not because you fired up a text editor, but because somewhere along the lines of finding out how to use it have come across one flame war after another about text editor preferences. For my part, I prefer Vim, and it suits me just fine. If you're not a fan, then feel free to skip over this section. If you don't know what Vim is, or you can't live without it, I strongly suggest that you keep on reading. If you're on the fence, stop right now, install it, read the manual, and try it out yourself, that's the only way to learn and decide if it's right for you.

Still with me? Ok, let's tackle this. The original vi was a modal editor that evolved into Vim (VI iMproved) later on down the line (like back in the 80's or 90's. Whatever, I'm not a history buff.) It gained a MASSIVE following and is one of the most versitile and renown editors in existance to this day. So when browsers came along, people wanted to know how to navigate around them. Since browsers are mainly text (or at least initially were) modeling the browsing experience after a text editor seemed natural. It turned out to be incredibly difficult, but Vimperator is now in version 3.2, and going strong. 

Vimperator, in short, aims to get rid of dependence on the mouse entirely, and facilitate navigation of webpages based on usage of the keyboard alone. Remember when I said that it was a modal editor? That means that some (most) keys have a capacity as command key (j=scroll down, k=scroll up) which can vary based on capitalization and context (usage with other commands), be combined with function keys (Ctrl, Alt, Shift), or even be used to spell out commands. Using Vim and Vimperator is more along the lines of "talking" to your computer instead of "indicating" to your computer.

With this addon, more than any other, it is imperative to RTFM. But, here's an unbelievably brief summarization.

### Moving

Just like vim. I'd tell you to RTFM, but that's why you're here, isn't it.

Here are the basics:

* `j` = down
* `k` = up
* `Ctrl - d` = more down
* `Ctrl - u` = more up
* `Ctrl - f` = most down
* `Ctrl - b` = most up
* `d` = delete tab
* `u` = undo delete tab
* `gt` = go +1 tab
* `gT` = go -1 tab
* `+` = zoom in
* `-` = zoom out

...and much, much more. But that'll get you started. And if something isn't responding, spam the `Esc` button for a few seconds and try again.

### Following Links

To follow links, you have two options. Do you want to open in in this same tab? Or do you want to open it in a new tab? `F` or `f` will give you hints as to what is follow-able. (Get it? 'F' for follow? Huh? Huh? Anybody? Bueller?  ...anyways) Anything that's follow-able will have a number next to it. Hit that number and it will be opened whichever way you chose to open it. 

But that's not all. If you have something specific that you want to follow, say a link that says, "Kim Kardashian"; then start typing in "k-i-m..." and watch the options narrow down. You can either type enough so that your intended link is the only option and then follow that, or you can narrow it down just enough so that you're not typing in the number `157` to select that link, and instead are typing `2` + `Enter`.

### MaxItems

```
:set maxitems=8
```

You don't want more than 15. It's too much after that.

### Search Engines

This is quite possibly my favorite feature. Not only searching the web from the command line, but returning live results, sometimes from specific websites.  First, click the down arrow by the search bar underneath your tabs, and select "Manage Search Engines" and then on "Get more search engines". Go ham. My must-haves are:

* [Wikipedia](https://addons.mozilla.org/en-US/firefox/addon/wikipedia-ssl-243981/)
* [Arch Linux Wiki Search](https://addons.mizilla.org/en-US/firefox/addon/archlinux-wiki/)
* [Thesaurus](https://addons.mozilla.org/en-US/firefox/addon/thesaurus-referencecom/)
* [Dictionary](https://addons.mozilla.org/en-US/firefox/addon/wiktionary-en/)
* [DuckDuckGo Lite](https://addons.mozilla.org/en-US/firefox/addon/duckduckgo-lite/)
* [The Pirate Bay](https://addons.mozilla.org/en-US/firefox/addon/the-pirate-bay-ssl/)
* [Wolfram\|Alpha](https://addons.mozilla.org/en-US/forefox/addon/wolframalpha/)
* [Open Street Map](https://addons.mozilla.org/en-US/firefox/addon/openstreetmap-1)
* [PDF Files](https://addons.mozilla.org/en-US/firefox/addon/pdf-files/)
* [Stack Overflow](https://addons.mozilla.org/en-US/firefox/addon/stack-overflow)
* Torrents - Choose your own...or several

Once these are in place, open up the search engine management dialog again and assign them all keywords. I prefer 3-4 letter abbreviations. Click "OK" and wait for the program to write your changes to the file `.mozilla/firefox/xxxxxxxx.default/searchplugins/*.xml` and then go back to the browser. Hit `o` like you would be opening up a webpage, and type in a keyword of a search engine you want to try out, and then a query and then hit "Enter".  For example to search for "the roxy los angeles, ca" in wikipedia:

```
wiki the roxy 
```

Or in openstreetmap:

```
omap the roxy london
```

Depending on which search engine you're using, you can most likely recieve live search results in the completion window. This is far and away the best way to search the internet. 

To set a search (for me DDG Lite) that pops up if no other search engine is specified (a *default* if you will) issue the command `set defsearch=[TAB]` and hitting that `tab` button there will show you your options from the providers that you have installed. Choose one and have fun!

In order to control what is shown in your Completions options when you are in the middle of typing something out, issue the command `:set complete=Ss`. RTFM if you wanna find out what that does.

### Hide close buttons on tabs

If you're using Vimperator, there's really no reason to use the close buttons on the tabs as `d` works just fine. Now, there's no repercussions to not removing them, but it does look clean if there's only the tab there, and you can make them disappear magically. If you want, you can remove the buttons by passing a value of 2 to `browser.tabs.closeButtons` in `about:config` if you are running with Firefox31 or earlier. After that, an addon restore the behavior of value=2 is called No Close Buttons. 

### Other options

Those are the settings that I can't live without. But I **strongly suggest** that you read the pages at `:help` and set your customizations appropriately.  Don't forget to close all your instances of firefox and then reopen them ("restarting" firefox) if you have made any changes, just to make sure that they have been saved. Otherwise, you may find out that an hour's worth of work wasn't saved and get super pissed off. Not my fault. Check the implementation before expecting the functionality.

## RES

Reddit Enhansement Suite is a *must* if you're going to peruse reddit on firefox. It allows you to preview images and posts right from the front page, tag other peoples' accounts in your browser, and basically win reddit.  In order to "win reddit" though you have to solve a couple interoperability issues first. Namely with NoScript, color config, and Vimperator.

These are trivial at best though. With NoScript, make sure to whitelist `https://www.reddit.com` and `https://www.redditstatic.com`. And Vimperator is even less of a problem. I actually use Vimperator with RES and enable both at the same time. RES however has it's own key navigation that is much better that Vimperator's `{F|f}` functionality. Disable Vimperator's keybindings if you want to use them though, otherwise you may accidently order yourself a russian bride.  You've been warned. Also, you can fine-tune the keybindings themself and even make the vim-esque. Just do whatever suits your taste.

Lastly, if you choose to have a custom color config or you're using system colors to render pages, the upvotes just flat out won't be shown. That's where Vimperator or the RES keybindings come into play though. Use them to vote, and try *really* hard to remember if you upvoted that last cat video or not, cause I'll tell you what, you won't be able to see the orange arrow unless you disable custom color. (But if you do, RES does let you choose a "Dark mode" up in the top-right corner under the gear icon)

## Quickmarks

Feel like Firefox gyps you out of a good bookmark manager? Well, suffer no longer as Quickmarks is here! No really, it's great. Once again RTFM, but it allows you to create folders on the fly, store bookmarks in two clicks or less, and gives you a decent access interface to boot. 

The only caveat is that it doesn't work *great* with Vimperator, but that's something that the Vimperator devs have been dragging their feet on. I'll get there soon, but for now, Quickmarks is only a click away.

## Others

There are a few addons that I have not found a distinct need, or at least a different method to achieve similar results for, but may be worth taking a look at to see if they fill a need that you have:

* Reader -- Format webpages for easy reading including autoscroll
* NoSquint -- Recolor and Resize webpages
* Tab Mix Plus -- Tab Managment (extrordinare!) 
* Flagfox -- Displays server locale information and other useful location tools
* Print Pages to PDF && Web2PDF: wrapper for [wkhtml2pdf](http://github.com/NitMeeedia/wkhtml2pdf)
* Youtube Video and Audio Downloader && Download Youtube Videos as MP4 --
* wrapper for [youtube-dl](http://rg3.github.io/youtube-dl)
* Tineye Reverse Image Search -- Right click option on images: (!Bang available)
* ChatZilla -- IRC browser client: [Hexchat](http://hexchat.github.io)
* RSS Feed Icon in Navbar -- RSS Feed Aggregator: [Newsbeuter](http://Newsbeuter.org)
* Free Memory -- It would be reviewed if it could be downloaded (no "Download" button present)
* PDF Viewer -- Built-in functionality. Also: [zathura](http://pwmt.org/projects/zathura/)

## That about wraps it up

Don't forget there are four parts to an iceweasel setup, and this is only one of them. The other three are crucial as well in order to get a reliable, well-functioning web browser up and running. 
