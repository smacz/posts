---
layout: post
title: "Virtual VLANs"
date: 2018-05-31
category: SysAdmin
program: pfSense
process: Install
description: "This is as simple as complex networking gets."
references:
  - title: "http://www.practicalnetworking.net/stand-alone/routing-between-vlans/"
    link: 'http://www.practicalnetworking.net/stand-alone/routing-between-vlans/'
  - title: "Configuring VLANs on pfSense"
    link: 'https://www.highlnk.com/2014/06/configuring-vlans-on-pfsense/'
  - title: "Using VLANs with OVS and libvirt"
    link: 'https://blog.scottlowe.org/2012/11/07/using-vlans-with-ovs-and-libvirt/'
  - title: "Multi-tenant/VLANs behind a virtualized pfSense firewall"
    link: 'http://www.jonkensy.com/multi-tenantvlans-behind-a-virtualized-pfsense-firewall-in-esxi/'
  - title: "Open vSwitch Cheat Sheet"
    link: 'http://therandomsecurityguy.com/openvswitch-cheat-sheet/'
  - title: "Cluster Networking for Multi-tenant isolation in Proxmox with OpenVSwitch"
    link: 'https://icicimov.github.io/blog/virtualization/Cluster-Networking-for-Multi-tenant-isolation-in-Proxmox-with-OpenVSwitch/'
  - title: "Configure Open vSwitch Tunnels"
    link: 'https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux_OpenStack_Platform/4/html/Installation_and_Configuration_Guide/Configuring_Open_vSwitch_tunnels.html'
  - title: "Configuring VXLAN an dGRE Tunnels on OpenvSwitch"
    link: 'http://networkstatic.net/configuring-vxlan-and-gre-tunnels-on-openvswitch/'
  - title: "Proxmox Wiki: Open VSwitch"
    link: 'https://pve.proxmox.com/wiki/Open_vSwitch#Installation'
  - title: "Firewall Deployment for Multitier Applications"
    link: 'https://zeltser.com/firewalls-for-multitier-applications/'
  - title: "Vendor lock-in fears"
    link: 'https://dnp94fjvlna2x.cloudfront.net/wp-content/uploads/2017/09/Screen-Shot-2017-09-15-at-9.52.00-AM.png'
  - title: "Deploy OpenFaaS and Kubernetes on DigitalOcean with Ansible"
    link: 'https://www.openfaas.com/blog/deploy-digitalocean-ansible/'
---

## Why did I do this?

1. Adding/removing NICs from a FreeBSD VM (pfSense) will require the VM be rebooted for the changes to take effect.  The “sub-interface” VLAN method is much more production-friendly.
2. OVS handles VLANs where Linux Bridges do not
3. OVS handles VXLAN encapsulation where Linux Bridges do not
4. I should be able to fail over to a single piece of hardware completely redundantly.
5. I should be able to deploy only a single piece of hardware in the exact same configuration that I would set up a 3+ piece cluster.
6. Because immutable infrastructure is the _shit_ and scales with an acceptable ratio for federated services.
7. Because Cloud-in-a-ProxMox-Box sounds pretty darn cool.
8. Because this can elastically scale _out_
9. Because it should be able to federate AAA


# Quotes

For corporate IT leaders, it’s practically impossible to deliver best-in-class IT solutions across the enterprise without a multi-cloud strategy,” Murr explains. “However, this isn’t the case for customer-facing products, which quite often ride atop a single IaaS or PaaS provider


