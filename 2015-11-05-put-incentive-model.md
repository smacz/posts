---
layout: post
title: "Put Incentive Model"
date: 2015-11-05
category: SysAdmin
program: MaidSafe
process: Metacognition
description: A rationale for the Put Incentive Model
---

# Summary

The PUT Incentive Model is designed to solve many problems, and most recently many have come to the surface. Issues regarding:

* APP reward mechanism
* Pay the Producer
* Network Bankrupsy via Free GETs
* Bug Bounty program

are all affected in some way by this proposal. This is intended to set the stage for a network wherein all who participate in their various functions (Farmer, APP Dev, Content Creator, End-User) will have an opportunity to benefit from the network.

The PUT Incentive Model notion was previously introduced by @bcndanos [here](https://forum.safenetwork.io/t/safe-pod-sf-safecoin-the-safe-ecosystem/2938/37) and has since been floating around in the forums, while the plan was still to reward APP developers by GETs. A simpler proposal can be found [here](https://forum.safenetwork.io/t/proposal-app-rewards-by-pay-per-put-commission/5812) by @polpolrene and @DavidMtl. This plan does not set out to change the fact that APP developers be paid, but the way that the network does so and how APP developers can use these funds to help cultivate a community both around their individual APPs, as well as the SAFE Network in general.

# Motivation

This proposal uses the amount that APPs PUT data onto the network (on behalf of the user) to determine how much they will be rewarded by the network.

There have been many discussions about rewarding APP developers with GETs, and how that system can be gamed without End-Users really noticing. Without End-Users really *feeling the impact* of how these malicious APPs could cheat the system, they will not be motivated in any visceral way to dissuade this type of behavior until it is too late and the SAFE Network fails because of these greedy developers.

By rewarding per PUT, if a malicious app developer were to institute a policy wherein an APP were to make more PUTs than was necessary, then the initial funds to facilitate these extraneous PUTs would necessarily come out of the wallets of the End-Users. This is easily recognizable, and we can rely on human nature, specifically greed, to ensure that these types of APPs, no matter the quality of their functionality, will not prosper.

Also, PUTting recycles Safecoin. This enables the circulation of the lifeblood of the network and is to be encouraged (see: [Free GETs aren't going to fly](https://forum.safenetwork.io/t/free-gets-arent-going-to-fly/5836/54)) This adds value to the network both in the form of currency, and of general information. (The argument is that the more people have access to more information the more opportunities arise for more people).

Once an APP uploads to the network *on the behalf of the user* - it will initiate an action similar to a farming attempt - and either recieve a reward (sent to the App Wallet - see below) or not based on the outcome of that attempt.

For the argument that "Not all APPs need to PUT" see the "Drawbacks" section.

# Detailed design

To preface this section, there will be no concrete detailed network design as of yet, this is simply a Request for Comments that is based on the morality of the feature alone. (and as such is not a complete RFC, but rather one to discuss in the community).

## Network Bankruptcy

It has been put forth by @jreighley that [Free GETs aren't going to fly](https://forum.safenetwork.io/t/free-gets-arent-going-to-fly/5836). There are a couple points to the effect that they may, but many of the solutions chip away at the problem, and don't provide a paradigm shift to counteract the effect.

Assuming that the premise of and responses to the argument are understood, there still remains one big unanswered problem - GETs are free *and* can generate income. If the economics that I understand still hold, this will not work. Namely that you can't get something for nothing.

There has been much speculation (and math) done that suggest that GETs will be overwhelmingly more popular than PUTs. So the question I have is "Why would we reward that which is over-abundant, prone to gaming, and not contributing anything to the network?"

The answer is that we shouldn't. Assisting the *direct economic advanacement* of the network should be rewarded. This is a value to the network in both a technological sense, and a common one. Technologically speaking, this will balance out the cost of operating the network. From a more abstract point of view, it increases the ubiquitousness of PUTs, while making sure that APPs are designed to contribute to the collective amount of information that is on the network. This helps to both increase adoption, and disseminate information into the public's hands.

## Identification for reward purposes

There is also the problem of how to distinguish a particular APP's GETs from another. This may lead to many ideas that compromise, if not outright break the anonymity that the network works so hard to maintain. Think browser tracking via UserAgent strings. (also UserAgent spoofing...) I would assert that it would be easier to identify PUTs made from specific APPs rather than GETs.

One of the reasons being that APPs (following the premise of this proposal) will already be designed in such a way as to facilitate PUTting information on behalf of the End-User. This then can be structured to include information in - or be able derive from the context of - the data indicating which application is deserving of a reward. The result of this is that _the information need not be kept after the data is PUT onto the network_.

## APP Farming Attempt

An APP farming attempt will work the same as would a regular farming attempt. This ensures that it is not "X% of the PUT cost".

It is unknown at this time if the size of the PUT request will affect the chance of a successful farming attempt.

## APP Wallets

This proposition includes a new type of wallet. An autonomous APP Wallet that is driven solely by code, and no single user has access to all of the APPs funds (unless they code themselves to).

The implementation is intended to facilitate the handling of Payments *from* the network, and *to* devs and content creators (as defined by the devs) alike.

### Reward Mechanisms

The assumption is that the rewards will all be pooled together to eventually be split out to the devs and the content creators. This is accomplished by having the app developers code the wallet in the way that they see fit - having rewards to to either the devs, the content creators, or both in some proportional manner. The hope is that this will incentivize community building as well as development contributions in the race for a bigger rewards pool per APP.

#### To the Devs

The devs must have an autonomous way to divvy up the percentage of the APP Wallet that is allotted to them. Either that, or have it go towards any PUT costs that are associated with maintaining or expanding the codebase. This could be prepaid space for a git-like VCS for comments and commits.

#### To the Content Creators

> @DavidMtl
>
> Instead of thinking that we fixed the producer problem by rewarding uploaders with the Get reward, I think it would be much much more productive to develop platform that will help them get real money like: Shop plugins, crowdfunding, patreonage, subscriptions, gift[ing/ tipping], etc.

There is no mandate in this proposal that says that content creators need to be given a reward oportunity - and neither should there be. However, this proposal facilitates, even *encourages* such a system to be commonplace in the scenarios where it is appropriate.

## "Free Demo" type deal

Specifically speaking about configuration and saving data pertaining to APPs, this method allows an APP to be "demo'd" before a user decides to use this APP to PUT their data. That data could be a recoloring of the APP skin, to personal preferences when it comes to music selection. Whatever it is to customize the experience, that costs a PUT and will reward the APP. Before that happens though, the user will have a full demo of the APPs functionality before having to decide to invest in customizing it.

This does not necessarily pertain to the content that the APP is able to serve, but rather the functionality of the APP itself.


# Drawbacks

1) Not all APPs need to PUT

No, they don't. The ones that don't, however, aren't generating revenue for the network, and therefore the network shouldn't take on the burden of subsidizing those APPs.

So what can the devs do for APPs that don't PUT? Well, there is always the payment directly from the users to the APP Wallet. (such as micro-payments or subscription services) There are also more complex business models that are being employed today in the FOSS industry. These are still valid forms of monetization.

Also, keep in mind that not all PUTs have to be in the form of public content. PUTs can be initiated for configuration files, or messages. There are all types of PUTs, and different APPs will utilize space on the network differently.

2) Lurkers make up 90% of the existing Internet

And they are not contributing therefore need not be rewarded, or reward those who facilitate them although they are certainly free to lurk. When they decide to contribute they can do so in the most advantageous way possible.

3) I'll just create an APP to PUT everything I want to.

If you're just talking about PUTting your CD collection up and getting rewarded for it, hell, I'll probably be the first one to do that. Seems like a good idea to me. And if it's good enough to compete with LiteStuff (or other spin-offs) by all means go for it! (a little marketing goes a long way)

But are you going to create an interface for every APP that you use? Not likely. Are End-Users going to be using it too? Not if it's bad. What if it's good? Then either you got yourself your own killer APP, or you merge into the bigger APP (whose funds you started siphoning anyways) and the combination of the two services can provide more revenue than either could alone. (if they split up the community and therefore the reward pool)

4) This will encourage high-PUT APPs

What number of PUTs is high? How many are necessary? These questions and more can only be figured out with the natural evolution of the network. If an APP of the same quality and compatability with a popular APP shows up and does the same actions with less PUT costs, it stands to reason that the users will use that APP.

However, if that APP is limited by the amount of features, where the more expensive APP has more features, some may choose to use the fully-featured APP that's more expensive. It's a free market, baby!

5) Content Copying

This proposal does not aim to solve the problem of copying content by slightly amending to or altering the file. As such it is still vulnerable to any exploitation of copied content that PtP would be. This is still an issue that needs to be solved. (Specifically both Watermarking and Proof of Unique Human require further investigation for a solution to be found).

For the time being, I would assert that this would be most effectively done outside of this App reward implementation, and as such is beyond the scope of this document.

6) What if I want a different APP to be rewarded for my PUT after I upload it?

Well, that's the thing about content, once it's there it's there (unless it's copied - see above). If another PUT would have to be issued to put it on the network under a separate APP, then that second APP *would* recieve credit for PUTting that piece of data onto the network.

But then the content creator just uploaded the same file twice to the network - not gaining any security/popularity, having the data de-duplicated anyways, and above all paying for it - or worse, not having it uploaded at all due to de-duplication and not rewarding the second App. The entire premise here is quite daft considering that an alternative would be to instead just tip the APP directly (as there is a valid wallet address for it to be sent to) or the APP devs directly.

Keep in mind though, content can be rewarded by any way that is made possible by the system. The content itself is APP-independent. This is exemplified in the ability for the information on the APP that submitted the PUT on behalf to be destroyed once the data is PUT and the APP Wallet credited.

7) Just like vault farming attempts, the code that initiates this farming attempt is under the control of the end-user and is subject to be tampered with providing a better chance at a reward, or a larger reward. This can be mitigated by integrating nodes into the equation to verify the attempt.

# Unresolved Questions

1. Will the size of the PUT request affect the chance and/or reward amount of a successful farming attempt?
2. Pseudo-code needs to be written.
3. Can we call it something other than a farming attempt?



# Alternatives

See @dyamanaka's OP [here](https://forum.safenetwork.io/t/app-developer-rewards-discussion/3045)

* GET incentive model
* GET & PUT incentive model

The alternatives below (also mentioned in the above link) are not necessarily mutally exclusive to the proposed system.

* One-time payment model
* Subscription Payment
* Reoccurring micropayment
