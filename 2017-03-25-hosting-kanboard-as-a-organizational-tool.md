---
layout: post
title: "Hosting Kanboard as a organizational tool"
date: 2017-03-25
category: HomeLab
program: Kanboard
process: Install
repo: 
description: "Kanboard is a tool to facilitate organization using the Kanban methodology. It is very extensible and I think it will work out just fine for not just my own purposes..."
image: kanboard-front.png
references:
  - title: HostPresto - How to install Kanboard on CentOS7 with Mariadb
    link: https://hostpresto.com/community/tutorials/how-to-install-kanboard-on-centos-7/
  - title: Vultr - How to install Kanboard on CentOS7 with Mariadb
    link: https://www.vultr.com/docs/how-to-install-kanboard-on-centos-7
  - title: PHP SAML Library
    link: https://github.com/onelogin/php-saml
  - title: Install Shibboleth IDP
    link: https://spaces.internet2.edu/pages/viewpage.action?pageId=49841792
---

## Conditional variables

The correct way to set variables, in this case `kanboard_mysql`, is to make it `true`. This way, statements like `when` and `if` can be evaluated solely using the boolean syntax without having to do string comparison.

# Shib SP
https://wiki.shibboleth.net/confluence/display/SHIB2/NativeSPLinuxRPMInstall

http://download.opensuse.org/repositories/security://shibboleth/CentOS_7/

## Yum

```
[security_shibboleth]
name=Shibboleth (CentOS_7)
type=rpm-md
baseurl=http://download.opensuse.org/repositories/security:/shibboleth/CentOS_7/
gpgcheck=1
gpgkey=http://download.opensuse.org/repositories/security:/shibboleth/CentOS_7//repodata/repomd.xml.key
enabled=1
```

## Apache

```
LoadModule mod_shib /usr/lib64/shibboleth/mod_shib_24.so
UseCanonicalName On
```

`/etc/httpd/conf.d/shib.conf`

https://wiki.shibboleth.net/confluence/display/SHIB2/NativeSPApacheConfig

```
2017-04-10 11:53:41 WARN Shibboleth.Application : insecure cookieProps setting, set to "https" for SSL/TLS-only usage
2017-04-10 11:53:41 WARN Shibboleth.Application : handlerSSL should be enabled for SSL/TLS-enabled web sites
2017-04-10 11:53:41 WARN Shibboleth.Application : no MetadataProvider available, configure at least one for standard SSO usage
```

## Systemd

```
systemctl enable shibd
systemctl start shibd
```

# Shib IDP

```
# yum install java-1.7.0-openjdk.x86_64 net-tools unzip wget
```

Install tomcat to `/opt/tomcat`.

#### Figure out a better mirror

https://tomcat.apache.org/download-80.cgi

```
wget http://mirror.nexcess.net/apache/tomcat/tomcat-8/v8.0.43/bin/apache-tomcat-8.0.43.tar.gz -O /opt/tomcat
```

```/etc/profile.d/script.sh
#!/bin/bash
CATALINA_HOME=/opt/tomcat
PATH=$CATALINA_HOME/bin:$PATH
CATALINA_OPTS="-XX:+UseG1GC -Xmx1500m:-XX MaxPermSize=128m"
export PATH CATALINA_HOME
export CLASSPATH=.
export CATALINA_OPTS
```

```
chmod +x /etc/profile.d/script.sh
ource /etc/profile.d/script.sh
chmod +x $CATALINA_HOME/bin/startup.sh
chmod +x $CATALINA_HOME/bin/shutdown.sh
chmod +x $CATALINA_HOME/bin/catalina.sh
```

```/usr/local/bin/restart-tomcat
#!/bin/bash

echo "---- Shutting down Tomcat -----"
/opt/tomcat/bin/shutdown.sh && echo "---- Tomcat shut down -----"
echo "---- Starting up Tomcat -----"
/opt/tomcat/bin/startup.sh && echo "---- Tomcat started up -----"
```

Check if it's on:

```root
# ss -l4tn
```

If not, try turning off `firewalld`
```root
systemctl stop firewalld
```

### Making IDP work with tomcat initially

```$CATALINA_BASE/conf/Catalina/localhost/idp.xml
<Context docBase="/opt/shibboleth-idp/war"
    privileged="true"
    antiResourceLocking="false"
    swallowOutput="true">

   <!-- Work around lack of Max-Age support in IE/Edge -->
   <CookieProcessor alwaysAddExpires="true" />
</Context> 
```

Remove `appBase` from `/opt/tomcat/conf/server.xml`

```/etc/systemd/system/tomcat.service
# Systemd unit file for tomcat
[Unit]
Description=Apache Tomcat Web Application Container
After=syslog.target network.target

[Service]
Type=forking

Environment=JAVA_HOME=/usr/lib/jvm/jre
Environment=CATALINA_PID=/opt/tomcat/temp/tomcat.pid
Environment=CATALINA_HOME=/opt/tomcat
Environment=CATALINA_BASE=/opt/tomcat
Environment='CATALINA_OPTS=-Xms512M -Xmx1024M -server -XX:+UseParallelGC'
Environment='JAVA_OPTS=-Djava.awt.headless=true -Djava.security.egd=file:/dev/./urandom'

ExecStart=/opt/tomcat/bin/startup.sh
ExecStop=/bin/kill -15 $MAINPID

User=tomcat
Group=tomcat
UMask=0007
RestartSec=10
Restart=always

[Install]
WantedBy=multi-user.target
```

https://shibboleth.net/downloads/identity-provider/latest/

https://www.howtoforge.com/how-to-install-tomcat-on-centos-7

https://wiki.shibboleth.net/confluence/display/IDP30/Installation

https://wiki.shibboleth.net/confluence/display/IDP30/ApacheTomcat8

https://wiki.shibboleth.net/confluence/display/SHIB2/IdPApacheTomcatPrepare

https://stackoverflow.com/questions/3914888/deploy-application-on-custom-host-of-tomcat

https://wiki.shibboleth.net/confluence/display/IDP30/Configuration

https://wiki.shibboleth.net/confluence/display/IDP30/MetadataConfiguration

https://wiki.shibboleth.net/confluence/display/IDP30/AuthenticationConfiguration

https://wiki.shibboleth.net/confluence/display/IDP30/AttributeResolverConfiguration

https://wiki.shibboleth.net/confluence/display/IDP30/AttributeFilterConfiguration

https://www.digitalocean.com/community/tutorials/how-to-install-apache-tomcat-8-on-centos-7
