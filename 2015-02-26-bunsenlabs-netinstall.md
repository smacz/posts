---
layout: post
title: "BunsenLabs Netinstall"
date: 2015-02-26
category: Battlestation
program: BunsenLabs
process: Install
description: Building up a base install from a Jessie netinstall, the command line, and the BunsenLabs github repos.
---

## Start the Install

Nothing crazy. Set the time and place. Guided - Use entire disk. (Or if you're
dual booting like me, set it up manually, but then you would already know to do
that, right?) Set up separate `/home` partition. Deselect everything except 
basic utils.

Leave the root password blank if you want to have sudo powers for your user.

This is assuming your install is in a VM or via ethernet for the time being. 
Not saying wireless is impossible to have right from the get go...just highly 
improbable.

(Revised) Copy-Pasta of cheat sheet from johnraff [here](crunchbang.org/forums/viewtopic.php?pid=418637#p418637):


## PART 1 "The part formerly known as Bunsen-Core" 

CHEAT SHEET 
This is not a script! There's no error checking at all. 
If you try to run all these commands in one go you'll very likely hit a problem 
half way through and get into a nasty mess. Run them one at a time, and make 
sure things are OK before moving on to the next. 

OK (starting from a bare Debian Jessie netinstall):

``` bash
	sudo apt-get update 
	sudo apt-get dist-upgrade 
	## optional for tracking bugs in testing or unstable: 
	sudo apt-get install apt-listbugs 

	# Add contrib non-free to sources.list ...one LOOONG command
	sudo mv /etc/apt/sources.list{,-orig} sed -r 's/(^deb [^ ]*)( jessie | jessie-updates | jessie\/updates | jessie-backports ).*$/\1\2 main contrib non-free/g' /etc/apt/sources.list-orig | sudo tee /etc/apt/sources.list >/dev/null 

```

	# alternate: 
	sudo vim /etc/apt/sources.list 
	# and add contrib non-free to each deb line 


	### System tools 
	sudo apt-get install bash-completion lintian libnss-mdns xdg-utils 
		rsync anacron usbutils wmctrl menu bc whois rpl cpufrequtils 
		debconf-utils apt-xapian-index build-essential user-setup 
		avahi-utils avahi-daemon ftp sshfs cowsay figlet curl 

		# alternate install: screen
		sudo apt-get install tmux 

		# alternate install metapackage: ssh
		sudo apt-get install openssh-client 
		

	### Filesystem tools 
	sudo apt-get install e2fsprogs xfsprogs reiserfsprogs reiser4progs jfsutils 
		ntfs-3g fuse gvfs gvfs-fuse gvfs-bin gvfs-backends fusesmb 


	** I had to 'sudo apt-get update'  here for unrar to install below **

	### Compression utilities 
	sudo apt-get install unrar unace unalz unzip lzop rzip zip arj 
	# Already the newest version: bzip2 xz-utils 
	

	### Core desktop applications:
	sudo apt-get install --no-install-recommends openbox obconf thunar 
		thunar-volman thunar-archive-plugin gksu thunar-media-tags-plugin 
		lightdm tint2 gmrun htop conky-all gparted 

		# alternate install: terminator
		sudo apt-get install --no-install-recommends xfce4-terminal

		# alternate install: gedit gvim emacs leafpad
		sudo apt-get install --no-install-recommends geany geany-plugins 

		# alternate install: mpv mplayer2
		sudo apt-get install vlc vlc-plugin-notify

		# alternate install: deluge rtorrent
		sudo apt-get install --no-install-recommends transmission-gtk

		# alternate install: zathura foxit okular
		sudo apt-get install --no-install-recommends evince

		# alternate install: xchat irssi weechat bitlbee 
		sudo apt-get install --no-install-recommends hexchat

		# alternate install: empathy telepathy
		sudo apt-get install --no-install-recommends pidgin

		# alternate install: brasero unetbootin
		sudo apt-get install xfburn

		# alternate install: qiv gimageview gpicview gqview
		sudo apt-get install viewnior 

		# alternate install: shotwell digiKam(Qt)
		sudo apt-get install gimp gimp-plugin-registry

		# alternate install: feh
		sudo apt-get install --no-install-recommends nitrogen 

	## My *necessary* additions:
	sudo apt-get install --no-install-recommends git vim youtube-dl

		# alternate install: mutt
		sudo apt-get install --no-install-recommends icedove

		# alternate install: dillo w3m chromium firefox
		sudo apt-get install --no-install-recommends iceweasel

		# alternate install: mencoder handbrake
		sudo apt-get install: --no-install-recommends devede 


	### Desktop Utilities
	sudo apt-get install --no-install-recommends lxappearance xfce4-notifyd 
		libnotify-bin xinput xsel xdotool fbxkb scrot desktop-base clipit 
		python-xdg gnome-keyring libpam-gnome-keyring xfce4-screenshooter ntp 
		gdebi imagemagick lua5.2 lua-posix lua-socket lua-penlight hsetroot 
		suckless-tools compton policykit-1 gtrayicon tumbler 

		# alternate install: i3lock or xscreensaver
		sudo apt-get install --no-install-recommends light-locker

		# alternate install: mate-power-manager fdpowermon
		sudo apt-get install --no-install-recommends xfce4-power-manager 
			xfce4-power-manager-plugins 

		# alternate install: lightspark(testing)
		sudo apt-get install --no-install-recommends flashplugin-nonfree


	### GTK utilities 
	sudo apt-get install --no-install-recommends gnumeric galculator 
		gigolo catfish gsimplecal synaptic ghostscript zenity arandr 

		# alternate install: xarchiver
		sudo apt-get install --no-install-recommends file-roller

	# Configure gksu
	sudo update-alternatives --set libgksu-gconf-defaults /usr/share/libgksu/debian/gconf-defaults.libgksu-sudo 

	sudo update-gconf-defaults 
	

	### Graphics 
	sudo apt-get install --no-install-recommends fonts-dejavu fonts-droid 
		fonts-freefont-ttf fonts-liberation ttf-mscorefonts-installer 
		dmz-cursor-theme gtk2-engines-murrine gtk2-engines-pixbuf gtk2-engines 


	### Sound 
	sudo apt-get install alsa-base alsa-utils lame xfce4-volumed 
		volumeicon-alsa alsa-tools alsa-tools-gui alsa-oss alsamixergui
		libalsaplayer0 gstreamer0.10-alsa

		# alternate install: pavucontrol
		sudo apt-get install --no-install-recommends volti

		# alternate install: xfce4-mixer
		sudo apt-get install alsamixergui


	### Network 
	sudo apt-get install firmware-linux firmware-iwlwifi firmware-ralink 
		firmware-ipw2x00 firmware-realtek intel-microcode 
		amd64-microcode 
	# already installed: wireless-tools
	
		# alternate install: wicd wicd-curses
		sudo apt-get install --no-install-recommends 
			network-manager-{gnome,openvpn-gnome,pptp-gnome,vpnc-gnome} 


	### Get/Start X11
	sudo apt-get install xorg 

	sudo apt-get update

	startx 
	
	sudo apt-get update && sudo apt-get dist-upgrade
	# Try right-clicking...
```


### Part 2 -- Configure from Github
	
	
	# Using BunsenLabs github to theme and configure
	mkdir BunsenLabs && cd BunsenLabs

	# bash command to install all repos
	repos="bunsen-configs bunsen-exit bunsen-pipemenus bunsen-user-setup
		bunsen-images bunsen-faenza-icon-theme bunsen-conky bunsen-welcome
		bunsen-common bunsen-lock bunsen-wmhacks deb8-theme bunsen1-dark-theme
		bunsen1-theme bunsen-docs bunsen-installscripts"; for repo in $repos; do 
		git clone git://github.com/bunsenlabs/$repo; done

	# or do it manually
	git clone https://github.com/bunsenlabs/bunsen-configs
	git clone https://github.com/bunsenlabs/bunsen-exit
	git clone https://github.com/bunsenlabs/bunsen-pipemenus
	git clone https://github.com/bunsenlabs/bunsen-user-setup
	git clone https://github.com/bunsenlabs/bunsen-images
	git clone https://github.com/bunsenlabs/bunsen-faenza-icon-theme
	git clone https://github.com/bunsenlabs/bunsen-conky
	git clone https://github.com/bunsenlabs/bunsen-welcome
	git clone https://github.com/bunsenlabs/bunsen-common
	git clone https://github.com/bunsenlabs/bunsen-lock
	git clone https://github.com/bunsenlabs/bunsen-wmhacks
	git clone https://github.com/bunsenlabs/deb8-theme
	git clone https://github.com/bunsenlabs/bunsen1-dark-theme
	git clone https://github.com/bunsenlabs/bunsen1-theme
	git clone https://github.com/bunsenlabs/bunsen-docs
	git clone https://github.com/bunsenlabs/bunsen-installscripts

	### Executables

	# Move pipemenu executables and bl-exit to /usr/bin
	#sudo cp bunsen-pipemenus/bl-* bunsen-exit/bl-exit /usr/bin/
	sudo ln -s ~/BunsenLabs/bunsen-pipemenus/bl-* 
		~/BunsenLabs/bunsen-exit/bl-exit /usr/bin
		
	# Setup /usr/lib/bunsen and move bunsen-common there
	#sudo mkdir /usr/lib/bunsen && sudo cp -r bunsen-common /usr/lib/bunsen
	sudo mkdir /usr/lib/bunsen  && sudo ln -s ~/BunsenLabs/bunsen-common 
		/usr/lib/bunsen

	# Set WM-Hacks up
	#sudo cp bunsen-wmhacks/bl-* /usr/bin
	sudo ln -s ~/BunsenLabs/bunsen-wmhacks/bl-* /usr/bin
	

	### Themes & Images

	# Move themes to /usr/share/themes
	mv bunsen1-theme/Bunsen1/* bunsen1-theme/
	#sudo cp -r bunsen1-dark-theme/ deb8-theme/ bunsen1-theme /usr/share/themes/
	sudo ln -s ~/BunsenLabs/bunsen1-dark-theme/ ~/BunsenLabs/deb8-theme/
		~/BunsenLabs/bunsen1-theme /usr/share/themes/

	# Move icons to /usr/share/icons
	tar -xzvf bunsen-faenza-icon-theme/bunsen-faenza-icon-theme.tar.gz
	#sudo cp -r bunsen-faenza-icon-theme/ /usr/share/icons/
	sudo ln -s ~/BunsenLabs/bunsen-faenza-icon-theme/{Faenza-Bunsen,Faenza-Dark-Bunsen} 
		/usr/share/icons/

	# Move wallpapers to /usr/share/wallpapers
	#sudo cp -r bunsen-images/wallpapers/* /usr/share/wallpapers
	sudo ln -s ~/BunsenLabs/bunsen-images/wallpapers/* /usr/share/wallpapers

	# Move grub/login backgrounds to /usr/share/images/desktop-base
	#sudo cp bunsen-images/svg/* bunsen-images/bl-grub/* bunsen-images/bl-login/* 
	#	/usr/share/images/desktop-base
	sudo ln -s ~/BunsenLabs/bunsen-images/svg/* ~/BunsenLabs/bunsen-images/bl-grub/*
		~/BunsenLabs/bunsen-images/bl-login/* /usr/share/images/desktop-base

	%%%%%%%% severely slowed down system 2015/4/04 %%%%%%%%%%%%%%
	# Add background image to grub
	sudo vim /etc/default/grub
		# Set the background image
		GRUB_BACKGROUND=/usr/share/wallpapers/1920x1080/hydrogen-1920x1080.png
	sudo grub-mkconfig -o /boot/grub/grub.cfg
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

	%%%%%%%%%%%%%% Not honoring icon change - 2015/4/04 %%%%%%%%
	# Setup user LightDM icon
	sudo mkdir -p /usr/share/icons/lightdm/256x256/status
	sudo cp bunsen-images/icon-avatar/Bunsenlabs-flame.png
		/usr/share/icons/lightdm/256x256/status/
	sudo cp lightdm/index.theme /usr/share/icons/lightdm/index.theme
	sudo gtk-update-icon-cache /usr/share/icons/lightdm/
	
	# Change as needed
	sudo vim /etc/lightdm/lightdm-gtk-greeter.conf
		# Try to use something in /usr/share/images/desktop-base/
		background=/usr/share/wallpapers/1920x1080/hydrogen-1920x1080.png
		theme-name=deb8-theme
		icon-theme-name=lightdm
		greeter-hide-users=true
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

	# Set cursor to black
	# alternate config: vim ~/.gtkrc-2.0
	lxappearance # Mouse Cursor
		
	### Configs

	# Copy all conkys to correct directory
	mkdir -p bunsen-configs/.config/conky
	cp -r bunsen-conky/conky/* bunsen-configs/.config/conky/
	
	# Copy hidden configs to home dir (that have stuff in them)
	#cd ~/BunsenLabs/bunsen-configs
	#cp -r .config .conkyrc .gmrunrc .gtkrc-2.0 .gtkrc-2.0.mine .local .pbuilderrc .profile.bunsen 
	#	.Xresources .xsessionrc ~
	rm -r ~/{.config,.local}
	ln -s ~/BunsenLabs/bunsen-configs/{.config,.conkyrc,.gmrunrc,.gtkrc-2.0,.gtkrc-2.0.mine,.local,.pbuilderrc,.profile.bunsen,.Xresources,.xsessionrc}
		~/
	#cd ~/BunsenLabs

	### BL-Welcome

	#sudo mkdir -p /usr/lib/bunsen/bunsen-welcome
	#sudo cp -r bunsen-welcome/bunsen-welcome/ /usr/lib/bunsen/
	#sudo cp bunsen-welcome/bl-welcome /usr/bin
	sudo ln -s ~/BunsenLabs/bunsen-welcome/bunsen-welcome/ /usr/lib/bunsen
	sudo ln -s ~/BunsenLabs/bunsen-welcome/bl-welcome /usr/bin

	# Move all docs to their appropriate location
	# for dir in ${~/BunsenLabs/*} # includes full pathname instead of base dir
	#sudo mkdir /usr/share/doc/$dir
	#sudo mv ${dir}/debian/* ${dir}/READM* ${dir}/LICENS* /usr/share/doc/$dir
	# OR
	#sudo ln -s  ${dir}/debian/* ${dir}/READM* ${dir}/LICENS* /usr/share/doc/ 
	### TODO: actually figure out how to do this

	# My personal setup:
	# Icon theme: nouveGnomeGray
	iceweasel tsujan.deviantart.com/art/nouveGnomeGray-300365158
	#Click download
	#Open with Archive Manager
	#Extract to ~/BunsenLabs/
	sudo cp -r ~/BunsenLabs/nouveGnomeGray /usr/share/icons 
	# alternate config: ~/.gtkrc-2.0
	lxappearance # Icon Theme

	# Gtk theme: Bunsen2-Dark-Blue 
	# alternate config: ~/.gtkrc-2.0
	lxappearance # Theme

	# OBtheme: Bunsen1
	# alternate config: ~/.config/openbox/rc.xml
	obconf

	# For the conky and tint2 switchers pipemenus
	git clone git://github.com/woodape/bl-switcher
	#sudo cp bl-switcher/bl-* /usr/bin/
	sudo ln -s ~/BunsenLabs/bl-switcher/bl-* /usr/bin

	# For battery
	git clone git://github.com/decayofmind/tidybattery
	#sudo cp tidybattery/tidybattery.py /usr/bin/tidybattery
	sudo ln -s ~/BunsenLabs/tidybattery/tidybattery.py /usr/bin/tidybattery
	# Create config file from README -- just read it...
	echo -e "lots-of-lines"

	### BUGS

		* GRUB now takes forever
		* LightDM not working w/ avatar-icons
	


## PART 5 Reboot

	systemctl reboot 
	# you shouldn't need sudo to run systemctl after this reboot. Unless you
	# borked the system. In which case, do not pass go, and do not collect $200.
	

## Total install size

df

apt/dpkg

	
## TODO

### REPO FILES

* Uncomment first run scripts in openbox autostart
* Standardize wallpaper naming conventions
* Make lightdm/index.theme for LightDM avatar [look here](crunchbang.org/forums/viewtopic.php?pid=422743#422743)
* AUTOSTART: bl-hotcorners & | bl-{tint2|conky}-switcher-resume.py &
* CONFIG: .bash\_aliases .gtk-bookmarks 
* have as default `.config/tint2 .config/conky` for `bl-*-switcher.py`
* Rework bunsen-user-setup
* Write bunsen-installscripts

### CONFIG FILES
* Update `.config/openbox/autostart`
	* bl-hotcorners
	* tidybattery
	* comment out/replace xscreensaver
* Update `.config/openbox/menu.xml`
	* bl-*-switcher.py
	* multimedia-pipemenu
* Update `.gtkrc-2.0`
	* Mouse Theme
	* Icon Theme
	* GTK Theme
* Update `.config/openbox/rc.xml`
	* OB Theme
	* Key Bindings (see below, in DESKTOP)
* Update `.config/nitrogen/nitrogen.cfg` to reflect
  /usr/share/wallpapers as it's directory

### SETUP

* Set themes from command line in ~/.config/openbox/rc.xml
* Set wallpaper w/ nitrogen on the cmd-line

### DESKTOP

* cb-lock = light-locker vs xscreensaver vs i3lock
* Default OB keyboard shortcuts
