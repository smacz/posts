---
layout: post
title: "A Cost-Benefit Analysis of Troubleshooting"
date: 2017-01-17
category: Battlestation
program: Fedora
process: Metacognition
repo: 
description: "Working right through my workout schedule and way past my bedtime on helping a buddy install Fedora got me thinking - is it worth it to troubleshoot?"
references:
  - title:
    link:
---

# When Computers Fail, There's Always a Reason™

That's what makes them so great, and what makes my nights so long. One of my buddies had just got done telling me a quote I think I'll remember for the rest of my life:

>No, I wasn't going to just reboot it. Because then I'd have to reboot it next week, or tomorrow, or in an hour.

This just after explaining to me why he was stuck at work instead of coming to our open source club meeting. However, I came to understand that this same exact scenario was fixed by a reboot over six months ago according to him. So I ask myself, what was it worth to troubleshoot it?

# Old Paradigms

* Backups
* SAN
* Configuration Management
* Root Access
* Failover
* You're gunna break something

# New Tech

* Virtualization
    * Snapshots
    * Containers
* Configuration Management
* Cloud

# The Time-Epistemology Paradox

* Learning
    * The new guy draws the network diagram
    * Quicker to solve problems later on, next time
    * Share knowledge as value

* Time
    * Personal vs Paid
    * What is the cost

* Insert Pink Floyd joke Here

# Root Cause

* Documentation
* Google-fu is about as accurate as a near-sighted teenager with a rail gun
* Cargo Cultism
* System Complexity

# Determination

* Systematic vs conceptual failure
* Level of fuck-upedness
* Who's footing the bill

Was it worth it for me, this specific time? Yeah, yeah it was.
