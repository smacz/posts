---
layout: post
title: "ZNC, the IRC bouncer"
date: 2016-12-16
category: HomeLab
program: ZNC
process: Install
repo: https://andrewcz.com/gitlist/znc.git
image: fsociety_irc.jpeg
description: "Not having my own username while out and about on IRC got old really quick. My weechat idle server was on my internal network, and I wasn't going to be opening a ssh port there from the public internet anytime soon. So I decided I'll be putting a bouncer on my gateway VPS."
references:
  - title: ZNC Wiki
    link: http://wiki.znc.in/ZNC
  - title: ArchWiki - ZNC
    link: https://wiki.archlinux.org/index.php/Znc
  - title: CMD notification module for ZNC
    link: https://github.com/bgirard/znc-cmd-notify
  - title: A simple plugin for ZNC that sends emails to the user when they are mentioned and not connected
    link: https://github.com/d0ugal-archive/znc_mailer
  - title: ZNC awayping plugin
    link: http://www.jonmasters.org/blog/2009/06/22/znc-awayping-plugin-now-with-improved-antiping/
  - title: Setting up Weechat
  - link: https://alexjj.com/blog/2016/9/setting-up-weechat/
---

# Authentication

## Username & Pass

The password entry in znc, used by clients to connect to ZNC, has several configuration options.

```
Hash    The hash of a salted password.
    Hash = 44ccdd8655fb2c9bf8e6026fc51dfeabfd3e361f696c9373c00a496a4dcaed6f
Method  The password hashing method.
    Method = sha256
Salt    A random set of 20 characters for salting the password.
    Salt = ,e9a+t9WwSCjR_5:XAQu
```

All these are generated using znc --makepass. This has to be run on the server before the user can login.

I mean, theoretically, you can just do it once in a VM and get a valid Hash/Salt combination. That hadn't worked when I tried it. However, I did have to change my password to alphanumerical only as ZNC didn't like special characters in the password. After doing that, I don't believe that I had tried a re-deployment without running `znc --makepass` on the host. It's worth a shot.

# Weechat

## Adding the server

```
/server add BNC my.bouncer.net/6697 -ssl -username=username/network -password=password -autoconnect
/set irc.server.BNC.ssl on
/set irc.server.BNC.ssl_fingerprint MD5_or_SHA256_or_SHA512_fingerprint_here
/connect BNC
```

The two values `-ssl` and the set command right after it may seem redundant, however, they were both required in order to assure the server that I indeed wanted to use SSL.

It's really important to specify the `ssl_fingerprint` if you're using a cert that isn't signed by a CA, because if `gnutls` doesn't find it in your own cert store, it will reject the connection entirely.

## SSL Fingerprint

The SSL in weechat in Debian Jessie doesn't offer sha256 or sha512. In fact, it only offers sha1. However, in new versions, those other, more secure variants are accepted. In that case, getting the correct SSL fingerprint is just a matter of changing up the variable listed here. Otherwise, the following, executed on the server-side, will generate the correct fingerprint for weechat to use. Otherwise, it'll check it against the certs that it knows about. AKA in a liveiso or someone else's computer, it'd be best to use a Let's Encrypt cert to deal with that.

```
cat /var/lib/znc/.znc/znc.pem | openssl x509 -sha1 -fingerprint -noout | tr -d ':' | tr 'A-Z' 'a-z' | cut -d = -f 2
```

## SSL Certs

I don't know how I want to eventually integrate this into a standard. Right now I have all of my roles separately generating their own certs. When I start using Lets Encrypt, I'll have to use those certs, but what about the roles on their own? They need to be able to stand independently. I feel that I'll be integrating this into vault and a couple specially reserved variable names - e.g. "if defined".

## Auto NickServ IDENTIFY

```
/set irc.server.NETWORK.nicks <nick>
/set irc.server.NETWORK.command "/msg NickServ IDENTIFY <pass>"
```

If you are able to use SASL, that would be even better, but as long as your password is travelling over TLS, there shouldn't be a problem.

# Real-World

My VPS was kickbanned the first time I tried to connect. Turns out that the last person to use my IP address had an open proxy or a IRC spam bot running on it. So I had to submit a contention of the label to the database. This wasn't hosted by Freenode, but by a organization that ran DroneBL. It took three weeks or so for me to ask them on IRC if they had gotten around to it yet. I was told to email the guy who ran the thing. I did and the next day my IP address was taken off the blacklist. Sweet!

## Networks

When connecting with Weechat, there's not much I have been able to do in order to be in several different networks at once. There is a command to switch between networks once connected to ZNC though:

```
/znc JumpNetwork <network name>
```

This assumes that you've read ZNC's wiki page on Weechat which has two or three ways on how to pass `/znc` commands without Weechat intercepting them and throwing an error. The default network that is connected to on startup was the one specified in the `username` field when the server was added.

### Multiple Networks

In order to join multiple networks, create several different servers that use ZNC's functionality of specifying the network along with the username to point to a specific network:

```
/server add BNC <my.bouncer.net>/6697 -ssl -username=<username>/<network1> -password=<password> -autoconnect
/server add BNC <my.bouncer.net>/6697 -ssl -username=<username>/<network2> -password=<password> -autoconnect
/server add BNC <my.bouncer.net>/6697 -ssl -username=<username>/<network3> -password=<password> -autoconnect
/server add BNC <my.bouncer.net>/6697 -ssl -username=<username>/<network4> -password=<password> -autoconnect
```

This is on the weechat side of things, as the bouncer should automatically connect to the various networks.

# Plugins

# Ops

I always forget the command to set `+o` on my account to change the topic For future reference here's how to become `OPS` as a previously authenticated user:

```
/msg chanserv op #osuosc
```

And then to De-op:

```
/deop
```

## Ops Actions

### Setting the topic

The topic of the room can be set from the weechat command line just like anything else, but only by an op. `/topic` can retrieve the topic without being ops. But as an op, the topic can be _set_, with the same command:

```
/topic <topic to display>
```

### Kicking a user out of a channel

```
/kick <user>
```

Hahahahahahahahahaha.
