---
layout: post
title: "A Network for a HomeLab"
date: 2017-08-19
category: HomeLab
program: Networking
process: Architect
repo: 
image: 
description: "There's a lot that goes into architecting a nework, from production and development separation, to physical hardware management, to guest networks. Doing this right requires a lot of thought and vetting. Hopefully this is a good start."
references:
  - title: Multitier Architecture
    link: https://en.wikipedia.org/wiki/Multitier_architecture
  - title: Subnetting and VLANs
    link: https://learningnetwork.cisco.com/thread/34441
---

# Logical Network

So first and most importantly is the architecture of the internet-facing application network. I decided to take a two-tier approach, with a third, inner management layer. The paradigm here is called "Concentric Rings of Security", in that it mimics a citadel and the levels that house more important things the closer you get to the inner-most level, eventually ending up with the most important people and things in the very center. However, as we'll see, there's always a back door...

## Public

~~So the incoming connection is connected to a router/firewall that hosts a subnet full of UI-layer servers. These are the Apache's and Postfix's of a software stack. These relay the generated information to whomever requested it.

The second layer is a combination of both the data and the programs that interpret that data in a meaningful way. These are known as the "Business Layer" and "Data Layer" respectively. There are many schemes in which these are separated into other subnets with firewalls separating them, as they represent different levels of risk were they to be exploited, however I chose to group them together. This is because there exists such an unambiguous relationship between the two servers, that there is little to no realistic benefit to be gained by placing a firewall between the two. Additionally, doing so would create way too many firewall rules to maintain that could have been avoided otherwise. Lastly, if the related servers were placed in their own VLAN to segment them from the rest of the subnet, that would prevent different applications from having to be exposed to one another in the event that one of them were to be compromised.

Now, there are some who balk at providing DNS for all of those servers into the UI layer - also known as, and referred to hereafter as the DMZ - however, that is an advanced "best practice" that I would choose to eschew much like the separate subnets, due to the fact that these servers need to get in touch with each other somehow. However, I will provide them with static IP addresses, instead of using DHCP, as it is best for servers in general. So the DNS entries for these two subnets - Business Logic/Data (BL/D) and DMZ - will be housed in the DMZ's router/firewall, as that is using pfSense, which allows (like many firewalls) for use as a DNS server, with a built-in bind daemon.

Now, the third subnet that is allowed direct access to the public application subnets is the Shared Services (SHR) subnet, whose gateway is a client on the BL/D subnet.This is where the administrative user's private keys reside on a jumphost that is used for all outside administration of servers. This server can allow SOCKS proxying and ssh forwarding as necessary. Additionally, all internal services that are capable of master-slave relationships (LDAP) should be housed in this subnet, and pushed out to the upstream subnets.

A graphical representation of this may look like the following:

## Private

Here's where it gets a little tricky. For instance, I need a location where I can place my laptop, where it will be both protected, privileged to administer the application subnets, and operate as a workstation. In order to do this, there is a separate router directly connected to the internet, that serves the Workstation subnet. However, that same router (or a connected router) also allows connection to the jumphost(s) inside of the SHR subnet.

In once case, I have the virtualization hosts that are hosting these virtual networks inside of this subnet. This allows management of the hosts via the jumphost, in a secured environment. However, there is another setup wherein the hardware is connected via dedicated ethernet ports to a separate subnet that is only accessible via the Workstation subnet. Lastly, there is another subnet that is for Guest Wifi only. This is completely segmented from the rest of the network, and only has access to the internet writ large. Expanding on our diagram:~~

#### While most of that is still vaguely true, I've gotten schooled in network architecture, and have been advised to build _out_ rather than build _down_.

# Networks

We have four internal networks that are in a star configuration with the WAN-facing router. Each network has a pfSense gateway.

## COR

This network contains the core routers that are served by the WAN router. Their IP address range should be different than the application servers to easily differentiate them when troubleshooting.

## DMZ 

This is the externally-reachable network that serves all incoming requests. This is usually done with a series of port-forwards. This network can reach the internet and the BLD network, however both firewalls between the DMZ and BLD networks should be administered on a whitelist basis. That means only allowing specific applications as they are created.

## BLD

This is the network where most of the data and business logic servers live. This network communicates almost exclusively with the DMZ network serving its requests. This network stores all of the application data, and should be guarded carefully. However, due to design requirements, there should be internet access to the internet, for updates, data retrieval, etc. However, NO INCOMING PACKETS SHOULD ORIGINATE OUTSIDE OF THE INTERNAL NETWORK.

## ADM 

This is the administrative network that takes care of the rest. This is tricky, because we don't want _any_ incoming connections, as this houses access to our infastructure and privileged accounts. This is the home of our jumphosts, LDAP master servers, and DNS masters. These can all push out configuration options to slave servers in the other networks. This also is where we run Ansible or other configuration management from. Also, the physical hosts should be in this network as well.

Also, this is the only other location that is accessible from the WAN. However, it is only in the form of a port-forward directly from the WAN on a non-standard port to a jumphost that only allows authenticated account logins.

## VIP

This network is for BYOD and other unauthenticated/controlled accounts. These are _definitely_ not allowed anywhere near our other networks, and should be directed straight out to the internet, as mostly this is just for browsing. However, there should be an ability to get to one of the jumphosts on the administrative network, as it would then provide access to administer the network locally without having to do circuitous routing out to the WAN and back.

# IP Addressing and Naming Scheme

So how do we do this on a practical level? Well, since we know what we're looking for, it's just a matter of logically enumerating everything that we're looking at.

## Server Names (Public)

The hostnames of the servers is not arbitrary - each section of the hostname means something. They are formatted like `<service><func><##><os><net><env><loc>` for example, a blog webserver that is not loadbalanced (the only one) that is in production running on a CentOS server in my homelab would look like `blogui01pcho`.

- Service
  - custom name up to 7 chars
- Functionality
  - One of:
    - `ui`: presentation
    - `bl`: business logic
    - `db`: data(base)
    - `nw`: network
    - `in`: infrastructure
    - `sp`: special
    - `vh`: virtualization host
- Number
  - Count up from `01`, padded to 2 digits
- Operating System
  - One of:
    - `f`: Fedora
    - `c`: CentOS
    - `w`: Windows
    - `u`: Ubuntu
    - `d`: Debian
    - `b`: BSD
- Network
  - One of:
    - `c`: COR
    - `d`: DMZ
    - `b`: BLD
    - `s`: ADM
    - `g`: VIP (guest)
  - In the event that a server is in two networks at the same time (router/firewall) the network should be defined as the LAN-side of the box
- Environment
  - One of:
    - `p`: production
    - `d`: development
    - `s`: shared services
    - `n`: spine (hardware)
    - `h`: hub (workstation)
  - One of:
    - `s`: stallman
    - `t`: turing
    - `f`: focault/feynman
- Location
  - One of:
    - `h`: hosted
    - `c`: crowncloud
    - `v`: vultr
    - `d`: digital ocean
  - One of:
    - `o`: office

## Networks

This directly relates to the environment of the hostname, as well as the DNS server that is providing the entry server.

- `dev.andrewcz.com` - development
  - `10.0.0.0/16`
  - `dmz.dev.andrewcz.com` - demilitarized zone
    - `10.0.1.0/24`
  - `bld.dev.andrewcz.com` - business layer/data
    - `10.0.2.0/24`
  - `shr.dev.andrewcz.com` - shared services
    - `10.0.3.0/24`
- `prd.andrewcz.com` - production
  - `10.1.0.0/16`
  - `dmz.prd.andrewcz.com` - demilitarized zone
    - `10.1.1.0/24`
  - `bld.prd.andrewcz.com` - business layer/data
    - `10.1.2.0/24`
  - `shr.prd.andrewcz.com` - shared services
    - `10.1.3.0/24`
- `hub.andrewcz.com` - workstation/hub
  - `192.168.2.0/24`
- `spn.andrewcz.com` - spine
  - `192.168.3.0/24`
- `gst.andrewcz.com` - guest
  - `192.168.1.0/24`

## Address ranges

For each IP address, the last octet can indicate what type of services a given server provides.

#### TODO: These should be able to be easily put into CIDR notation 

- 0/27 `0-31` - Network Appliances (Switches, Routers, Firewalls, etc.)
- 32/27 `32-63` - Infastructure (LDAP, Logging, IDS/IPS, etc.)
- 64/26 & 128/26 `64-191` - Services (Application Services)
- 254/26 `192-255` - Special/Reserved (Jumphost, admin boxes)

## Accounts

On every server, there should exist several accounts, based on the server usage and location:

- `root` - Disabled login if possible, and random passwd
- `admin` - Add to group `wheel`
- service accounts - `apache`, `postfix`, etc.
- user accounts - served by LDAP, and subject to access control lists (group or host-level)
