---
layout: post
title: "Subscribing to Nextcloud Public Calendar Link"
date: 2021-05-16
category: Sysadmin
program: Nextcloud
process: Configure
repo:
image: calendar.png
description: "Gracefully receiving gifts is difficult, for me at least. Fitting them into my existing life is a surprise challenge/opportunity. Especially when it's not readily obvious how to do that."
references:
  - title: 'Share calendar as iCal link'
    link: 'https://help.nextcloud.com/t/share-calendar-as-ical-link/64257'
---

So, recently I was looking to collaborate with someone else with a nextcloud instance. Naturally, they were using the calendar app, so I asked them to share the calendar link.

Well, without any prompting, what I got was a link to the HTML page of the Nextcloud calendar. It looked like this:

```
https://example.com/nextcloud/apps/calendar/p/ch3rX4OmNj9jZecc
```

While this is nice, it's not exactly workable to what I have going on right now. That's because I have all of my calendars sync'd to my phone. This is using DAVx5 and ICSx5 and connecting them to my Nextcloud instance. With that public link, I couldn't use either WebCal or WebDAV to sync it.

# Transposing the Public Link to a WebCal URL

So while there _is_ a way for the share link to natively come out as a WebCal link, since it appeared that this was the first thing that the individual I was working with was able to determine to be the way to share the calendar, I wanted to see if there was a way for me to make it work on my side without any further follow-up on their end.

It turns out that there is. Nextcloud has a "Share as a link" box for calendars, that would format that same URL into a WebCal link. So, taking the link above, I was able to transpose it as such:

```
webcal://example.com/remote.php/dav/public-calendars/ch3rX4OmNj9jZecc?export
```

With this, I was able to import this into my Nextcloud calendar listing as a link, and it gets not only updated on Nextcloud, but also synced to my phone too!

This seems to be truly the best of both worlds.
